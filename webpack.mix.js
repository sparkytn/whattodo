const mix = require('laravel-mix');
require('laravel-mix-ignore');
path = require('path');
require('@tinypixelco/laravel-mix-wp-blocks');
const WebpackBar = require('webpackbar');
const autoprefixer = require('autoprefixer');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Sage application. By default, we are compiling the Sass file
 | for your application, as well as bundling up your JS files.
 |
 */

mix
	.setPublicPath('./public')
	.webpackConfig({
		output: {
			publicPath: 'public/',
			chunkFilename: 'scripts/vendors/[name].bundle.js',
		},
		resolve: {
			mainFields: ['browser', 'main'],
			alias: {
				'@components': path.join(__dirname, 'resources/assets/scripts/components')
			}
		},
		plugins: [
			new WebpackBar()
		],

		// Minimize chunk splitting to 1
		optimization: {
			splitChunks: {
				cacheGroups: {
					vendors: false
				}
			}
		}
	})
	.autoload({
		jquery: ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery"],
	});

mix
	.browserSync({
		proxy: 'https://whattodo.test',
		https: true,
	});
mix.sass('resources/assets/styles/vendors/lightgallery.scss', 'styles/vendors');

mix
	.sass('resources/assets/styles/admin.scss', 'styles')

mix
	.sass('resources/assets/styles/app.scss', 'styles')
	.options({
		processCssUrls: false,
	})
	.purgeCss({
		content: [
			"includes/**/*.php",
			"functions.php",
			"index.php",
			"app/**/*.php",
			"resources/**/*.html",
			"resources/**/*.js",
			"resources/**/*.php",
			"storage/framework/**/*.php",
		],
		safelist: {
			standard: [	'show','hide','fade','modal-backdrop','g-col-xl-4','g-col-xl-3','alert-tertiary',
						'collapsing','fa-custom', 'fa-roots', 'fa-extreme','fa-cultural',
						'icon-activity', 'fb_invisible_flow','loaded', 'pb-0','pb-md-5','g-md-4'],
			deep: [/^woocommerce-/,/^yith-/,/^select2-/,/^lg/, /^single-product/, /^login/, /^iti/, /^entry-content/, /^fa-google/],
		}
	});


mix
	.js('resources/assets/scripts/app.js', 'scripts')
	.js('resources/assets/scripts/customizer.js', 'scripts')
	.blocks('resources/assets/scripts/editor.js', 'scripts')
	.autoload({jquery: ['$', 'window.jQuery']})
	.extract();

mix
	.copyDirectory('resources/assets/images', 'public/images')
	.copyDirectory('resources/assets/fonts', 'public/fonts');

mix
	.sourceMaps(false, "source-map")
	.version();
