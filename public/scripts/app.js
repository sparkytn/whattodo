"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["/scripts/app"],{

/***/ "./resources/assets/scripts/app.js":
/*!*****************************************!*\
  !*** ./resources/assets/scripts/app.js ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var bootstrap_js_src_collapse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap/js/src/collapse */ "./node_modules/bootstrap/js/src/collapse.js");
/* harmony import */ var bootstrap_js_src_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap/js/src/dropdown */ "./node_modules/bootstrap/js/src/dropdown.js");
/* harmony import */ var bootstrap_js_src_offcanvas__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bootstrap/js/src/offcanvas */ "./node_modules/bootstrap/js/src/offcanvas.js");
/* harmony import */ var bootstrap_js_src_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! bootstrap/js/src/modal */ "./node_modules/bootstrap/js/src/modal.js");
/* harmony import */ var vanilla_lazyload__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vanilla-lazyload */ "./node_modules/vanilla-lazyload/dist/lazyload.min.js");
/* harmony import */ var vanilla_lazyload__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vanilla_lazyload__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var intl_tel_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! intl-tel-input */ "./node_modules/intl-tel-input/index.js");
/* harmony import */ var intl_tel_input__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(intl_tel_input__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var intl_tel_input_build_js_utils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! intl-tel-input/build/js/utils */ "./node_modules/intl-tel-input/build/js/utils.js");
/* harmony import */ var intl_tel_input_build_js_utils__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(intl_tel_input_build_js_utils__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lightgallery__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lightgallery */ "./node_modules/lightgallery/lightgallery.umd.js");
/* harmony import */ var lightgallery__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lightgallery__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var lightgallery_plugins_thumbnail__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lightgallery/plugins/thumbnail */ "./node_modules/lightgallery/plugins/thumbnail/lg-thumbnail.umd.js");
/* harmony import */ var lightgallery_plugins_thumbnail__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lightgallery_plugins_thumbnail__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var lightgallery_plugins_zoom__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lightgallery/plugins/zoom */ "./node_modules/lightgallery/plugins/zoom/lg-zoom.umd.js");
/* harmony import */ var lightgallery_plugins_zoom__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lightgallery_plugins_zoom__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_swiper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @components/swiper */ "./resources/assets/scripts/components/swiper.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "jquery");


function _createForOfIteratorHelper(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];

  if (!it) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function F() {};

      return {
        s: F,
        n: function n() {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function e(_e) {
          throw _e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function s() {
      it = it.call(o);
    },
    n: function n() {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function e(_e2) {
      didErr = true;
      err = _e2;
    },
    f: function f() {
      try {
        if (!normalCompletion && it["return"] != null) it["return"]();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}
/**
 * External Dependencies
 */







window.Modal = bootstrap_js_src_modal__WEBPACK_IMPORTED_MODULE_4__["default"];
window.Dropdown = bootstrap_js_src_dropdown__WEBPACK_IMPORTED_MODULE_2__["default"];

__webpack_require__(/*! bootstrap-select */ "./node_modules/bootstrap-select/dist/js/bootstrap-select.js");







$(document).ready(function () {
  /*
  -----------------------------------------
  	Select dropdown
  -----------------------------------------
  */
  var lazyLoadInstance = new (vanilla_lazyload__WEBPACK_IMPORTED_MODULE_5___default())({// Your custom settings go here
  });
  $('.select-dropdown').selectpicker({
    style: '',
    styleBase: 'form-select',
    size: 10
  }); // when the modal is opened autoplay it
  // $('#HeroVideoModal').on('shown.bs.modal', function (e) {
  // 	$("#videoModalIframe").attr('src',$videoSrc + "?amp;modestbranding=1&amp;showinfo=0");
  // })
  //
  // $('#HeroVideoModal').on('hidden.bs.modal', function (event) { // when modal hidden
  // 	$('#videoModalIframe') // pick the right element
  // 		.attr('src', '');  // empty the video id
  // })

  /*
  -----------------------------------------
  Dropdown show on Hover
  -----------------------------------------
  */
  // make it as accordion for smaller screens

  if (window.innerWidth > 992) {
    document.querySelectorAll('.dropdown-hover').forEach(function (everyitem) {
      var dropdownItem = bootstrap_js_src_dropdown__WEBPACK_IMPORTED_MODULE_2__["default"].getOrCreateInstance(everyitem.querySelector('[data-bs-toggle]'));
      everyitem.addEventListener('mouseover', function (e) {
        dropdownItem.show();
      });
      everyitem.addEventListener('mouseleave', function (e) {
        dropdownItem.hide();
        $(e.currentTarget).focusout();
      });
    });
  }

  var phoneNumberInput = document.querySelector("#billing_phone") || document.querySelector("#client-phone");

  if (phoneNumberInput) {
    window.phoneNumberInput = intl_tel_input__WEBPACK_IMPORTED_MODULE_6___default()(phoneNumberInput, {
      initialCountry: 'auto',
      geoIpLookup: function geoIpLookup(callback) {
        $.get('https://ipinfo.io', function () {}, "jsonp").always(function (resp) {
          var countryCode = resp && resp.country ? resp.country : "tn";
          callback(countryCode);
        });
      },
      separateDialCode: true,
      hiddenInput: "full_phone",
      formatOnDisplay: true,
      utilsScript: (intl_tel_input_build_js_utils__WEBPACK_IMPORTED_MODULE_7___default())
    });
  } //woocommerce-EditAccountForm
  // $( document.body ).on( 'updated_checkout', function(data) {
  // 	var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>",
  // 		country_code = $('#billing_country').val();
  //
  // 	var ajax_data = {
  // 		action: 'append_country_prefix_in_billing_phone',
  // 		country_code: $('#billing_country').val()
  // 	};
  // 	if(firstCall){
  // 		firstCall = false;
  // 	}else{
  // 		$.post( ajax_url, ajax_data, function( response ) {
  // 			let phone = $('#billing_phone').val()
  // 			$('#billing_phone').val(response);
  //
  // 		});
  // 	}
  //
  // } );

  /*
  -----------------------------------------
  Dynamic Imports
  -----------------------------------------
  */
  // light gallery


  if ($('#lightgallery').length) {
    // 	import('./components/lightgallery.js' /* webpackChunkName: "lightgallery" */)
    // 		.catch(error => 'An error occurred while loading the component');
    lightgallery__WEBPACK_IMPORTED_MODULE_8___default()(document.getElementById('lightgallery'), {
      plugins: [(lightgallery_plugins_zoom__WEBPACK_IMPORTED_MODULE_10___default()), (lightgallery_plugins_thumbnail__WEBPACK_IMPORTED_MODULE_9___default())],
      speed: 500,
      licenseKey: '0000-0000-000-0001'
    });
  }
  /*
  -----------------------------------------
  Price range
  -----------------------------------------
  */
  // light gallery


  if ($('.price-range').length) {
    noUiSlider.create($('.price-range')[0], {
      start: [10, 1500],
      connect: true,
      tooltips: true,
      step: 10,
      range: {
        'min': [0, 5],
        '30%': [500, 5],
        '60%': [1000, 5],
        'max': [1500, 5]
      },
      pips: {
        mode: 'range',
        density: 5
      }
    });
  }
  /*
  -----------------------------------------
  	Show booking sticky bar for booking now
  -----------------------------------------
  */


  if ($('#booking-card').length) {
    var showBookingHeaderCTA = function showBookingHeaderCTA(mediaQueryDownLG) {
      if ($('body').hasClass('page')) {
        return false;
      }

      if (mediaQueryDownLG.matches) {
        // If media query matches
        bookingHeaderCTA.addClass('mobile');
      } else {
        bookingHeaderCTA.removeClass('mobile');
      }

      bookingCardPos = $('#booking-card').offset().top;
      bookingCardHeight = $('#booking-card').height();
    };

    var mediaQueryDownLG = window.matchMedia("(max-width: 992px)"),
        bookingHeaderCTA = $('.activity-booking-cta'),
        bookingCardHeight = $('#booking-card').height(),
        bookingCardPos = $('#booking-card').offset().top;
    mediaQueryDownLG.addListener(showBookingHeaderCTA);
    showBookingHeaderCTA(mediaQueryDownLG);
    $(window).scroll(function () {
      if (bookingHeaderCTA.hasClass('mobile')) {
        if ($(this).scrollTop() > 200 && $(this).scrollTop() <= bookingCardPos - window.innerHeight + 100) {
          bookingHeaderCTA.addClass('show');
        } else {
          bookingHeaderCTA.removeClass('show');
        }
      } else {
        if ($(this).scrollTop() > bookingCardPos + bookingCardHeight / 2) {
          bookingHeaderCTA.addClass('show');
        } else {
          bookingHeaderCTA.removeClass('show');
        }
      }
    });
  }
  /*
  -----------------------------------------
  	Configure  Google maps to auto-complite in Tunisia
  -----------------------------------------
  */


  if ($('.yith-wcbk-google-maps-places-autocomplete').length) {
    jQuery(".yith-wcbk-google-maps-places-autocomplete").each(function () {
      var mapAutocomplete = new google.maps.places.Autocomplete(this, {
        types: ['(cities)'],
        componentRestrictions: {
          country: "tn"
        }
      });
    });
  }
  /*
  -----------------------------------------
  	Quantity Input hack
  -----------------------------------------
  */


  $('.quantity-input').each(function () {
    var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min') ? input.attr('min') : 0,
        max = input.attr('max') ? input.attr('max') : 100;
    btnUp.click(function () {
      var oldValue = input.val() ? parseFloat(input.val()) : 0;

      if (max && oldValue >= max) {
        var newVal = max;
      } else {
        var newVal = oldValue + 1;
      }

      if (newVal < min) {
        newVal = min;
      }

      input.val(newVal).change();
    });
    btnDown.click(function () {
      var oldValue = parseFloat(input.val());

      if (oldValue <= min) {
        var newVal = min;
      } else {
        var newVal = oldValue - 1;
      }

      if (newVal > max) {
        newVal = max;
      }

      input.val(newVal).change();
    });
  });
  /*
  -----------------------------------------
  	Reduce Paragraph height and add read more
  -----------------------------------------
  */

  var showChar = 300;
  var ellipsesText = "...";
  var moreText = "<i class='fas fa-plus-circle me-1'></i> Lire la suite";
  var lessText = "<i class='fas fa-minus-circle me-1'></i>Réduire";
  $('.more').each(function () {
    var content = $(this).html();

    if (content.length > showChar) {
      var c = content.substr(0, showChar);
      var h = content.substr(showChar - 1, content.length - showChar);
      var html = c + '<span class="moreellipses">' + ellipsesText + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<br><a href="#" class="morelink readmore-link-collapse small d-inline-block mt-2">' + moreText + '</a></span>';
      $(this).html(html);
    }
  });
  $(".morelink").click(function () {
    if ($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moreText);
    } else {
      $(this).addClass("less");
      $(this).html(lessText);
    }

    $(this).parent().prev().toggle();
    $(this).prev().prev().toggle();
    return false;
  });
  /*
  -----------------------------------------
  	Sidebar sticky
  -----------------------------------------
  */
  // if($('.sticky').length){
  // 	 new StickySidebar('.sticky', {
  // 		containerSelector: '.sidebar',
  // 		topSpacing: 60,
  // 		bottomSpacing: 60
  // 	});
  // }

  /*
  -----------------------------------------
  	Share buttons
  -----------------------------------------
  */

  if ($('[btn-social-share]').length) {
    var Url = window.location.href;
    var UrlEncoded = encodeURIComponent(Url);
    var title = encodeURIComponent($(".page-header .title").html());
    $("#fb-share").attr('href', "http://www.facebook.com/share.php?u=" + UrlEncoded);
    $("#tw-share").attr('href', "https://twitter.com/intent/tweet?text=" + title + " " + UrlEncoded);
    $("#in-share").attr('href', "http://www.linkedin.com/shareArticle?mini=true&url=" + UrlEncoded + "&title=" + title);
  } // $('.hf-form').on('hf-error', function(e) {
  // 	console.log('asdas', e.currentTarget.dataset['messageError']);
  // 	return;
  // });
  // $('.hf-form').on('hf-success', function(e) {
  // 	console.log('asdas', e.currentTarget.dataset['messageSuccess']);
  // 	return;
  // });


  $(document).on("click", '.woocommerce-checkout button[name="apply_coupon"]', function (event) {
    event.preventDefault();
    var $form = $('form[name="checkout"]');
    $form.block({
      message: ''
    });
    var data = {
      security: wc_checkout_params.apply_coupon_nonce,
      coupon_code: $('input[name="coupon_code"]').val()
    };
    $.ajax({
      type: 'POST',
      url: wc_checkout_params.wc_ajax_url.toString().replace('%%endpoint%%', 'apply_coupon'),
      data: data,
      success: function success(code) {
        console.log(code);
        $('.woocommerce-error, .woocommerce-message').remove();
        $form.removeClass('processing').unblock();

        if (code) {
          $('button[name="apply_coupon"]').parent().after(code);
          setTimeout(function () {
            $('.woocommerce-form-coupon .alert').fadeOut(300);
          }, 4000);
          $(document.body).trigger('update_checkout', {
            update_shipping_method: false
          });
        }
      },
      dataType: 'html'
    });
  }); // var forms = document.querySelectorAll('.needs-validation')
  // // Loop over them and prevent submission
  // Array.prototype.slice.call(forms)
  // 	.forEach(function (form) {
  // 		form.addEventListener('submit', function (event) {
  // 			event.preventDefault();
  // 			event.stopPropagation();
  //
  // 			let $this = $('#addPost');
  //
  // 			if ($this.hasClass('disabled')) return false;
  //
  //
  //
  // 			form.classList.add('was-validated');
  // 			form.classList.add('disabled');
  //
  // 		}, false)
  // 	})

  $('#sparkyContactForm').on('submit', function (e) {
    e.preventDefault();
    $('.is-invalid').removeClass('is-invalid');
    $('[js-feedback-form]').addClass('d-none');
    var formHTML = document.getElementById('sparkyContactForm');
    var form = $(this),
        spinner = $('[js-spinner]'),
        full_name = form.find('#client-name').val(),
        subject = form.find('#client-subject').val(),
        email = form.find('#client-email').val(),
        phone = form.find('#client-phone').val(),
        message = form.find('#client-message').val(),
        ajaxUrl = form.data('url'),
        nonce_data = form.data('nonce');
    var test = document.querySelector('#sparkyContactForm');
    var elmErr = form.find('.form-control:invalid,.form-select:invalid,.custom-range:invalid,.custom-file-input:invalid,.custom-control-input:invalid');
    form.addClass('was-validated');

    if (!this.checkValidity() && elmErr && elmErr.length > 0) {
      $(window).scrollTop($(elmErr[0]).offset().top - 50);
      return false;
    }

    if (form.hasClass('disabled')) {
      return false;
    }

    var formData = new FormData(formHTML);
    var data = {};

    var _iterator = _createForOfIteratorHelper(formData),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var _step$value = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_step.value, 2),
            key = _step$value[0],
            value = _step$value[1];

        data[key] = value;
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    form.addClass('disabled');
    spinner.removeClass('d-none');
    form.find('[type="submit"').addClass('disabled');
    grecaptcha.ready(function () {
      grecaptcha.execute('6LeQ4nYfAAAAAMyykUj7Xazlegxz7Js-zYfuX8Os', {
        action: 'submit'
      }).then(function (token) {
        data['token'] = token;
        data['nonce_data'] = nonce_data;
        data['action'] = 'sparky_save_user_contact_form';
        $.ajax({
          url: ajaxUrl,
          type: 'post',
          data: data,
          error: function error(response) {
            spinner.addClass('d-none');
            form.removeClass('disabled');
            form.find('input, button, textarea').removeAttr('disabled');
            form.find('button').removeClass('disabled');
            $('[js-feedback-form]').removeClass('d-none');
          },
          success: function success(response) {
            spinner.addClass('d-none');
            form.find('button').removeClass('disabled');
            form.find('input, button, textarea').removeAttr('disabled');
            form.removeClass('disabled');
            form.removeClass('was-validated');
            $("#sparkyContactForm")[0].reset();

            if (response.success) {
              var myModal = new bootstrap_js_src_modal__WEBPACK_IMPORTED_MODULE_4__["default"](document.getElementById('formSubmittedModal'), {
                backdrop: "static"
              });
              myModal.show();
            } else {
              if (response.message === 'google_captcha') {
                $('[js-feedback-form] .detail').html('Captcha non valide,');
              }

              if (response.message === 'required_fields') {
                $('[js-feedback-form] .detail').html('Tous les champs avec un astérisque (*) sont obligatoires.');
              }

              if (response.message === 'invalid_email') {
                $('[js-feedback-form] .detail').html('Votre adresse email est non valide');
              }

              console.log(response);
              $('[js-feedback-form]').removeClass('d-none');
            }
          }
        });
      });
    });
  });
  $('#sparkyCustomActivity').on('submit', function (e) {
    e.preventDefault();
    $('.is-invalid').removeClass('is-invalid');
    $('[js-feedback-form]').addClass('d-none');
    var form = $(this),
        spinner = $('[js-spinner]'),
        ajaxUrl = form.data('url');
    var elmErr = form.find('.form-control:invalid,.form-select:invalid,.custom-range:invalid,.custom-file-input:invalid,.custom-control-input:invalid');
    form.addClass('was-validated');

    if (!this.checkValidity() && elmErr && elmErr.length > 0) {
      $(window).scrollTop($(elmErr[0]).offset().top - 50);
      return false;
    }

    if (form.hasClass('disabled')) {
      return false;
    }

    form.addClass('disabled');
    spinner.removeClass('d-none');
    form.find('[type="submit"').addClass('disabled');
    grecaptcha.ready(function () {
      grecaptcha.execute('6LeQ4nYfAAAAAMyykUj7Xazlegxz7Js-zYfuX8Os', {
        action: 'submit'
      }).then(function (token) {
        $('#token').val(token);
        $.ajax({
          url: ajaxUrl,
          type: 'post',
          data: $('#sparkyCustomActivity').serialize(),
          error: function error(response) {
            spinner.addClass('d-none');
            form.removeClass('disabled');
            form.find('input, button, textarea').removeAttr('disabled');
            form.find('button').removeClass('disabled');
            $('[js-feedback-form]').removeClass('d-none');
          },
          success: function success(response) {
            spinner.addClass('d-none');
            form.find('button').removeClass('disabled');
            form.find('input, button, textarea').removeAttr('disabled');
            form.removeClass('disabled');
            form.removeClass('was-validated');
            $("#sparkyCustomActivity")[0].reset();

            if (response.success) {
              var myModal = new bootstrap_js_src_modal__WEBPACK_IMPORTED_MODULE_4__["default"](document.getElementById('formSubmittedModal'), {
                backdrop: "static"
              });
              myModal.show();
            } else {
              if (response.message === 'google_captcha') {
                $('[js-feedback-form] .detail').html('Captcha non valide,');
              }

              if (response.message === 'required_fields') {
                $('[js-feedback-form] .detail').html('Tous les champs avec un astérisque (*) sont obligatoires.');
              }

              if (response.message === 'invalid_email') {
                $('[js-feedback-form] .detail').html('Votre adresse email est non valide');
              }

              $('[js-feedback-form]').removeClass('d-none');
            }
          }
        });
      });
    });
  });
});

/***/ }),

/***/ "./resources/assets/scripts/components/swiper.js":
/*!*******************************************************!*\
  !*** ./resources/assets/scripts/components/swiper.js ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "jquery");
 // Configure Swiper to use modules

swiper__WEBPACK_IMPORTED_MODULE_0__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_0__.Navigation, swiper__WEBPACK_IMPORTED_MODULE_0__.Pagination, swiper__WEBPACK_IMPORTED_MODULE_0__.Autoplay, swiper__WEBPACK_IMPORTED_MODULE_0__.EffectFade, swiper__WEBPACK_IMPORTED_MODULE_0__.Lazy]); // init Swiper:

$('.home-hero-carousel').each(function () {
  var $this = $(this);
  var slider = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](this, {
    // Default parameters
    slidesPerView: 1,
    init: true,
    spaceBetween: 0,
    speed: 1000,
    loop: true,
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    lazy: true,
    autoplay: {
      delay: 4000
    } // on: {
    // 	init: function () {
    // 		this.update();
    // 	},
    // }

  }); //slider.init();
});
$('.home-activities-carousel').each(function () {
  var $this = $(this);
  var slider = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](this, {
    // Default parameters
    slidesPerView: 1.3,
    slidesPerGroup: 1,
    init: false,
    spaceBetween: 30,
    pauseOnMouseEnter: true,
    speed: 600,
    loop: false,
    lazy: true,
    autoplay: {
      delay: 4000,
      disableOnInteraction: true
    },
    navigation: {
      nextEl: $this.parent().find('.swiper-button-next')[0],
      prevEl: $this.parent().find('.swiper-button-prev')[0]
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets'
    },
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1.3,
        spaceBetween: 20,
        slidesPerGroup: 1
      },
      // when window width is >= 480px
      767: {
        slidesPerView: 2.5,
        spaceBetween: 20,
        slidesPerGroup: 2
      },
      1000: {
        slidesPerView: 3.2,
        slidesPerGroup: 3
      },
      // when window width is >= 640px
      1300: {
        slidesPerView: 4.2,
        slidesPerGroup: 4
      }
    },
    on: {
      init: function init() {
        this.update();
      }
    }
  });
  slider.init();
});

if ($('.detail-activities-carousel').length) {
  $('.detail-activities-carousel').each(function () {
    var $this = $(this);
    var slider = new swiper__WEBPACK_IMPORTED_MODULE_0__["default"](this, {
      // Default parameters
      slidesPerView: "auto",
      init: false,
      spaceBetween: 30,
      pauseOnMouseEnter: true,
      speed: 600,
      loop: false,
      autoplay: {
        delay: 4000,
        disableOnInteraction: true
      },
      lazy: true,
      navigation: {
        nextEl: $this.parent().find('.swiper-button-next')[0],
        prevEl: $this.parent().find('.swiper-button-prev')[0]
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets'
      },
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1.3,
          spaceBetween: 20,
          slidesPerGroup: 1
        },
        // when window width is >= 480px
        767: {
          slidesPerView: 2.5,
          spaceBetween: 20,
          slidesPerGroup: 2
        },
        1000: {
          slidesPerView: 3.5,
          slidesPerGroup: 3
        },
        // when window width is >= 640px
        1200: {
          slidesPerView: 4.5,
          slidesPerGroup: 4
        }
      },
      on: {
        init: function init() {
          this.update();
        }
      }
    });
    slider.init();
  });
}

/***/ }),

/***/ "./resources/assets/styles/vendors/lightgallery.scss":
/*!***********************************************************!*\
  !*** ./resources/assets/styles/vendors/lightgallery.scss ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/assets/styles/admin.scss":
/*!********************************************!*\
  !*** ./resources/assets/styles/admin.scss ***!
  \********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/assets/styles/app.scss":
/*!******************************************!*\
  !*** ./resources/assets/styles/app.scss ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ (function(module) {

module.exports = window["jQuery"];

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["styles/app","styles/admin","styles/vendors/lightgallery","/scripts/vendor"], function() { return __webpack_exec__("./resources/assets/scripts/app.js"), __webpack_exec__("./resources/assets/styles/vendors/lightgallery.scss"), __webpack_exec__("./resources/assets/styles/admin.scss"), __webpack_exec__("./resources/assets/styles/app.scss"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=app.js.map