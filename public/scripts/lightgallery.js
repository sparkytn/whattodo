"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunk"] = self["webpackChunk"] || []).push([["lightgallery"],{

/***/ "./resources/assets/scripts/components/lightgallery.js":
/*!*************************************************************!*\
  !*** ./resources/assets/scripts/components/lightgallery.js ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var lightgallery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lightgallery */ \"./node_modules/lightgallery/lightgallery.es5.js\");\n\n(0,lightgallery__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(document.getElementById('lightgallery'), {\n  plugins: [lgZoom, lgThumbnail],\n  speed: 500\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvYXNzZXRzL3NjcmlwdHMvY29tcG9uZW50cy9saWdodGdhbGxlcnkuanMuanMiLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUdBQSx3REFBWSxDQUFDQyxRQUFRLENBQVJBLGNBQUFBLENBQUQsY0FBQ0EsQ0FBRCxFQUEwQztBQUNyREMsRUFBQUEsT0FBTyxFQUFFLFNBRDRDLFdBQzVDLENBRDRDO0FBRXJEQyxFQUFBQSxLQUFLLEVBQUU7QUFGOEMsQ0FBMUMsQ0FBWkgiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL3NjcmlwdHMvY29tcG9uZW50cy9saWdodGdhbGxlcnkuanM/ZWRiYyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgbGlnaHRHYWxsZXJ5IGZyb20gXCJsaWdodGdhbGxlcnlcIjtcblxuXG5saWdodEdhbGxlcnkoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xpZ2h0Z2FsbGVyeScpLCB7XG5cdHBsdWdpbnM6IFtsZ1pvb20sIGxnVGh1bWJuYWlsXSxcblx0c3BlZWQ6IDUwMCxcbn0pO1xuIl0sIm5hbWVzIjpbImxpZ2h0R2FsbGVyeSIsImRvY3VtZW50IiwicGx1Z2lucyIsInNwZWVkIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./resources/assets/scripts/components/lightgallery.js\n");

/***/ })

}]);