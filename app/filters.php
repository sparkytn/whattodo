<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return ' &hellip;';
});

add_filter( 'excerpt_length', function () {
    return 15;
} );

/**
 * Add support for WooCommerce plugins templates.
 */
add_filter('sage-woocommerce/templates', function ($paths) {
	$paths[] = WP_PLUGIN_DIR . '/yith-woocommerce-booking-premium/templates/';
	return $paths;
});

/**
 * Add support for WooCommerce plugins templates.
 */
add_filter('sage-woocommerce/templates', function ($paths) {
	$paths[] = WP_PLUGIN_DIR . '/yith-woocommerce-multi-vendor-premium/templates/';
	return $paths;
});
