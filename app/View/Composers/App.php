<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class App extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        '*',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'siteName' => $this->siteName(),
            'activitiesCategories' => $this->activitiesCategories(),
            'activitiesRegions' => $this->activitiesRegions(),
            'shopURL' => $this->shopURL(),
        ];
    }

    /**
     * Returns the site name.
     *
     * @return string
     */
    public function siteName()
    {
        return get_bloginfo('name', 'display');
    }


    /**
     * Returns the all categories of product.
     *
     * @return \WP_Term[]
     */
    public function activitiesCategories()
    {
        return get_terms( 'product_cat');
    }


    /**
     * Returns the all regions of product.
     *
     * @return \WP_Term[]
     */
    public function activitiesRegions()
    {
        return get_terms( 'region');
    }


    /**
     * Returns shop Link
     *
     * @return string
     */
    public function shopURL()
    {
        return get_permalink( wc_get_page_id( 'shop' ) );
    }
}
