<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;
use function App\Helpers\wtd_transiants;

class FrontPage extends Composer
{

	protected $transientKey = 'front-page';
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'activitiesRegions' => $this->activitiesRegions(),
            'testimonials' => $this->testimonials(),
//            'faq' => $this->faq(),
//            'featuredPosts' => $this->featuredPosts(),
        ];
    }


    /**
     * Returns the all regions of product.
     *
     * @return \WP_Term[]
     */
    public function activitiesRegions()
    {
		$key = 'regions_list__term';
        return getTermsDataWTD($key, 'region', 20);
    }


    /**
     * Returns shop Link
     *
     * @return string
     */
    public function testimonials()
    {
		$key = 'reviews_homepage';
		$args = array(
			'status' => 'approve',
			'type' => 'review',
			'number' => 3,
		);

        return getReviewsDataWTD($key, $args,7  );
    }

//    /**
//     * Returns shop Link
//     *
//     * @return string
//     */
//    public function faq()
//    {
//        return get_permalink( wc_get_page_id( 'shop' ) );
//    }

//    /**
//     * Returns shop Link
//     *
//     * @return string
//     */
//    public function featuredPosts()
//    {
//        return get_permalink( wc_get_page_id( 'shop' ) );
//    }
}
