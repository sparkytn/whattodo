<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;
use App\Helpers\TransientManager;

class ActivityCarousel extends Component
{
	/**
	 * @var string
	 */
	public $type;

	/**
	 * @var null
	 */
	public $term;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var mixed|\WP_Query
	 */
	public $activities;

	/**
	 * @var string
	 */
	public $btn;
	/**
	 * @var string
	 */
	public $class;


	/**
	 * Create a new component instance.
	 *
	 * @param string $type type of activity random, feature, category
	 * @param string $title
	 * @param null $btn
	 * @param null $term
	 */
    public function __construct( $type = 'rand', $term = null , $title = 'D\'autres activités', $btn = null, $class = '' )
    {
        $this->type = $type;
        $this->term = $term;
        $this->title = $title;
		$this->btn = $btn;
		if($this->btn === null){
			$this->btn = [
				'link' => get_permalink( wc_get_page_id( 'shop' ) ),
				'title' =>  __('Voir toutes les activités ')
			];
		}

        $this->class = $class;
        $this->activities = $this->get_activities();
    }


    public function get_query_args() {

    	$tax_args = [];

    	if($this->type == 'featured'){
    		$tax_args = [
				'taxonomy' => 'product_visibility',
				'field'    => 'name',
				'terms'    => 'featured',
				'operator' => 'IN',
			];
		}

    	if($this->type == 'category') {
			$tax_args = [
				'taxonomy'      => 'product_cat',
				'field' 			=> 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => $this->term,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			];
		}

		return [
			'post_type'           => 'product',
			'no_found_rows' 	  => true,
			'post_status'         => 'publish',
			'posts_per_page'      => 10,
			'orderby'			  => 'rand',
			'tax_query'           => [ $tax_args ],
		];
	}


	public function get_activities() {
		$key = 'activities_list__' . $this->type;
		$query_args = $this->get_query_args();

		return getQueryDataWTD($key, $query_args, 3);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.activity-carousel');
    }
}
