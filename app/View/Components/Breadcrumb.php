<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;
use Log1x\Crumb\Facades\Crumb;

class Breadcrumb extends Component
{
	/**
	 * Create a new component instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * The breadcrumb items.
	 *
	 * @return string
	 */
	public function items()
	{


		$items = Crumb::build()->toArray();



		$post_type = get_query_var('taxonomy');
		//dd( $post_type);

		$count = count($items);
		if ( is_product() ) {

			$terms = wc_get_product_terms(
				get_the_ID(),
				'product_cat',
				apply_filters(
					'woocommerce_breadcrumb_product_terms_args',
					array(
						'orderby' => 'parent',
						'order'   => 'DESC',
					)
				)
			);

			if ( $terms ) {
				$main_term = apply_filters( 'woocommerce_breadcrumb_main_term', $terms[0], $terms );
				$shop_page_id = wc_get_page_id( 'shop' );

				$items[$count]['label'] = $items[$count-1]['label'];
				$items[$count]['url'] = $items[$count-1]['url'];

				$items[$count - 2 ]['label'] = get_the_title( $shop_page_id );

				$items[$count - 1 ]['label'] = $main_term->name;
				$items[$count - 1 ]['url'] = get_term_link( $main_term );
			}
		}

		if ( is_tax() ) {
			$shop_page_id = wc_get_page_id( 'shop' );
			$items[$count]['label'] = $items[$count-1]['label'];
			$items[$count]['url'] = $items[$count-1]['url'];

			$items[$count - 1 ]['label'] = get_the_title( $shop_page_id );
			$items[$count - 1 ]['url'] = get_permalink( $shop_page_id );
		}

		if(is_post_type_archive()){

			$shop_page_id = wc_get_page_id( 'shop' );
			$items[$count-1]['label'] = get_the_title( $shop_page_id );
		}

		return $items;
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\View\View|string
	 */
	public function render()
	{
		return $this->view('components.breadcrumb');
	}
}
