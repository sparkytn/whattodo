<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Alert extends Component
{
    /**
     * The alert type.
     *
     * @var string
     */
    public $type;

    /**
     * The alert message.
     *
     * @var string
     */
    public $message;

    /**
     * The alert title.
     *
     * @var string
     */
    public $title;

    /**
     * Alert with container.
     *
     * @var boolean
     */
    public $full;

    /**
     * Create the component instance.
     *
     * @param string $type
     * @param string $message
     * @param string $title
     * @param bool $full
     */
    public function __construct($type = 'primary', $message = null, $title = null, $full = true)
    {
        $this->title = $title;
        $this->type = $type;
        $this->message = $message;
        $this->full = $full;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.alert');
    }
}
