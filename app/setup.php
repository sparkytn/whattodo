<?php

/**
 * Theme setup.
 */

namespace App;

use function Roots\asset;

/**
 * Register the theme assets.
 *
 * @return void
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('sage/vendor.js', asset('scripts/vendor.js')->uri(), ['jquery'], null, true);
    wp_enqueue_script('sage/app.js', asset('scripts/app.js')->uri(), ['sage/vendor.js', 'jquery'], null, true);

    wp_add_inline_script('sage/vendor.js', asset('scripts/manifest.js')->contents(), 'before');

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if(is_product()){
		wp_enqueue_style('sage/lg.css', asset('styles/vendors/lightgallery.css')->uri(), false, null);
	}

    wp_enqueue_style('sage/app.css', asset('styles/app.css')->uri(), false, null);

}, 100);

/**
 * Register the theme assets with the block editor.
 *
 * @return void
 */
add_action('enqueue_block_editor_assets', function () {
    if ($manifest = asset('scripts/manifest.asset.php')->get()) {
        wp_enqueue_script('sage/vendor.js', asset('scripts/vendor.js')->uri(), $manifest['dependencies'], null, true);
        wp_enqueue_script('sage/editor.js', asset('scripts/editor.js')->uri(), ['sage/vendor.js'], null, true);

        wp_add_inline_script('sage/vendor.js', asset('scripts/manifest.js')->contents(), 'before');
    }

    wp_enqueue_style('sage/editor.css', asset('styles/editor.css')->uri(), false, null);
}, 100);

/**
 * Register the initial theme setup.
 *
 * @return void
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil', [
		'clean-up' => ['disable_gutenberg_block_css' => false],
        'nav-walker',
		'disable-trackbacks',
		'js-to-footer',
        'nice-search',
        'relative-urls'
    ]);

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
		'activity_cat_nav' =>__('Activity Category Menu', 'sage'),
		'activity_region_nav' =>__('Activity Region Menu', 'sage'),
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Add theme support for Wide Alignment
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#wide-alignment
     */
    add_theme_support('align-wide');


    /**
     * Enable responsive embeds
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
     */
    add_theme_support('responsive-embeds');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', [
        'caption',
        'comment-form',
        'comment-list',
        'gallery',
        'search-form',
        'script',
        'style'
    ]);

	/**
	 * Enable WooCommerce Light Box in product
	 */
	//add_theme_support('wc-product-gallery-lightbox');

	/**
	 * Image sizes
	 */
	add_image_size( 'woocommerce_thumbnail', 410, 200, true );
	add_image_size( 'woocommerce_single', 830, 400, true );
	add_image_size( 'category_thumbnail', 500, 500, true );

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Enable theme color palette support
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-color-palettes
     */

}, 20);

/**
 * Register the theme sidebars.
 *
 * @return void
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h4 class="accordion-header mb-4">',
        'after_title' => '</h4>'
    ];
	$footerConfig = [
		'before_widget' => '<section class="footer-widget %1$s %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="mb-3 title h6">',
		'after_title' => '</h3>'
	];

	register_sidebar([
			'name' => __('Shop Sidebar', 'sage'),
			'id' => 'sidebar-shop'
	] + $config);

	register_sidebar([
			'name' => __('Product Sidebar', 'sage'),
			'id' => 'sidebar-product'
	] + $config);

    register_sidebar([
        'name' => __('Primary', 'sage'),
        'id' => 'sidebar-primary'
    ] + $config);

    register_sidebar([
        'name' => __('Footer', 'sage'),
        'id' => 'sidebar-footer'
    ] + $config);


	register_sidebar([
			'name'          => __('Footer Area 2', 'sparky'),
			'id'            => 'widget-footer-area-2'
		] + $footerConfig);

	register_sidebar([
			'name'          => __('Footer Area 1', 'sparky'),
			'id'            => 'widget-footer-area-1'
		] + $footerConfig);

});
