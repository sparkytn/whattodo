<?php

/**
 * Get the data from transient or DB
 *
 * @param string $key
 * @param array $query_args
 * @param int $ttl Days
 * @return mixed|\WP_Query
 */

function getQueryDataWTD($key, $query_args, $ttl = 1)
{
	$daySeconds = 86000;

	$data = get_transient($key);

	if( ! $data ){
		$data = new \WP_Query($query_args);
		set_transient($key, $data, $daySeconds * $ttl);
	}

	return $data;

}

function getTermsDataWTD($key, $terms, $ttl = 1)
{
	$daySeconds = 86000;
	$data = get_transient($key);
	if( ! $data ){
		$data = get_terms( $terms );
		set_transient($key, $data, $daySeconds * $ttl);
	}

	return $data;
}


function getReviewsDataWTD($key, $args, $ttl = 1)
{
	$daySeconds = 86000;
	$data = get_transient($key);
	if( ! $data ){
		$comments_query = new WP_Comment_Query;
		$data =  $comments_query->query( $args );
		set_transient($key, $data, $daySeconds * $ttl);
	}


	return $data;
}
