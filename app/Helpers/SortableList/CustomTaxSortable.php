<?php
/**
 * Class for making post types sortable.
 * Adds menu item for sorting posts.
 * They are not paged, so better not to add them to a post type
 * that will contain hundreds of items. More like something little used.
 *
 *		Define page template through:
 *			$sort = new dhfSortable($postType, $title)
 *              e.g.
 *                      $resources = new dhfSortable('resources', 'Resources');
 *              where 'resources' is a custom post type you have already defined.
 *
 * This class was heavily influenced by http://soulsizzle.com/jquery/create-an-ajax-sorter-for-wordpress-custom-post-types/
 */

namespace App\Helpers\SortableList;

class CustomTaxSortable extends SortableEntryList {
	var $postType = '';
	var $taxonomy = '';
	var $title = '';

	function __construct($postType, $tax, $title) {
		parent::__construct();

		$this->postType = $postType;
		$this->taxonomy = $tax;
		$this->title = $title;

		add_action('admin_menu' , array( $this, 'enable_sort' ));
		add_action('wp_ajax_'.$this->taxonomy.'_sort', array( $this, 'save_sort_order' ));
	}


	/**
	 * Add Sort menu
	 */
	function enable_sort() {

		add_submenu_page('edit.php?post_type=' . $this->postType, 'Sort '. $this->title, '✩ Sort '. $this->title, 'edit_posts', basename(__FILE__), array( $this, 'sort'));
	}

	/**
	 * Display Sort admin page
	 */
	function sort() {

		// Query pages.
		$args = array(
			'taxonomy' => $this->taxonomy,
			'meta_key' => 'term_order',
			'orderby' => 'meta_value',
			'order'         => 'ASC',
			'hide_empty' => false,
			'hierarchical' => false,
			'parent' => 0,
		);

		$term = get_terms($args);

		?>
		<div class="wrap">

			<div id="icon-edit" class="icon32"></div>
			<h2>Sort <?php echo $this->title; ?> <img src="<?php bloginfo('url'); ?>/wp-admin/images/loading.gif" id="loading-animation" /></h2>

			<ul id="sortable-list" data-type="<?=$this->taxonomy?>">
				<?php foreach( $term as $category ) { ?>

					<li id="<?php echo $category->term_id ?>">  <?php echo  $category->name; ?></li>

				<?php } ?>
			</ul>

		</div><!-- #wrap -->

		<?php
	}


	/**
	 * Save the sort order to database
	 */
	function save_sort_order() {
		$order = explode(',', $_POST['order']);
		$counter = 0;
		foreach ($order as $term_id) {
			update_term_meta( $term_id, 'term_order', $counter );
			$counter ++ ;

		}
		clean_term_cache($order, $this->taxonomy);
		wp_send_json([
			'success' => true,
		]);
	}

}
