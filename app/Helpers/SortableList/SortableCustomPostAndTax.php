<?php
/**
 * Class for making post types sortable.
 * Adds menu item for sorting posts.
 * They are not paged, so better not to add them to a post type
 * that will contain hundreds of items. More like something little used.
 *
 *        Define page template through:
 *            $sort = new dhfSortable($postType, $title)
 *              e.g.
 *                      $resources = new dhfSortable('resources', 'Resources');
 *              where 'resources' is a custom post type you have already defined.
 *
 * This class was heavily influenced by http://soulsizzle.com/jquery/create-an-ajax-sorter-for-wordpress-custom-post-types/
 */

namespace App\Helpers\SortableList;


class SortableCustomPostAndTax {

	public $postType = '';
	public $title = '';
	public $taxTerm = '';
	public $isTax = '';
	public $menu_slug = '';
	private $assets = '/app/Helpers/SortableList/assets/';

	function __construct($postType, $tax, $title, $isTax = false)
	{
		$this->postType = $postType;
		$this->title = $title;
		$this->taxTerm = $tax;
		$this->isTax = $isTax;

		$action = $this->isTax ? $this->taxTerm : $this->postType ;

		$this->menu_slug = 'sorting_' . $this->postType;
		if ($this->isTax) {
			$this->menu_slug = 'sorting_' . $this->taxTerm;
		}

		add_action('admin_menu', array($this, 'enable_sort'));
		add_action('wp_ajax_' . $action . '_sort', array($this, 'save_sort_order'));

		if( isset($_GET['page']) && $_GET['page'] === $this->menu_slug ){
			add_action('admin_print_styles', array( $this, 'enqueue_styles' ));
			add_action('admin_print_scripts', array( $this, 'enqueue_scripts' ));
		}


	}


	/**
	 * Add Sort menu
	 */
	function enable_sort()
	{

		add_submenu_page('edit.php?post_type=' . $this->postType, 'Sort Posts', '✩ Sort ' . $this->title, 'edit_posts', $this->menu_slug, array($this, 'sort'));
	}

	/**
	 * Display Sort admin page
	 */
	function sort()
	{
		if ($this->isTax) {

			$args = array(
				'taxonomy' => $this->taxTerm,
				'meta_key' => 'term_order',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'hide_empty' => false,
				'hierarchical' => false,
				'parent' => 0,
			);

			$entries = get_terms($args);
		} else {
			$entries = get_posts('post_type=' . $this->postType . '&posts_per_page=-1&orderby=menu_order&order=ASC');
		}

		?>
		<div class="wrap">

			<div id="icon-edit" class="icon32"></div>
			<h2>Sort <?php echo $this->title; ?> <img src="<?php bloginfo('url'); ?>/wp-admin/images/loading.gif" id="loading-animation"/></h2>

			<ul id="sortable-list" data-type="<?= $this->isTax ? $this->taxTerm : $this->postType ?>">
				<?php
				foreach ($entries as $entry) {
					$terms = '';
					if ($this->isTax) {
						$entry_id = $entry->term_id;
						$entry_name = $entry->name;
					} else {
						$terms = get_the_term_list($entry->ID, $this->taxTerm, ' —— <small>', ' - ', '</small>');
						$entry_id = $entry->ID;
						$entry_name = $entry->post_title;
					}
					echo "<li id='$entry_id'>$entry_name $terms</li>";
				}
				?>
			</ul>

		</div><!-- #wrap -->

		<?php
	}


	/**
	 * Save the sort order to database
	 */
	function save_sort_order()
	{

		$order = explode(',', $_POST['order']);
		$counter = 0;

		foreach ($order as $entry_id) {
			if ($this->isTax) {
				update_term_meta($entry_id, 'term_order', $counter);
			} else {
				wp_update_post(
					[
						'ID' => $entry_id,
						'menu_order' => $counter
					]);
			}
			$counter++;
		}
		if ($this->isTax) {
			clean_term_cache($order, $this->taxTerm);
		}

		wp_send_json(
			[
				'success' => true,
			]);
	}


	/**
	 * Add CSS to admin
	 */
	function enqueue_styles() {
		global $typenow;
		$pages = array($this->postType);
		if (in_array($typenow, $pages)){
			wp_enqueue_style('cpt-sort-css', get_stylesheet_directory_uri() . $this->assets .'cpt-sort.css');
		}
	}

	/**
	 * Add JS to admin
	 */
	function enqueue_scripts() {
		global $typenow;


		$pages = array($this->postType);
		if (in_array($typenow, $pages)){
			wp_enqueue_script('jquery-ui-sortable');
			wp_enqueue_script('cpt-sort-js', get_stylesheet_directory_uri() . $this->assets .'cpt-sort.js');
		}
	}

}

