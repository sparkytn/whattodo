<?php

namespace App\Modules;

/*
|--------------------------------------------------------------------------
| Custom Activity Class ( Submission + Admin view )
|--------------------------------------------------------------------------
|
| CPT in Poet config file
*/

class CustomActivityForm
{

	public static $cpt = 'custom_activity_req';

	public function __construct() {

		add_action('wp_ajax_nopriv_custom_activity_req', [$this, 'save_form']);
		add_action('wp_ajax_custom_activity_req', [$this, 'save_form']);

		add_filter( 'manage_custom_activity_req_posts_columns', [$this, 'custom_columns_head'] );
		add_action( 'manage_custom_activity_req_posts_custom_column', [$this, 'custom_columns_value'], 10, 2 );

		add_action( 'add_meta_boxes', [$this, 'add_meta_box'] );
	}


	/**
	 * Save Contact Form
	 *
	 * @return string|string[]
	 */
	function save_form() {


		if ( $this->validateCaptcha() ) {
			return  wp_send_json([
				'success' => false,
				'message' => 'google_captcha'
			]);
		}

		check_ajax_referer( 'custom_activity_req', 'nonce' );

		if( ! isset($_POST['name']) &&
			! isset($_POST['phone']) &&
			! isset($_POST['email']) &&
			! isset($_POST['activity_type']) &&
			! isset($_POST['pax']) &&
			! isset($_POST['budget']) &&
			! isset($_POST['details'])
		){
			return wp_send_json([
				'success' => false,
				'message' => 'required_fields',
			]);
		}


		if (! is_email($_POST['email'])) {
			return wp_send_json([
				'success' => false,
				'message' => 'invalid_email',
				'email' => $_POST['email'],
			]);
		}


		$name = sanitize_text_field($_POST['name']);
		$activity_type = sanitize_text_field($_POST['activity_type']);
		$email = sanitize_email($_POST['email']);
		$phone = sanitize_text_field($_POST['full_phone']);
		$budget = sanitize_text_field($_POST['budget']);
		$pax = sanitize_text_field($_POST['pax']);
		$details = sanitize_textarea_field($_POST['details']);

		$post_meta_data['name'] = $name;
		$post_meta_data['email'] = $email;
		if($phone){
			$post_meta_data['phone'] = $phone;
		}
		$post_meta_data['activity_type'] = $activity_type;
		$post_meta_data['budget'] = $budget;
		$post_meta_data['pax'] = $pax;
		$post_meta_data['details'] = $details;

		$args = array(
			'post_author' => 1,
			'post_title' => 'Demande de '.$name,
			'post_status' => 'publish',
			'post_type' => self::$cpt,
		);

		$postID = wp_insert_post($args);

		if ($postID === 0) {
			return wp_send_json([
				'success' => false,
				'message' => 'not_inserted'
			]);
		}

		update_post_meta($postID, self::$cpt ,$post_meta_data);

		$to = get_field( 'support_email', 'option' ) ? get_field( 'support_email', 'option' ) : get_bloginfo('admin_email');
		$subject = 'WTD Custom Activity Request  #'.$postID.' - '.$name;

		$headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>';
		$headers[] = 'Reply-To: '.$name.' <'.$email.'>';
		$headers[] = 'Content-Type: text/html: charset=UTF-8';
		$body  = "<h3>Demande d'activité personnalisée</h3>";
		$body .= "Pour voir le messge dans le back office du site <a href='".get_edit_post_link($postID)."' target='_blank'>cliquer ici</a><br>";
		$body .= "<p>- Nom et prénom : <b>" . $name . "</b><br>";
		if($phone) {
			$body .= "- Téléphone : <b>" . $phone . "</b><br>";
		}
		$body .= "- Type d'activité : <b>" . $activity_type . "</b><br>";
		$body .= "- Budget : <b>" . $budget . "</b><br>";
		$body .= "- Nb. de personnes : <b>" . $pax . "</b></p>";
		$body .= '<p> Détail : <br>'.$details.'</p>';

		wp_mail($to, $subject, $body, $headers);

		return wp_send_json([
			'success' => true,
			'message' => 'ca_saved'
		]);
	}



	function custom_columns_head( $columns ){
		$newColumns = array();
		$newColumns['cb'] = $columns['cb'];
		$newColumns['status'] = 'Status';
		$newColumns['title'] = $columns['title'];
		$newColumns['type'] = 'Activity Type';
		$newColumns['budget'] = 'Budget';
		$newColumns['pax'] = 'Persons';
		$newColumns['details'] = 'Details';
		$newColumns['date'] = 'Date';
		return $newColumns;
	}



	function custom_columns_value( $column, $post_id ){

		$metaData = get_post_meta($post_id, self::$cpt, true);
		switch( $column ) {
			case 'status' :
				$status = get_field('status', $post_id ) ?? [ 'value' => 'new', 'label' => 'New'];
				// @todo : style in stylesheet
				echo '<style>.column-status { width: 90px; !important;}</style>';
				echo '<style>.car-label{display: inline-block;width: 10px;height: 10px; margin-right: 2px; border-radius: 50%; background: #888;}</style>';
				echo '<style>.car-label-new{background: #8bff3c;}</style>';
				echo '<style>.car-label-processing{background: orange;}</style>';
				echo '<style>.car-label-waiting-client{background: orchid;}</style>';
				echo '<style>.car-label-closed-not-serious{background: #222;}</style>';
				echo '<style>.car-label-cancelled{background: red;}</style>';
				echo '<style>.car-label-ordered{background: #00ba88;}</style>';
				echo '<style>.car-label-finished{background: #0c63e4;}</style>';
				echo "<span class='car-label car-label-{$status['value']}'></span><span style='font-size: 10px;font-weight:bold;'> {$status['label']} </span>";
				break;
			case 'type' :
				echo $metaData['activity_type'];
				break;
			case 'budget' :
				echo $metaData['budget'];
				break;
			case 'pax' :
				echo $metaData['pax'] . ' Pax';
				break;
			case 'details' :
				echo substr($metaData['details'], 0, 40) ;
				break;
		}
	}



	function add_meta_box() {
		add_meta_box( 'contact_req_details', 'Rquest Details', [$this, 'metabox_callback'], self::$cpt, 'normal','high' );
	}


	function metabox_callback( $post ) {

		$metaData = get_post_meta($post->ID, self::$cpt, true);
		echo '<ul>';
		echo "<li>ID : <b>#{$post->ID}</b></li>";
		foreach ($metaData as $key => $data){
			if($key !== 'details'){
				echo "<li> $key : <b> $data </b></li>" ;
			}
		}
		echo nl2br("<li> Details  : <br><b> $data </b></li>");
		echo '</ul>';
	}


	/**
	 * Validate Google Captcha
	 *
	 * @return boolean
	 */

	public function validateCaptcha() {
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$secretKey = "6LeQ4nYfAAAAAF1h4VN5mBJHw66p3fWUGig0rgjk";
		$ip = $_SERVER['REMOTE_ADDR'];

		$captchaData = [
			'secret' => $secretKey,
			'response' => $_POST['token']
		];

		$contextOptions = [
			'http' => [
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($captchaData)
			]
		];
		$context  = stream_context_create($contextOptions);
		$response = file_get_contents($url, false, $context);
		$responseKeys = json_decode($response,true);

		return !$responseKeys["success"] && $responseKeys["score"] > 0.5;
	}



}

