<?php

namespace App\Modules;

/*
|--------------------------------------------------------------------------
| LazyLoader Class mange the lazy loading of the images and iFrames
|--------------------------------------------------------------------------
| 	Front dependency : https://github.com/verlok/vanilla-lazyload/
|	Add js script : import LazyLoad from 'vanilla-lazyload';
*/

class LazyLoader
{

	public function __construct()
	{
		// filter content
		add_filter( 'the_content', [$this, 'filter_images'], 200 );
		add_filter( 'post_thumbnail_html', [$this, 'filter_images'], 200 );
		add_filter( 'widget_text', [$this, 'filter_images'], 200 );

		// filter iframe html
		add_filter('oembed_result', [$this, 'filter_iframes'], 200);
		add_filter('embed_oembed_html', [$this, 'filter_iframes'], 200);

		// filter get attachment $attr
		add_filter('wp_get_attachment_image_attributes', [$this, 'attachment_image']);

	}



	/**
	 * Add Lazy class to the image tag and replace src/srcset to data-*
	 *
	 * @param $content
	 * @return string|string[]
	 */

	function filter_images($content) {

		if ( is_feed()
			|| intval( get_query_var( 'print' ) ) == 1
			|| intval( get_query_var( 'printpage' ) ) == 1
			|| strpos( $_SERVER['HTTP_USER_AGENT'], 'Opera Mini' ) !== false
		) {
			return $content;
		}

		$matches = array();
		$skip_images_regex = '/class=".*lazy.*"/';
		$placeholder_image = apply_filters( 'lazysizes_placeholder_image', 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==' );
		preg_match_all( '/<img\s+.*?>/', $content, $matches );

		$search = array();
		$replace = array();

		foreach ( $matches[0] as $imgHTML ) {

			// Don't to the replacement if a skip class is provided and the image has the class.
			if ( ! ( preg_match( $skip_images_regex, $imgHTML ) ) ) {

				$replaceHTML = preg_replace( '/<img(.*?)src=/i',
					'<img$1src="' . $placeholder_image . '" data-src=', $imgHTML );

				if( preg_match('/class="([^"]*)"/', $replaceHTML )){
					$replaceHTML = preg_replace( '/class="/i', 'class="lazy ', $replaceHTML );
				}else {
					$replaceHTML = preg_replace( '/ src="/i', ' class="lazy" src="' , $replaceHTML );
				}



				array_push( $search, $imgHTML );
				array_push( $replace, $replaceHTML );
			}
		}

		$content = str_replace( $search, $replace, $content );

		return $content;
	}



	/**
	 * Add Lazy class to the iframe html
	 *
	 * @param $html
	 * @return mixed
	 */

	function filter_iframes( $html ) {
		if ( false === strpos( $html, 'iframe' ) ) {
			return $html;
		}

		$html = preg_replace( '/<iframe /i', '<iframe class="lazy" ' , $html );
		return $html;
	}


	/**
	 * Add Lazy to the $attr of the image and replace the src and srcset
	 *
	 * @param $attr
	 * @return mixed
	 */

	function attachment_image($attr) {
		$attr['data-src'] = $attr['src'] ;
		unset($attr['src']);
		$attr['class'] .= ' lazy';

		if( array_key_exists('srcset', $attr) ){
			$attr['data-srcset'] = $attr['srcset'];
			unset($attr['srcset']);
		}
		return $attr;
	}

}
