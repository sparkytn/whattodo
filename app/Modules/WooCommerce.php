<?php


namespace App\Modules;


class WooCommerce
{

	public function __construct()
	{
		add_action('woocommerce_before_reset_password_form', [$this, 'wrapper_before_wordpress_reset_password'],20);
		add_action('woocommerce_before_lost_password_form', [$this, 'wrapper_before_wordpress_reset_password'],20);
		add_action('woocommerce_before_lost_password_confirmation_message', [$this, 'wrapper_before_wordpress_reset_password'],20);
		add_action('woocommerce_after_reset_password_form', [$this, 'wrapper_after_wordpress_reset_password'],20);
		add_action('woocommerce_after_lost_password_form', [$this, 'wrapper_after_wordpress_reset_password'],20);
		add_action('woocommerce_after_lost_password_confirmation_message', [$this, 'wrapper_after_wordpress_reset_password'],20);
	}

	public function wrapper_before_wordpress_reset_password()
	{
		echo  '<div class="page-body"><div class="container p-0"><div class="page-content-card mb-5"><div class="row justify-content-center"><div class="col-md-8">';

	}

	public function wrapper_after_wordpress_reset_password()
	{
		echo  '</div></div></div></div></div>';

	}

}
