<?php

acf_add_local_field_group(array(
	'key' => 'group_625d1dc37fd9a',
	'title' => 'Activity Label',
	'fields' => array(
		array(
			'key' => 'field_625d1ddcdd6cc',
			'label' => 'Label',
			'name' => 'label',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'label',
			),
			'taxonomy' => '',
			'allow_null' => 1,
			'multiple' => 0,
			'max' => '',
			'return_format' => 'id',
			'acfe_add_post' => 0,
			'acfe_edit_post' => 0,
			'save_custom' => 0,
			'save_post_status' => 'publish',
			'acfe_bidirectional' => array(
				'acfe_bidirectional_enabled' => '0',
			),
			'acfe_field_group_condition' => 0,
			'ui' => 1,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
			array(
				'param' => 'current_user_role',
				'operator' => '==',
				'value' => 'shop_manager',
			),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
			array(
				'param' => 'current_user_role',
				'operator' => '==',
				'value' => 'administrator',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'side',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
	'acfe_autosync' => '',
	'acfe_form' => 0,
	'acfe_display_title' => '',
	'acfe_meta' => '',
	'acfe_note' => '',
));

