<?php

if( ! function_exists('acf_add_local_field_group') ){
	return ;
}

require_once 'acf-config.php';

require_once 'acf-homepage.php';
require_once 'acf-fields-custom-activity.php';
require_once 'acf-fields-activity-detail.php';
require_once 'acf-fields-label.php';
require_once 'acf-fields-badge.php';
