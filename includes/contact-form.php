<?php
/*
|--------------------------------------------------------------------------
| Contact Form
| Content Type : sparky_contact_form see config/poet.php
|--------------------------------------------------------------------------
|	todo : move the email html to template
 */


add_action('wp_ajax_nopriv_sparky_save_user_contact_form', 'sparky_save_contact_form');
add_action('wp_ajax_sparky_save_user_contact_form', 'sparky_save_contact_form');

function sparky_save_contact_form()
{
	check_ajax_referer( 'sparky_save_user_contact_form', 'nonce_data' );
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$secretKey = "6LeQ4nYfAAAAAF1h4VN5mBJHw66p3fWUGig0rgjk";
	$ip = $_SERVER['REMOTE_ADDR'];

	$captchaData = [
		'secret' => $secretKey,
		'response' => $_POST['token']
	];
	$contextOptions = [
		'http' => [
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($captchaData)
		]
	];
	$context  = stream_context_create($contextOptions);
	$response = file_get_contents($url, false, $context);
	$responseKeys = json_decode($response,true);

	if ( !$responseKeys["success"] && $responseKeys["score"] >! 0.5) {
		wp_send_json([
			'success' => false,
			'message' => 'google_captcha',
			'score' => $responseKeys["score"],
		]);
	}

	if( ! isset($_POST['subject']) &&
		! isset($_POST['full_name']) &&
		! isset($_POST['email']) &&
		! isset($_POST['full_phone']) &&
		! isset($_POST['message'])
	){
		wp_send_json([
			'success' => false,
			'message' => 'required_fields',
		]);
	}


	if (! is_email($_POST['email'])) {
		wp_send_json([
			'success' => false,
			'message' => 'invalid_email',
			'email' => $_POST['email'],
		]);
	}

	$title = sanitize_text_field($_POST['subject']);
	$full_name = sanitize_text_field($_POST['full_name']);
    $email = sanitize_email($_POST['email']);
    $phone = sanitize_text_field($_POST['full_phone']);
    $message = sanitize_textarea_field($_POST['message']);

    $post_meta_data['_contact_subject_value_key'] = $title;
    $post_meta_data['_contact_email_value_key'] = $email;
    $post_meta_data['_contact_full_name_value_key'] = $full_name;
    $post_meta_data['_contact_message_value_key'] = $message;
    if($phone){
		$post_meta_data['_contact_phone_value_key'] = $phone;
	}

    $args = array(
		'post_author' => 1,
		'post_status' => 'publish',
		'post_type' => 'sparky_contact_form',
		'meta_input' => $post_meta_data
	);

    $postID = wp_insert_post($args);

    if ($postID === 0) {
		wp_send_json([
			'success' => false,
			'message' => $postID
		]);
	}

	$to = get_field( 'support_email', 'option' ) ? get_field( 'support_email', 'option' ) : get_bloginfo('admin_email');
	$subject = 'WTD Contact - '.$title;

	$headers[] = 'From: '.get_bloginfo('name').' <'.$to.'>';
	$headers[] = 'Reply-To: '.$title.' <'.$email.'>';
	$headers[] = 'Content-Type: text/html: charset=UTF-8';
	$body  = "<h3>Message envoyer depuis le formulaire de contact</h3>";
	$body .= "Pour voir le messge dans le back office du site <a href='".get_edit_post_link($postID)."' target='_blank'>cliquer ici</a><br>";
	$body .= "Nom et prénom : <b>" . $full_name . "</b><br>";
	if($phone) {
		$body .= "Téléphone : <b>" . $phone . "</b><br>";
	}
	$body .= "Sujet : <b>" . $title . "</b><br>";
	$body .= '<p>'.$message.'</p>';

	wp_mail($to, $subject, $body, $headers);

	wp_send_json([
		'success' => true,
		'message' => 'contact_form_saved'
	]);
}






add_filter( 'manage_sparky_contact_form_posts_columns', 'sunset_set_contact_columns' );
function sunset_set_contact_columns( $columns ){
	$newColumns = array();
	$newColumns['cb'] = $columns['cb'];
	$newColumns['subject'] = 'Sujet';
	$newColumns['full_name'] = 'Full Name';
	$newColumns['message'] = 'Message';
	$newColumns['contact-details'] = 'Email & Phone';
	$newColumns['date'] = 'Date';
	return $newColumns;
}

add_action( 'manage_sparky_contact_form_posts_custom_column', 'sunset_contact_custom_column', 10, 2 );
function sunset_contact_custom_column( $column, $post_id ){

	switch( $column ){

		case 'subject' :
			echo get_post_meta( $post_id, '_contact_subject_value_key', true );
			break;
		case 'message' :
			echo get_post_meta( $post_id, '_contact_message_value_key', true );
			break;
		case 'contact-details' :
			$email = get_post_meta( $post_id, '_contact_email_value_key', true );
			$phone =  get_post_meta( $post_id, '_contact_phone_value_key', true );
			$phone = '<br> <a href="tel:'.$phone.'" target="_blank">'.$phone.'</a> .';
			echo '<a href="mailto:'.$email.'" target="_blank">'.$email.'</a>'. $phone;
			break;
		case 'full_name' :
			echo get_post_meta( $post_id, '_contact_full_name_value_key', true );
			break;
	}

}


/* CONTACT META BOXES */
add_action( 'add_meta_boxes', 'sunset_contact_add_meta_box' );

function sunset_contact_add_meta_box() {
	add_meta_box( 'contact_form_details', 'Message Details', 'sparky_contact_form_callback', 'sparky_contact_form', 'normal' );
}

function sparky_contact_form_callback( $post ) {

	$full_name = get_post_meta( $post->ID, '_contact_full_name_value_key', true );
	$email = get_post_meta( $post->ID, '_contact_email_value_key', true );
	$phone =  get_post_meta( $post->ID, '_contact_phone_value_key', true );
	$subject =  get_post_meta( $post->ID, '_contact_subject_value_key', true );
	$message = get_post_meta( $post->ID, '_contact_message_value_key', true );

	echo '<h3>Contact details :</h3>';
	echo '<p>Full Name : <b>'.$full_name.'</b></p>';
	echo '<p>Email : <b>'.$email.'</b> </p>';
	echo '<p>Phone : <b>'.$phone.'</b> </p>';
	echo '<h3>Message details :</h3>';
	echo '<h4>Subject : <b>'.$subject.'</b> </h4>';
	echo '<p>Message : </p> <p>'.$message.'</p>';
}






add_action( 'init', function () {
	register_post_status( 'processed', array(
		'label'                     => _x( 'Traité', 'Post status label', 'wtd' ),
		'public'                    => true,
		'label_count'               => _n_noop( 'Traité<span class="count">(%s)</span>', 'Traités <span class="count">(%s)</span>', 'wtd' ),
		'post_type'                 => array( 'sparky_contact_form' ), // Define one or more post types the status can be applied to.
		'show_in_admin_all_list'    => true,
		'_builtin'					=> true,
		'show_in_admin_status_list' => true,
		'show_in_metabox_dropdown'  => true,
		'show_in_inline_dropdown'   => true,
		'dashicon'                  => 'dashicons-yes-alt',
	) );
});


add_action('admin_footer-post.php', 'jc_append_post_status_list');
function jc_append_post_status_list(){
	global $post;
	$complete = '';
	$label = '';
	if($post->post_type == 'sparky_contact_form'){
		if($post->post_status == 'processed'){
			$complete = ' selected=\"selected\"';
			$label = 'Traité';
		}
		$value= '\"processed\"';
		echo '
          <script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value='. $value . $complete.'>Traité</option>");
               $(".misc-pub-section #post-status-display").append("'.$label.'");
          });
          </script>
          ';
	}
}

function jc_display_archive_state( $states ) {
	global $post;
	$arg = get_query_var( 'post_status' );
	if($arg != 'processed'){
		if($post->post_status == 'processed'){
			return array('Traité');
		}
	}
	return $states;
}
add_filter( 'display_post_states', 'jc_display_archive_state' );

