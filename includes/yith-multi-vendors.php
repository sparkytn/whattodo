<?php



/*
|--------------------------------------------------------------------------
| Disable the vendor store page on frontend
|--------------------------------------------------------------------------
|
 */

if( function_exists( 'YITH_Vendors' ) ){
	add_action('pre_get_posts', 'yith_disable_vendor_tax_archive');
	if( ! function_exists( 'yith_disable_vendor_tax_archive' ) ){
		function yith_disable_vendor_tax_archive($qry) {
			if ( ! is_admin() && is_tax( YITH_Vendors()->get_taxonomy_name() )){
				$qry->set_404();
			}
		}
	}
}


/*
|--------------------------------------------------------------------------
| remove vendor name in template
|--------------------------------------------------------------------------
|
 */
add_filter('yith_wcmv_show_vendor_name_template', function () {
	return false;
});



/*
|--------------------------------------------------------------------------
| Disable the vendor store page on frontend
|--------------------------------------------------------------------------
|
 */
// https://support.yithemes.com/hc/en-us/articles/115000113113
//$vendor = apply_filters( 'yith_new_vendor_registration_fields', $vendor );
add_filter('yith_new_vendor_registration_fields---------',function ($vendor) {

	//$vendor->code_tva = $_POST['yith_vendor_data']['code_tva'];
	?>

	<tr class="form-field yith-choosen">
		<th scope="row" valign="top">
			<label for="yith_vendor_location"><?php _e( 'Code TVA', 'yith-woocommerce-product-vendors' ); ?></label>
		</th>

		<td>
			<input type="text" class="regular-text" name="yith_vendor_data[code_tva]" id="yith_vendor_code_tva" placeholder="<?php _e( "Code TVA", 'yith-woocommerce-product-vendors' ); ?>" value="<?php echo $vendor->code_tva ?>" /><br />
		</td>
	</tr>
	<?php

	return $vendor;
},1);
