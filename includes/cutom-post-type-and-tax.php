<?php

// Register Custom Taxonomy
function sparky_custom_taxonomy_region()  {
	$labels = array(
		'name'                       => 'Regions',
		'singular_name'              => 'Region',
		'menu_name'                  => 'Regions',
		'all_items'                  => 'All Regions',
		'parent_item'                => 'Parent Region',
		'parent_item_colon'          => 'Parent Region:',
		'new_item_name'              => 'New Region Name',
		'add_new_item'               => 'Add New Region',
		'edit_item'                  => 'Edit Region',
		'update_item'                => 'Update Region',
		'separate_items_with_commas' => 'Separate Region with commas',
		'search_items'               => 'Search Regions',
		'add_or_remove_items'        => 'Add or remove Regions',
		'choose_from_most_used'      => 'Choose from the most used Regions',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'          	 => true,
		'show_in_quick_edit'         => true,
		'meta_box_cb' 				 => 'post_categories_meta_box',
	);
	register_taxonomy( 'region', 'product', $args );
}


add_action( 'init', 'sparky_custom_taxonomy_region', 0 );




/**
 * Use radio inputs instead of checkboxes for term checklists in specified taxonomies.
 *
 * @param   array   $args
 * @return  array
 */
function sparky_term_radio_checklist( $args ) {
	if ( ! empty( $args['taxonomy'] ) && $args['taxonomy'] === 'region' /* <== Change to your required taxonomy */ ) {
		if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { // Don't override 3rd party walkers.
			if ( ! class_exists( 'Sparky_Category_Radio_Checklist' ) ) {
				/**
				 * Custom walker for switching checkbox inputs to radio.
				 *
				 * @see Walker_Category_Checklist
				 */
				class Sparky_Category_Radio_Checklist extends Walker_Category_Checklist {
					function walk( $elements, $max_depth, ...$args ) {
						$output = parent::walk( $elements, $max_depth, ...$args );
						$output = str_replace(
							array( 'type="checkbox"', "type='checkbox'" ),
							array( 'type="radio"', "type='radio'" ),
							$output
						);

						return $output;
					}
				}
			}

			$args['walker'] = new Sparky_Category_Radio_Checklist;
		}
	}

	return $args;
}

add_filter( 'wp_terms_checklist_args', 'sparky_term_radio_checklist' );
