<?php


/*
|--------------------------------------------------------------------------
| Remove Sold by Vendor
|--------------------------------------------------------------------------
|
 */

//remove_filter( 'woocommerce_cart_item_name', array(YITH_Vendors_Frontend_Premium::class, 'add_sold_by_vendor'));
//remove_filter( 'woocommerce_order_item_name', array(YITH_Vendors_Frontend_Premium::class, 'add_sold_by_vendor'));


/*
|--------------------------------------------------------------------------
| Change Checkout payment Button text and add price
|--------------------------------------------------------------------------
|
 */
add_filter( 'woocommerce_order_button_text', 'sparky_custom_button_text' );

function sparky_custom_button_text( $button_text ) {
	return __('Payer','wtd') . ' ' . WC()->cart->get_total(false) . get_woocommerce_currency();
}


/*
|--------------------------------------------------------------------------
| Generate custom gallery for product page
|--------------------------------------------------------------------------
|
 */
function sparky_get_gallery_image_html($attachment_id, $class, $main_image = false) {
	//$flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
	$gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
	$thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( 500, 200 ) );
	$image_size        = apply_filters( 'woocommerce_gallery_image_size', $main_image ? 'woocommerce_single' : $thumbnail_size );
	$full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
	$thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
	$full_src          = wp_get_attachment_image_src( $attachment_id, $full_size );
	$alt_text          = trim( wp_strip_all_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) );
	$image             = wp_get_attachment_image(
		$attachment_id,
		$image_size,
		false,
		apply_filters(
			'woocommerce_gallery_image_html_attachment_image_params',
			array(
				'title'                   => _wp_specialchars( get_post_field( 'post_title', $attachment_id ), ENT_QUOTES, 'UTF-8', true ),
				'data-caption'            => _wp_specialchars( get_post_field( 'post_excerpt', $attachment_id ), ENT_QUOTES, 'UTF-8', true ),
				'data-src'                => esc_url( $full_src[0] ),
				'data-large_image'        => esc_url( $full_src[0] ),
				'data-large_image_width'  => esc_attr( $full_src[1] ),
				'data-large_image_height' => esc_attr( $full_src[2] ),
				'class'                   => esc_attr( $main_image ? 'wp-post-image img-fluid' : '' ),
			),
			$attachment_id,
			$image_size,
			$main_image
		)
	);
	$attr = '';
	if(str_contains($class, '--last')){
		$attr = 'data-next-photo="'.__('Voir plus').'"';
	}
	return '<a href="' . esc_url( $full_src[0] ) . '" '. $attr .' class="gallery-item rounded object-fit '. $class .'">' . $image . '</a>';
}



/*
|--------------------------------------------------------------------------
| Remove rating from booking sidebar in product page
|--------------------------------------------------------------------------
|
 */
remove_action('yith_wcbk_widget_booking_form_head','woocommerce_template_single_rating',20);


/*
|--------------------------------------------------------------------------
| Change a currency symbol
|--------------------------------------------------------------------------
|
 */
add_filter('woocommerce_currency_symbol', 'sparky_change_existing_currency_symbol', 10, 2);
function sparky_change_existing_currency_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'TND': $currency_symbol = 'TND'; break;
	}
	return $currency_symbol;
}



/*
|--------------------------------------------------------------------------
| Add price by person in product detail "A partir de 0000TND par personne"
|--------------------------------------------------------------------------
|
 */

add_filter( 'woocommerce_get_price_html', 'sparky_generate_custom_price_html', 99, 2 );
function sparky_generate_custom_price_html( $price_html, $product ){

	if( is_admin() || $product->get_minimum_number_of_people() <= 1){
		return $price_html;
	}


	$basePrice = $product->get_base_price() ? $product->get_base_price() : 0;

	if( $product->get_price() && $product->get_price() % $basePrice === 0){
		$tot = $basePrice;
	}else{
		$tot = $basePrice - ( $basePrice * $product->get_last_minute_discount() / 100 );
	}

	$html = $price_html .'<span class="unit-price fw-bold">'. __('Soit ', 'wtd') .'<span class="fw-bold">';
	$html .= $tot . ' ' . get_woocommerce_currency_symbol() . '</span> ' . __('par pax.', 'wtd');
	$html .= '</span>';
	return $html;
}

// when Ajax update the price
add_filter( 'yith_wcbk_booking_product_get_calculated_price_html', 'sparky_get_calculated_price_html', 99, 2 );
function sparky_get_calculated_price_html( $price_html, $price) {
	if( ! isset($_POST['persons']) || $_POST['persons'] <= 1 ){
		return $price_html;
	}
	$unit_price = number_format($price / $_POST['persons']);
	$price_html = $price_html .'<div class="unit-price text-body d-block fw-normal h6 mt-1">'. __('Soit ', 'wtd') .' <span class="fw-bold text-tertiary h5">';
	$price_html .= wc_trim_zeros( $unit_price ) . ' <span class="fs-6">' . get_woocommerce_currency_symbol() . '</span></span> ' . __('par personne', 'wtd');
	$price_html .= '</div>';
	return $price_html;
}





/*
|--------------------------------------------------------------------------
| Custom rating in product review
|--------------------------------------------------------------------------
|
 */

function the_rating_stars( $rating ) {

	if(! is_numeric($rating)) return;

	$html = '';

	for( $x = 0; $x < 5; $x++ ) {
		if( floor( $rating ) - $x >= 1 )
		{ $html .= '<i class="fas fa-star text-warning"></i>'; }
		elseif( $rating - $x > 0 )
		{ $html .= '<span class="position-relative d-inline-block" style="width:27px; height:24px;"><i class="fas fa-star text-muted opacity-25 fa-stack-1x"></i><i class="fas fa-star-half text-warning fa-stack-1x top-0" style="line-height:1.35"></i></span>'; }
		else
		{ $html .= '<i class="fas fa-star text-muted opacity-25"></i>'; }
	}
	return $html;
}



/*
|--------------------------------------------------------------------------
| Change Yith Booking booking per minute
|--------------------------------------------------------------------------
|
 */
add_filter('yith_wcbk_duration_minute_select_options', function () {
	return array( '5' => '5', '15' => '15', '30' => '30', '45' => '45', '60' => '60', '90' => '90' );
});


/*
|--------------------------------------------------------------------------
| Change Yith Booking date increment in Product detail default 15 to 5min
|--------------------------------------------------------------------------
|
 */
add_filter('yith_wcbk_get_minimum_minute_increment', function () {
	return 5;
});


/*
|--------------------------------------------------------------------------
| Change the size of thumbnail Category
|--------------------------------------------------------------------------
|
 */
add_filter('subcategory_archive_thumbnail_size', function () {
	return 'category_thumbnail';
});



/*
|--------------------------------------------------------------------------
| Related products args post per page and number of columns
|--------------------------------------------------------------------------
|
 */
add_filter( 'woocommerce_output_related_products_args', 'sparky_related_products_args', 20 );
function sparky_related_products_args( $args ) {
	$args['posts_per_page'] = 8; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}


/*
|--------------------------------------------------------------------------
| Reorder and remove some links in user dashboard
|--------------------------------------------------------------------------
|
 */
add_filter( 'woocommerce_account_menu_items', 'misha_remove_my_account_dashboard' );
function misha_remove_my_account_dashboard( $menu_links ){
//dd(get_permalink(get_field( 'faq_page', 'option' )));
	unset( $menu_links['dashboard'] );
	unset( $menu_links['orders'] );

	$logout = $menu_links['customer-logout'];

	unset($menu_links['customer-logout']);

	$menu_links['aide'] = __('Aide', 'wtd');
	$menu_links['contact'] = __('Contact', 'wtd');
	$menu_links['customer-logout'] = $logout;
	return $menu_links;
}




/*
|--------------------------------------------------------------------------
| Add custom links to user dashboard
| Help + Contact
|--------------------------------------------------------------------------
|
 */
add_filter( 'woocommerce_get_endpoint_url', 'sparky_wc_hook_endpoint', 10, 2 );
function sparky_wc_hook_endpoint( $url, $endpoint){
	if( $endpoint === 'aide' ) {
		$url = get_permalink(get_field( 'faq_page', 'option' ));
	}
	if( $endpoint === 'contact' ) {
		$url = get_permalink( get_page_by_path( 'contact' ) );
	}
	return $url;
}





/*
|--------------------------------------------------------------------------
| Redirect my-account page to bookings page. user profile
|--------------------------------------------------------------------------
|
 */
add_action('template_redirect', 'sparky_redirect_to_orders_from_dashboard' );
function sparky_redirect_to_orders_from_dashboard(){
	global $wp;
	if( is_account_page() && $wp->request === 'my-account' && is_user_logged_in() ){
		wp_safe_redirect( wc_get_account_endpoint_url( 'bookings' ) );
		exit;
	}

}


/*
|--------------------------------------------------------------------------
| Removes Checkout Fields and Reorder them
|--------------------------------------------------------------------------
|
 */

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
//	unset($fields['billing']['billing_first_name']);
//	unset($fields['billing']['billing_last_name']);
	unset($fields['billing']['billing_company']);
//	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_address_2']);
//	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_postcode']);
//	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_state']);
//	unset($fields['billing']['billing_phone']);
	unset($fields['order']['order_comments']);
//	unset($fields['billing']['billing_email']);
//	unset($fields['account']['account_username']);
//	unset($fields['account']['account_password']);
//	unset($fields['account']['account_password-2']);
	$fields['billing']['billing_email']['priority'] = 25;
	$fields['billing']['billing_phone']['priority'] = 26;

	return $fields;
}



/*
|--------------------------------------------------------------------------
| Changes Args of some Woocommerce form fields
|--------------------------------------------------------------------------
|
 */
add_filter('woocommerce_form_field_args',function ( $args, $key, $value = null ) {

	/* This is not meant to be here, but it serves as a reference
	of what is possible to be changed.
	$defaults = array(
		'type'			  => 'text',
		'label'			 => '',
		'description'	   => '',
		'placeholder'	   => '',
		'maxlength'		 => false,
		'required'		  => false,
		'id'				=> $key,
		'class'			 => array(),
		'label_class'	   => array(),
		'input_class'	   => array(),
		'return'			=> false,
		'options'		   => array(),
		'custom_attributes' => array(),
		'validate'		  => array(),
		'default'		   => '',
	); */

	if( $args['id'] === 'billing_first_name' || $args['id'] === 'billing_last_name'){
		$args['class'][] = 'col-lg-6 mt-lg-0';
	}

	if( $args['id'] === 'billing_phone' || $args['id'] === 'billing_email'){
		$args['class'][] = 'col-lg-6';
	}
	// Start field type switch case
	switch ( $args['type'] ) {


		case "select" :  /* Targets all select input type elements, except the country and state select input types */
			$args['class'][] = 'form-group'; // Add a class to the field's html element wrapper - woocommerce input types (fields) are often wrapped within a <p></p> tag
			$args['input_class'] = array('form-control', 'input-lg'); // Add a class to the form input itself
			//$args['custom_attributes']['data-plugin'] = 'select2';
			$args['label_class'] = array('form-label');
			$args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  ); // Add custom data attributes to the form input itself
			break;

		case 'country' : /* By default WooCommerce will populate a select with the country names - $args defined for this specific input type targets only the country select element */
			$args['class'][] = 'form-group single-country';
			$args['input_class'] = array('form-select', 'input-lg');
			$args['label_class'] = array('form-label');
			//$args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  ); // Add custom data attributes to the form input itself
			break;

		case "state" : /* By default WooCommerce will populate a select with state names - $args defined for this specific input type targets only the country select element */
			$args['class'][] = 'form-group'; // Add class to the field's html element wrapper
			$args['input_class'] = array('form-control', 'input-lg'); // add class to the form input itself
			//$args['custom_attributes']['data-plugin'] = 'select2';
			$args['label_class'] = array('form-label');
			$args['custom_attributes'] = array( 'data-plugin' => 'select2', 'data-allow-clear' => 'true', 'aria-hidden' => 'true',  );
			break;


		case "password" :
		case "text" :
		case "email" :
		case "tel" :
		case "number" :
			$args['class'][] = 'form-group';
			//$args['input_class'][] = 'form-control input-lg'; // will return an array of classes, the same as bellow
			$args['input_class'] = array('form-control', 'input-lg');
			$args['label_class'] = array('form-label');
			break;

		case 'textarea' :
			$args['input_class'] = array('form-control');
			$args['label_class'] = array('form-label');
			break;

		case 'checkbox' :
			break;

		case 'radio' :
			break;

		default :
			$args['class'][] = 'form-group';
			$args['input_class'] = array('form-control', 'input-lg');
			$args['label_class'] = array('form-label');
			break;
	}

	return $args;
},10,3);



/*
|--------------------------------------------------------------------------
| Put payment method section in the end of form
|--------------------------------------------------------------------------
|
 */
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
add_action( 'woocommerce_checkout_after_order_review', 'woocommerce_checkout_payment', 30 );




/*
|--------------------------------------------------------------------------
| add CC icons in the CC method
|--------------------------------------------------------------------------
|
 */
add_filter( 'woocommerce_gateway_icon', function ($icon,$id) {
	if($id == 'cc_ctp'){
		return '<img src="/wp-content/themes/whattodo-theme/public/images/cc-v-m-icons.png" alt="Carte de crédit" width="80px" />';
	}
}, 10, 2 );



/*
|--------------------------------------------------------------------------
| Add Activity image in checkout order review
|--------------------------------------------------------------------------
|
 */

add_filter( 'woocommerce_cart_item_name', 'ts_product_image_on_checkout', 10, 3 );

function ts_product_image_on_checkout( $name, $cart_item, $cart_item_key ) {

	/* Return if not checkout page */
	if ( ! is_checkout() ) {
		return $name;
	}

	/* Get product object */
	$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

	/* Get product thumbnail */
	$thumbnail = $_product->get_image();

	/* Add wrapper to image and add some css */
	$image = '<div class="ts-product-image img-cover rounded overflow-hidden mb-3" style="height: 150px">' . $thumbnail . '</div>';

	/* Prepend image to name and return it */
	return $image . $name;
}


/*
|--------------------------------------------------------------------------
| Reorder Coupon code position in checkout form
|--------------------------------------------------------------------------
|
 */
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form');
add_action( 'woocommerce_review_order_before_payment', 'woocommerce_checkout_coupon_form' );


/*
|--------------------------------------------------------------------------
| Remove Empty cart message Notice (we have static empty cart message)
|--------------------------------------------------------------------------
|
 */
remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );



/*
|--------------------------------------------------------------------------
| Change Read more button text
|--------------------------------------------------------------------------
|
 */
//add_filter('woocommerce_product_single_add_to_cart_text', function () {
//    return __('Découvrir','wtd');
//});

add_filter('woocommerce_product_add_to_cart_text', function () {
    return __('Découvrir','wtd');
});



/*
|--------------------------------------------------------------------------
| Empty the user cart when cart item is canceled by user
|--------------------------------------------------------------------------
|
 */
function update_cart_if_booking_is_canceled()
{
	if(is_admin())  return;

	if( ! is_user_logged_in()) return;

	if ( ( defined('DOING_AJAX') && DOING_AJAX ) || WC()->cart->is_empty() ) return;

	$cart = WC()->cart->get_cart();
	$statusToApply = ['cancelled','unconfirmed','cancelled','cancelled_by_user'];
	foreach ($cart as $cart_item_key => $cart_item){
		$booking = yith_get_booking($cart_item['yith_booking_data']['_booking_id']);

		if( $booking && in_array($booking->get_status(),$statusToApply)){
			$cart = WC()->cart;
			$cart->empty_cart();
			break;
		}
	}
}
//add_action('template_redirect','update_cart_if_booking_is_canceled');


/*
|--------------------------------------------------------------------------
| Change Empty cart message to static in empty cart
|--------------------------------------------------------------------------
|
 */
function get_user_confirmed_bookings() {

	if ( is_admin() || ! is_user_logged_in() ) return false;

	$bookings = yith_wcbk_get_bookings(
		array(
			'user_id' => get_current_user_id(),
			'return'  => 'ids',
			'status' => 'confirmed'
		)
	);

	if( $bookings) {
		return  $bookings;
	}else{
		return false;
	}

}



/*
|--------------------------------------------------------------------------
| Keep only one item in cart
| When an item is added to the cart, remove other products
|--------------------------------------------------------------------------
|
 */

add_filter( 'woocommerce_add_cart_item_data', 'sparky_maybe_empty_cart', 10, 1);
function sparky_maybe_empty_cart( $cart_item_data ) {
	global $woocommerce;

	if( ! empty ( $woocommerce->cart->get_cart() ) && $cart_item_data ){
		$woocommerce->cart->empty_cart();
		wc_add_notice( 'Vous ne pouvez avoir qu\'une seule activité dans votre panier.', 'info' );
	}

	return $cart_item_data;
}










/*
|--------------------------------------------------------------------------
| Change notice when user request confirmation and not logged in
|--------------------------------------------------------------------------
|
 */


// \YITH_WCBK_Frontend_Action_Handler::init();
//if ( apply_filters( 'yith_wcbk_request_confirmation_login_required', true ) && ! is_user_logged_in() ) {
//	$notice = apply_filters( 'yith_wcbk_notice_for_request_confirmation_login_required', __( 'You must log in before asking for a booking confirmation', 'yith-booking-for-woocommerce' ) );
//	if ( wc_get_page_id( 'myaccount' ) ) {
//		$link_url = add_query_arg( array( 'yith-wcbk-product-redirect' => $product->get_id() ), wc_get_page_permalink( 'myaccount' ) );
//		$text     = apply_filters( 'yith_wcbk_button_text_for_request_confirmation_login_required', __( 'Log in', 'yith-booking-for-woocommerce' ) );
//
//		$notice .= "<a class='button' href='{$link_url}'>" . $text . '</a>';
//	}
//
//	$notice = apply_filters( 'yith_wcbk_request_confirmation_login_required_notice', $notice, $product );
//
//	wc_add_notice( $notice, 'error' );
//
//	return;
//}



