<?php


/*
|--------------------------------------------------------------------------
| Custom Login page design
|--------------------------------------------------------------------------
|
 */

use function Roots\asset;

//add_filter('login_color_palette', function () {
//	return [
//		'brand' => '#243e90',
//		'trim' => '#FFF',
//		'trim-alt' => '#d5d8df',
//	];
//});
//
///**
// * Change the WordPress login header to the blog name.
// *
// * @return string
// */
//add_filter('login_headertext', function () {
//	return get_bloginfo('name');
//});


add_action('login_enqueue_scripts', function () {
	wp_enqueue_style('wtd/login.css', asset('styles/admin.css')->uri(), false, null);
}, 100);
