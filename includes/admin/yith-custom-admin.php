<?php


/*
|--------------------------------------------------------------------------
| Add user info in booking detail dashboard
|--------------------------------------------------------------------------
|
 */

add_action('yith_wcbk_booking_metabox_info_after_second_column',function ($booking) {
	?>

	<div class="form-field form-field-wide " style="background: #efefef; border: 1px solid #ddd; border-radius: 10px; padding: 10px">
		<?php
		$user        = get_user_by( 'id', $booking->get_user_id( 'edit' ) );
		if ( $user ) {
			echo '<h5 style="margin-bottom: 5px; margin-top: 0px; color:#448a85"> #'. absint( $user->ID ). ' - ' .esc_html( $user->display_name )  . '</h5><p>' . esc_html( $user->user_email ) . '<br>'. get_the_author_meta( 'billing_phone',  $user->ID ). '</p>';
		}
		?>
		<p>
	</div>

	<?php
},10,1);
