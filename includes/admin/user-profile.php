<?php


/*
|--------------------------------------------------------------------------
| Add user customize user listing columns in account list
|--------------------------------------------------------------------------
|
 */

// add and reorder columns
function sparky_add_custom_user_column($columns){

	unset($columns['posts']);

	$openid_col = $columns['mo_openid_delete_profile_data'];
	unset($columns['mo_openid_delete_profile_data']);
	$email  = $columns['email'];
	unset($columns['email']);

	$columns['id'] = 'User ID';
	$columns['email'] = $email;
	$columns['phone'] = 'Phone';
	$columns['registration_date'] = 'Registration Date';

	$columns['mo_openid_delete_profile_data'] = $openid_col;

	return $columns;
}
add_filter('manage_users_columns', 'sparky_add_custom_user_column');


// make custom col sortable
function sparky_user_sortable_custom_column( $columns ) {
	$columns['registration_date'] = 'Registration Date';
	return $columns;
}
add_filter( 'manage_users_sortable_columns', 'sparky_user_sortable_custom_column' );


// add values to custom cols
function sparky_user_custom_column_values($value, $column_name, $user_id){
	switch ($column_name) {
		case 'phone' :
			$phone = get_the_author_meta( 'billing_phone', $user_id ) ? get_the_author_meta( 'billing_phone', $user_id ) : '--------';
			return  $phone;
		case 'id' :
			return $user_id;
		case 'registration_date' :
			$user_data = get_userdata( $user_id );
			return wp_date('d-m-Y',strtotime($user_data->user_registered));
		default:
	}
	return $value;
}
add_action('manage_users_custom_column', 'sparky_user_custom_column_values', 9, 3);





/*
|--------------------------------------------------------------------------
| Remove unnecessary fields from user profile page
|--------------------------------------------------------------------------
|
 */

// remove unnecessary fields in user profile page
if( is_admin() ){
	remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
	add_action( 'personal_options', 'hide_unnecessary_section_and_fields_in_profile_page' );
}
function hide_unnecessary_section_and_fields_in_profile_page() {
	?>
	<script type="text/javascript">
		jQuery( document ).ready(function( $ ){
			$( '.yoast, .user-description-wrap, .user-profile-picture, #fieldset-shipping, #fieldset-billing + h2, .user-url-wrap, .user-admin-color-wrap,.user-rich-editing-wrap,#application-passwords-section' ).remove();
			$( '#billing_company,#billing_address_2' ).parent().parent().remove();

			let userInfoBlock = $('.user-user-login-wrap').parent();

			userInfoBlock.prepend($( '#billing_phone' ).parent().parent());
			userInfoBlock.prepend($('.user-email-wrap'));

			userInfoBlock.find('#billing_phone').prop('disabled','disabled:');
			userInfoBlock.find('.user-email-wrap input[name=email]').prop('disabled','disabled:');
		} );
	</script>
	<style>
		#poststuff form#your-profile .acf-column-1 h2:nth-of-type(3),
		#poststuff form#your-profile .acf-column-1 h2:nth-of-type(4){
			display: none !important;
		}
	</style>
	<?php
}




// remove social links fields in user profile page
function modify_contact_methods($profile_fields) {

	foreach ($profile_fields as $key => $value){
		unset($profile_fields[$key]);
	}
	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');
