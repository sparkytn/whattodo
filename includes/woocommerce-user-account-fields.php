<?php



/*
|--------------------------------------------------------------------------
| Register fields Validating.
|--------------------------------------------------------------------------
|
 */

function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {


	if ( isset( $_POST['first_name'] ) && empty( $_POST['first_name'] ) ) {
		$validation_errors->add( 'first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
	}
	if ( isset( $_POST['last_name'] ) && empty( $_POST['last_name'] ) ) {
		$validation_errors->add( 'last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
		$validation_errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Phone is required!.', 'woocommerce' ) );
	}

	return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );



/*
|--------------------------------------------------------------------------
| save register extra fields Validating.
|--------------------------------------------------------------------------
|
 */
function wooc_save_extra_register_fields( $customer_id ) {
	if ( isset( $_POST['billing_phone'] )  && isset( $_POST['full_phone'] ) ) {
		// Phone input filed which is used in WooCommerce
		update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['full_phone'] ) );
	}
	if ( isset( $_POST['billing_first_name'] ) ) {
		//First name field which is by default
		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		// First name field which is used in WooCommerce
		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	}
	if ( isset( $_POST['billing_last_name'] ) ) {
		// Last name field which is by default
		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		// Last name field which is used in WooCommerce
		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	}
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );


add_action( 'woocommerce_save_account_details', 'cssigniter_save_account_details' );
function cssigniter_save_account_details( $user_id ) {
	if ( isset( $_POST['billing_phone'] ) && isset( $_POST['full_phone'] ) ) {
		update_user_meta( $user_id, 'billing_phone', sanitize_text_field( $_POST['full_phone'] ) );
	}
}
