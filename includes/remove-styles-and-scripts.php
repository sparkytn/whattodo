<?php
// todo : Clear remove scripts file


/*
|--------------------------------------------------------------------------
| Generate custom gallery for product page
|--------------------------------------------------------------------------
|
 */


/**
|--------------------------------------------------------------------------
| Check if WooCommerce Page of all kinds (not just cart and shop)
|--------------------------------------------------------------------------
 * @thanks: https://faish.al/2014/01/06/check-if-it-is-woocommerce-page/
 * Checked IDS with WooCommerce Repo 1 June 2019
 */
function ca_is_really_woocommerce_page() {

	if( function_exists ( 'is_woocommerce' ) && is_woocommerce() ) :
		return true;
	endif;

	$woocommerce_keys = array (
		'woocommerce_shop_page_id' ,
		'woocommerce_terms_page_id' ,
		'woocommerce_cart_page_id' ,
		'woocommerce_checkout_page_id' ,
		'woocommerce_pay_page_id' ,
		'woocommerce_thanks_page_id' ,
		'woocommerce_myaccount_page_id' ,
		'woocommerce_edit_address_page_id' ,
		'woocommerce_view_order_page_id' ,
		'woocommerce_change_password_page_id' ,
		'woocommerce_logout_page_id' ,
		'woocommerce_lost_password_page_id' ) ;

//	foreach ( $woocommerce_keys as $wc_page_id ) :
//		if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) :
//			return true;
//		endif;
//	endforeach;


	if(is_front_page()){
		return false;
	}

	return true;
}

add_action( 'wp_enqueue_scripts', 'ca_dequeue_woocommerce_js_css_on_non_woo_pages', 99 );
/**
 * Remove WooCommerce CSS and JS on non-WooCommerce pages for 3.6.4
 * Keep this updated by looking here:
 * https://github.com/woocommerce/woocommerce/blob/44d129e45286b782fcce2ba67acf837a46e0c6c9/includes/class-wc-frontend-scripts.php
 *
 * Comment out the ones you want to keep
 * -- Remember that now any sidebar / widget / shortcode from WooCommerce used on a
 * -- NON-Woo Page will not look good and won't work
 */
function ca_dequeue_woocommerce_js_css_on_non_woo_pages() {

	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	if ( ! function_exists( 'is_woocommerce' ) ) return; //exit if does not exist


	// dequeue styles
	$dequeue_style = array(
		'woocommerce-layout',
//		'wc-block-style',
		'woocommerce-inline',
		'woocommerce-smallscreen',
	);

	foreach ( $dequeue_style as $dstyle ) :
		wp_dequeue_style( $dstyle );
		wp_deregister_style( $dstyle );
	endforeach;

	// dequeue scripts
	$dequeue_script = array(
		'flexslider',
		'photoswipe' ,
		'photoswipe-ui-default',
		'wc-cart-fragments',
//		'wc-credit-card-form',
		'wc-add-to-cart',
		'wc-add-to-cart-variation',
		'zoom',
	);

	foreach ( $dequeue_script as $dscript ) :
		wp_dequeue_script( $dscript );
	endforeach;


	// Dequeue if is not WooCommerce page and not cart and not checkout
	// Add more conditions eg ` && ! is_front_page() ` to load on front page
	if (
	! ca_is_really_woocommerce_page()
	) :


		// dequeue scripts
		$dequeue_script = array(
			'js-cookie',
			'jquery-blockui',
			'jquery-payment',
			'wc-address-i18n',
			'wc-add-payment-method',
			'wc-cart',
			'wc-checkout',
			'wc-country-select',
			'wc-add-to-cart',
			'wc-credit-card-form',
			'wc-geolocation',
			'wc-lost-password',
			'wc-password-strength-meter',
			'woocommerce',
		);

		foreach ( $dequeue_script as $dscript ) :
			wp_dequeue_script( $dscript );
		endforeach;

	endif; // conditions met

}




/* ----------------
| 	Dequeue Styles and Scripts
------------------- */
add_action( 'wp_enqueue_scripts', 'sparky_dequeue_styles_and_scripts', 100 );
add_action( 'wp_head', 'sparky_dequeue_styles_and_scripts_header', 100 );

function sparky_dequeue_styles_and_scripts_header() {

//	wp_dequeue_style( 'global-styles' ); // REMOVE THEME.JSON

	if( is_singular('post') || is_page_template('template-custom-activity.blade.php')){
		wp_enqueue_style( 'wp-block-library' );
		wp_enqueue_style( 'wp-block-library-theme' );
	}
//	remove_action('wp_enqueue_scripts', 'the_champ_load_event',8);
//	remove_action('wp_enqueue_scripts', 'the_champ_frontend_scripts',8);
//	remove_action('wp_enqueue_scripts', 'the_champ_frontend_styles',8);
}
function sparky_dequeue_styles_and_scripts() {

	// YITH Plugin
	if( ! is_product() && ! is_shop() && ! is_product_taxonomy()){
		wp_dequeue_style( 'yith-wcbk' );
		wp_deregister_style( 'yith-wcbk' );
		wp_dequeue_style( 'yith-wcbk-datepicker' );
		wp_dequeue_style( 'yith-wcbk-datepicker' );
		wp_dequeue_script( 'yith-wcbk-datepicker' );
		wp_dequeue_script( 'yith-wcbk-date-picker' );
		wp_dequeue_script( 'yith-wcbk-booking-form' );
		wp_dequeue_script( 'yith-wcbk-services-selector' );
	}
	if( ! is_product() ){
		wp_dequeue_style( 'woocommerce-general' );
		wp_deregister_style( 'woocommerce-general' );
		wp_dequeue_script( 'wc-single-product' );
	}
	if ( ! is_checkout() ) {
		wp_dequeue_style( 'select2' );
		wp_deregister_style( 'select2' );
		wp_dequeue_script( 'select2' );
	}



	if(! is_user_logged_in()){
		wp_dequeue_style( 'dashicons' );
		wp_deregister_style( 'dashicons' );
	}

	wp_dequeue_style( 'yith-wcbk-simple-style' );
	wp_deregister_style( 'yith-wcbk-simple-style' );
	wp_deregister_style( 'yith-plugin-fw-icon-font' );
	wp_dequeue_style( 'yith-plugin-fw-icon-font' );
	wp_dequeue_style( 'yith-wcbk-date-range-picker' );
	wp_deregister_style( 'yith-wcbk-date-range-picker' );
	wp_dequeue_style( 'yith-wcbk-booking-form' );
	wp_deregister_style( 'yith-wcbk-booking-form' );
	wp_dequeue_style( 'yith-wcbk-fields' );
	wp_deregister_style( 'yith-wcbk-fields' );
	wp_dequeue_style( 'yith-wcbk-popup' );
	wp_deregister_style( 'yith-wcbk-popup' );

	wp_dequeue_style( 'yith-wcbk-frontend-style' );
	wp_deregister_style( 'yith-wcbk-frontend-style' );
	wp_dequeue_style( 'yith-wcbk-search-form' );
	wp_deregister_style( 'yith-wcbk-search-form' );
	wp_dequeue_style( 'yith-wcbk-people-selector' );
	wp_deregister_style( 'yith-wcbk-people-selector' );

	wp_dequeue_script( 'yith-wcbk-people-selector' );
	wp_dequeue_script( 'yith-wcbk-monthpicker' );


	// berocket Ajax filter
	if( ! is_shop() && ! is_product_taxonomy() ){
		wp_dequeue_style( 'berocket_aapf_widget-style' );
		wp_deregister_style( 'berocket_aapf_widget-style' );
		wp_dequeue_script( 'berocket_aapf_widget-script' );
	}
}




add_action('after_setup_theme', function() {

	// remove SVG and global styles
//	remove_action('wp_enqueue_scripts', 'wp_enqueue_global_styles');

	// remove wp_footer actions which add's global inline styles
//	remove_action('wp_footer', 'wp_enqueue_global_styles', 1);
	remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' );
	remove_action( 'in_admin_header', 'wp_global_styles_render_svg_filters' );

	// remove render_block filters which adding unnecessary stuff
	remove_filter('render_block', 'wp_render_duotone_support');
//	remove_filter('render_block', 'wp_restore_group_inner_container');
//	remove_filter('render_block', 'wp_render_layout_support_flag');
});



function remove_jquery_migrate( $scripts ) {
	if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
		$script = $scripts->registered['jquery'];
		if ( $script->deps ) {
			// Check whether the script has any dependencies
			$script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
		}
	}
}
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

function sparky_remove_thickbox(  ) {
	if (!is_admin()) {
		wp_deregister_style('thickbox');
		wp_deregister_script('thickbox');
	}
}
add_action( 'init', 'sparky_remove_thickbox' );
