/**
 * External Dependencies
 */



import Collapse from 'bootstrap/js/src/collapse';
import Dropdown from 'bootstrap/js/src/dropdown';
import Offcanvas from 'bootstrap/js/src/offcanvas';
import Modal from 'bootstrap/js/src/modal';


import LazyLoad from 'vanilla-lazyload';

window.Modal = Modal;
window.Dropdown = Dropdown;
require('bootstrap-select');

import intlTelInput from 'intl-tel-input';
import Utils from 'intl-tel-input/build/js/utils';


import lightGallery from "lightgallery";
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';


import '@components/swiper';


$(document).ready(() => {
	/*
	-----------------------------------------
		Select dropdown
	-----------------------------------------
	*/

	var lazyLoadInstance = new LazyLoad({
		// Your custom settings go here
	});
	$('.select-dropdown').selectpicker({
		style: '',
		styleBase: 'form-select',
		size: 10,
	});
	// when the modal is opened autoplay it
	// $('#HeroVideoModal').on('shown.bs.modal', function (e) {
	// 	$("#videoModalIframe").attr('src',$videoSrc + "?amp;modestbranding=1&amp;showinfo=0");
	// })
	//
	// $('#HeroVideoModal').on('hidden.bs.modal', function (event) { // when modal hidden
	// 	$('#videoModalIframe') // pick the right element
	// 		.attr('src', '');  // empty the video id
	// })



	/*
	-----------------------------------------
	Dropdown show on Hover
	-----------------------------------------
	*/
	// make it as accordion for smaller screens
	if (window.innerWidth > 992) {
		document.querySelectorAll('.dropdown-hover').forEach(function (everyitem) {
			let dropdownItem = Dropdown.getOrCreateInstance(everyitem.querySelector('[data-bs-toggle]'));
			everyitem.addEventListener('mouseover', function (e) {
				dropdownItem.show();
			});
			everyitem.addEventListener('mouseleave', function (e) {
				dropdownItem.hide();
				$(e.currentTarget).focusout();
			})
		});
	}


	const phoneNumberInput = document.querySelector("#billing_phone") || document.querySelector("#client-phone");

	if (phoneNumberInput) {
		window.phoneNumberInput = intlTelInput(phoneNumberInput, {
			initialCountry: 'auto',
			geoIpLookup: function (callback) {
				$.get('https://ipinfo.io', function () {
				}, "jsonp").always(function (resp) {
					var countryCode = (resp && resp.country) ? resp.country : "tn";
					callback(countryCode);
				});
			},
			separateDialCode: true,
			hiddenInput: "full_phone",
			formatOnDisplay: true,
			utilsScript: Utils,
		});
	}


	//woocommerce-EditAccountForm
	// $( document.body ).on( 'updated_checkout', function(data) {
	// 	var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>",
	// 		country_code = $('#billing_country').val();
	//
	// 	var ajax_data = {
	// 		action: 'append_country_prefix_in_billing_phone',
	// 		country_code: $('#billing_country').val()
	// 	};
	// 	if(firstCall){
	// 		firstCall = false;
	// 	}else{
	// 		$.post( ajax_url, ajax_data, function( response ) {
	// 			let phone = $('#billing_phone').val()
	// 			$('#billing_phone').val(response);
	//
	// 		});
	// 	}
	//
	// } );


	/*
	-----------------------------------------
	Dynamic Imports
	-----------------------------------------
	*/

	// light gallery
	if ($('#lightgallery').length) {
		// 	import('./components/lightgallery.js' /* webpackChunkName: "lightgallery" */)
		// 		.catch(error => 'An error occurred while loading the component');
		lightGallery(document.getElementById('lightgallery'), {
			plugins: [lgZoom, lgThumbnail],
			speed: 500,
			licenseKey: '0000-0000-000-0001'
		});
	}


	/*
	-----------------------------------------
	Price range
	-----------------------------------------
	*/

	// light gallery
	if ($('.price-range').length) {
		noUiSlider.create($('.price-range')[0], {
			start: [10, 1500],
			connect: true,
			tooltips: true,
			step: 10,

			range: {
				'min': [0, 5],
				'30%': [500, 5],
				'60%': [1000, 5],
				'max': [1500, 5]
			},
			pips: {
				mode: 'range',
				density: 5
			}
		});
	}


	/*
	-----------------------------------------
		Show booking sticky bar for booking now
	-----------------------------------------
	*/
	if ($('#booking-card').length) {
		let mediaQueryDownLG = window.matchMedia("(max-width: 992px)"),
			bookingHeaderCTA = $('.activity-booking-cta'),
			bookingCardHeight = $('#booking-card').height(),
			bookingCardPos = $('#booking-card').offset().top;

		mediaQueryDownLG.addListener(showBookingHeaderCTA)

		function showBookingHeaderCTA(mediaQueryDownLG) {
			if ($('body').hasClass('page')){

				return false;
			}
			if (mediaQueryDownLG.matches  ) { // If media query matches
				bookingHeaderCTA.addClass('mobile');
			} else {
				bookingHeaderCTA.removeClass('mobile');
			}
			bookingCardPos = $('#booking-card').offset().top;
			bookingCardHeight = $('#booking-card').height();
		}

		showBookingHeaderCTA(mediaQueryDownLG);

		$(window).scroll(function () {
			if (bookingHeaderCTA.hasClass('mobile')) {
				if ($(this).scrollTop() > 200 && $(this).scrollTop() <= bookingCardPos - window.innerHeight + 100) {
					bookingHeaderCTA.addClass('show');
				} else {
					bookingHeaderCTA.removeClass('show');
				}
			} else {
				if ($(this).scrollTop() > bookingCardPos + bookingCardHeight / 2) {
					bookingHeaderCTA.addClass('show');
				} else {
					bookingHeaderCTA.removeClass('show');
				}
			}

		});
	}


	/*
	-----------------------------------------
		Configure  Google maps to auto-complite in Tunisia
	-----------------------------------------
	*/
	if ($('.yith-wcbk-google-maps-places-autocomplete').length) {
		jQuery(".yith-wcbk-google-maps-places-autocomplete").each(function () {
			let mapAutocomplete = new google.maps.places.Autocomplete(this, {
				types: ['(cities)'],
				componentRestrictions: {country: "tn"}
			});
		})
	}


	/*
	-----------------------------------------
		Quantity Input hack
	-----------------------------------------
	*/
	$('.quantity-input').each(function () {

		var spinner = $(this),
			input = spinner.find('input[type="number"]'),
			btnUp = spinner.find('.quantity-up'),
			btnDown = spinner.find('.quantity-down'),
			min = input.attr('min') ? input.attr('min') : 0,
			max = input.attr('max') ? input.attr('max') : 100;

		btnUp.click(function () {
			var oldValue = input.val() ? parseFloat(input.val()) : 0;
			if (max && oldValue >= max) {
				var newVal = max;
			} else {
				var newVal = oldValue + 1;
			}
			if (newVal < min) {
				newVal = min;
			}
			input.val(newVal).change();
		});

		btnDown.click(function () {
			var oldValue = parseFloat(input.val());
			if (oldValue <= min) {
				var newVal = min;
			} else {
				var newVal = oldValue - 1;
			}

			if (newVal > max) {
				newVal = max;
			}

			input.val(newVal).change();
		});
	});


	/*
	-----------------------------------------
		Reduce Paragraph height and add read more
	-----------------------------------------
	*/
	let showChar = 300;
	let ellipsesText = "...";
	let moreText = "<i class='fas fa-plus-circle me-1'></i> Lire la suite";
	let lessText = "<i class='fas fa-minus-circle me-1'></i>Réduire";
	$('.more').each(function () {
		let content = $(this).html();

		if (content.length > showChar) {

			let c = content.substr(0, showChar);
			let h = content.substr(showChar - 1, content.length - showChar);

			let html = c + '<span class="moreellipses">' + ellipsesText + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<br><a href="#" class="morelink readmore-link-collapse small d-inline-block mt-2">' + moreText + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function () {
		if ($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moreText);
		} else {
			$(this).addClass("less");
			$(this).html(lessText);
		}
		$(this).parent().prev().toggle();
		$(this).prev().prev().toggle();
		return false;
	});


	/*
	-----------------------------------------
		Sidebar sticky
	-----------------------------------------
	*/
	// if($('.sticky').length){
	// 	 new StickySidebar('.sticky', {
	// 		containerSelector: '.sidebar',
	// 		topSpacing: 60,
	// 		bottomSpacing: 60
	// 	});
	// }


	/*
	-----------------------------------------
		Share buttons
	-----------------------------------------
	*/
	if ($('[btn-social-share]').length) {
		let Url = window.location.href;
		let UrlEncoded = encodeURIComponent(Url);
		let title = encodeURIComponent($(".page-header .title").html());
		$("#fb-share").attr('href', "http://www.facebook.com/share.php?u=" + UrlEncoded);
		$("#tw-share").attr('href', "https://twitter.com/intent/tweet?text=" + title + " " + UrlEncoded);
		$("#in-share").attr('href', "http://www.linkedin.com/shareArticle?mini=true&url=" + UrlEncoded + "&title=" + title);
	}


	// $('.hf-form').on('hf-error', function(e) {
	// 	console.log('asdas', e.currentTarget.dataset['messageError']);
	// 	return;
	// });
	// $('.hf-form').on('hf-success', function(e) {
	// 	console.log('asdas', e.currentTarget.dataset['messageSuccess']);
	// 	return;
	// });


	$(document).on("click", '.woocommerce-checkout button[name="apply_coupon"]', function (event) {

		event.preventDefault();

		let $form = $('form[name="checkout"]');
		$form.block({message: ''});

		let data = {
			security: wc_checkout_params.apply_coupon_nonce,
			coupon_code: $('input[name="coupon_code"]').val()
		};

		$.ajax({
			type: 'POST',
			url: wc_checkout_params.wc_ajax_url.toString().replace('%%endpoint%%', 'apply_coupon'),
			data: data,
			success: function (code) {
				console.log(code);
				$('.woocommerce-error, .woocommerce-message').remove();
				$form.removeClass('processing').unblock();

				if (code) {
					$('button[name="apply_coupon"]').parent().after(code);
					setTimeout(function () {
						$('.woocommerce-form-coupon .alert').fadeOut(300);
					}, 4000)
					$(document.body).trigger('update_checkout', {update_shipping_method: false});
				}

			},
			dataType: 'html'
		});

	});

	// var forms = document.querySelectorAll('.needs-validation')
	// // Loop over them and prevent submission
	// Array.prototype.slice.call(forms)
	// 	.forEach(function (form) {
	// 		form.addEventListener('submit', function (event) {
	// 			event.preventDefault();
	// 			event.stopPropagation();
	//
	// 			let $this = $('#addPost');
	//
	// 			if ($this.hasClass('disabled')) return false;
	//
	//
	//
	// 			form.classList.add('was-validated');
	// 			form.classList.add('disabled');
	//
	// 		}, false)
	// 	})


	$('#sparkyContactForm').on('submit', function (e) {

		e.preventDefault();

		$('.is-invalid').removeClass('is-invalid');
		$('[js-feedback-form]').addClass('d-none');

		const formHTML = document.getElementById('sparkyContactForm');
		var form = $(this),
			spinner = $('[js-spinner]'),
			full_name = form.find('#client-name').val(),
			subject = form.find('#client-subject').val(),
			email = form.find('#client-email').val(),
			phone = form.find('#client-phone').val(),
			message = form.find('#client-message').val(),
			ajaxUrl = form.data('url'),
			nonce_data = form.data('nonce');

		let test = document.querySelector('#sparkyContactForm');



		let elmErr = form.find('.form-control:invalid,.form-select:invalid,.custom-range:invalid,.custom-file-input:invalid,.custom-control-input:invalid');
		form.addClass('was-validated');

		if( !this.checkValidity() && elmErr && elmErr.length > 0){
			$(window).scrollTop( $(elmErr[0]).offset().top - 50)
			return false
		}


		if(form.hasClass('disabled')){
			return false;
		}

		let formData = new FormData(formHTML);

		let data = {};
		for (const [key, value] of formData) {
			data[key] =  value ;
		}


		form.addClass('disabled');
		spinner.removeClass('d-none');
		form.find('[type="submit"').addClass('disabled');

		grecaptcha.ready(function () {
			grecaptcha.execute('6LeQ4nYfAAAAAMyykUj7Xazlegxz7Js-zYfuX8Os', {action: 'submit'}).then(function (token) {

				data['token']= token;
				data['nonce_data']= nonce_data;
				data['action']= 'sparky_save_user_contact_form';

				$.ajax({

					url: ajaxUrl,
					type: 'post',
					data: data,
					error: function (response) {

						spinner.addClass('d-none');
						form.removeClass('disabled');
						form.find('input, button, textarea').removeAttr('disabled');
						form.find('button').removeClass('disabled');
						$('[js-feedback-form]').removeClass('d-none');
					},
					success: function (response) {

						spinner.addClass('d-none');
						form.find('button').removeClass('disabled');
						form.find('input, button, textarea').removeAttr('disabled');
						form.removeClass('disabled');
						form.removeClass('was-validated');
						$("#sparkyContactForm")[0].reset();
						if (response.success) {
							let myModal = new Modal(document.getElementById('formSubmittedModal'), {
								backdrop: "static"
							})
							myModal.show();
						} else {

							if (response.message === 'google_captcha') {
								$('[js-feedback-form] .detail').html('Captcha non valide,')
							}
							if (response.message === 'required_fields') {
								$('[js-feedback-form] .detail').html('Tous les champs avec un astérisque (*) sont obligatoires.')
							}
							if (response.message === 'invalid_email') {
								$('[js-feedback-form] .detail').html('Votre adresse email est non valide')
							}
							console.log(response);
							$('[js-feedback-form]').removeClass('d-none')
						}
					}

				});
			});
		});

	});


	$('#sparkyCustomActivity').on('submit', function (e) {

		e.preventDefault();

		$('.is-invalid').removeClass('is-invalid');
		$('[js-feedback-form]').addClass('d-none');

		let form = $(this),
			spinner = $('[js-spinner]'),
			ajaxUrl = form.data('url');


		let elmErr = form.find('.form-control:invalid,.form-select:invalid,.custom-range:invalid,.custom-file-input:invalid,.custom-control-input:invalid');
		form.addClass('was-validated');

		if( !this.checkValidity() && elmErr && elmErr.length > 0){
			$(window).scrollTop( $(elmErr[0]).offset().top - 50)
			return false
		}


		if(form.hasClass('disabled')){
			return false;
		}

		form.addClass('disabled');
		spinner.removeClass('d-none');
		form.find('[type="submit"').addClass('disabled');

		grecaptcha.ready(function () {
			grecaptcha.execute('6LeQ4nYfAAAAAMyykUj7Xazlegxz7Js-zYfuX8Os', {action: 'submit'}).then(function (token) {

				$('#token').val(token);

				$.ajax({
					url: ajaxUrl,
					type: 'post',
					data: $('#sparkyCustomActivity').serialize(),
					error: function (response) {

						spinner.addClass('d-none');
						form.removeClass('disabled');
						form.find('input, button, textarea').removeAttr('disabled');
						form.find('button').removeClass('disabled');
						$('[js-feedback-form]').removeClass('d-none');
					},
					success: function (response) {

						spinner.addClass('d-none');
						form.find('button').removeClass('disabled');
						form.find('input, button, textarea').removeAttr('disabled');
						form.removeClass('disabled');
						form.removeClass('was-validated');
						$("#sparkyCustomActivity")[0].reset();
						if (response.success) {
							let myModal = new Modal(document.getElementById('formSubmittedModal'), {
								backdrop: "static"
							})
							myModal.show();
						} else {

							if (response.message === 'google_captcha') {
								$('[js-feedback-form] .detail').html('Captcha non valide,')
							}
							if (response.message === 'required_fields') {
								$('[js-feedback-form] .detail').html('Tous les champs avec un astérisque (*) sont obligatoires.')
							}
							if (response.message === 'invalid_email') {
								$('[js-feedback-form] .detail').html('Votre adresse email est non valide')
							}
							$('[js-feedback-form]').removeClass('d-none')
						}
					}

				});
			});
		});

	});


});
