
import Swiper, {Autoplay, EffectFade, Lazy, Navigation, Pagination} from 'swiper';
// Configure Swiper to use modules
Swiper.use([ Navigation, Pagination, Autoplay, EffectFade, Lazy ]);


// init Swiper:

$('.home-hero-carousel').each( function() {
	let $this = $(this);
	let slider = new Swiper(this, {
		// Default parameters
		slidesPerView: 1,
		init: true,
		spaceBetween: 0,
		speed: 1000,
		loop: true,
		effect: 'fade',
		fadeEffect:{
			crossFade: true
		},
		lazy: true,
		autoplay: {
			delay: 4000
		},
		// on: {
		// 	init: function () {
		// 		this.update();
		// 	},
		// }
	});
	//slider.init();
});

$('.home-activities-carousel').each(function() {
	let $this = $(this);
	let slider = new Swiper(this, {
		// Default parameters
		slidesPerView: 1.3,
		slidesPerGroup:1,

		init: false,
		spaceBetween: 30,
		pauseOnMouseEnter: true,
		speed: 600,
		loop: false,
		lazy: true,
		autoplay: {
			delay: 4000,
			disableOnInteraction: true,
		},
		navigation: {
			nextEl: $this.parent().find('.swiper-button-next')[0],
			prevEl: $this.parent().find('.swiper-button-prev')[0],
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
		},
		breakpoints: {
			// when window width is >= 320px
			320: {
				slidesPerView: 1.3,
				spaceBetween: 20,
				slidesPerGroup:1,
			},
			// when window width is >= 480px
			767: {
				slidesPerView: 2.5,
				spaceBetween: 20,
				slidesPerGroup:2,
			},

			1000: {
				slidesPerView: 3.2,
				slidesPerGroup:3,
			},

			// when window width is >= 640px
			1300: {
				slidesPerView: 4.2,
				slidesPerGroup:4,
			}
		},
		on: {
			init: function () {
				this.update();
			},
		}
	});
	slider.init();
});

if($('.detail-activities-carousel').length){
	$('.detail-activities-carousel').each(function() {
		let $this = $(this);
		let slider = new Swiper(this, {
			// Default parameters
			slidesPerView: "auto",

			init: false,
			spaceBetween: 30,
			pauseOnMouseEnter: true,
			speed: 600,
			loop: false,
			autoplay: {
				delay: 4000,
				disableOnInteraction: true,
			},
			lazy: true,
			navigation: {
				nextEl: $this.parent().find('.swiper-button-next')[0],
				prevEl: $this.parent().find('.swiper-button-prev')[0],
			},
			pagination: {
				el: '.swiper-pagination',
				type: 'bullets',
			},
			breakpoints: {
				// when window width is >= 320px
				320: {
					slidesPerView: 1.3,
					spaceBetween: 20,
					slidesPerGroup:1,
				},
				// when window width is >= 480px
				767: {
					slidesPerView: 2.5,
					spaceBetween: 20,
					slidesPerGroup:2,
				},

				1000: {
					slidesPerView: 3.5,
					slidesPerGroup:3,
				},

				// when window width is >= 640px
				1200: {
					slidesPerView: 4.5,
					slidesPerGroup:4,
				}
			},
			on: {
				init: function () {
					this.update();
				},
			}
		});
		slider.init();
	})
}
