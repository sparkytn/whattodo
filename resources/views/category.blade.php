@extends('layouts.app')

@section('content')
	@include('partials.page-header')

	<div class="page-body">
		<div class="container p-0">
			<div class="page-content-card mb-5">
				<div class="container">
					@if (!have_posts())
						<div class="alert alert-warning">
							{{ __('Sorry, no results were found.', 'sage') }}
						</div>
						{!! get_search_form(false) !!}
					@endif

					<div class="row row-spacing">
						@while (have_posts()) @php the_post() @endphp
						<div class="col-md-6 col-lg-4">
							@include('partials.content-'.get_post_type())
						</div>
						@endwhile
					</div>

					{!! get_the_posts_navigation() !!}
				</div>
			</div>
		</div>
	</div>

@endsection
