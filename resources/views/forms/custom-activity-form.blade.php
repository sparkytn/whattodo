<form id="sparkyCustomActivity" class="mb-3" action="#" novalidate method="post" data-url="{{ admin_url('admin-ajax.php') }}">
	<h5 class="mb-4 text-tertiary">Ou Remplissez ce formulaire : </h5>
	<div class="row g-3">
		<div class="col-sm-12 has-error">
			<label class="form-label" for="client-name">Votre nom et prénom <span class="text-danger">*</span></label>
			<input class="form-control" type="text" id="client-name" name="name" placeholder="Votre nom et prénom" required="">
		</div>
		<div class="col-sm-6">
			<label class="form-label" for="client-email">Votre adresse email <span class="text-danger">*</span></label>
			<input class="form-control" type="email" id="client-email" name="email" placeholder="Votre adresse email" required="">
		</div>
		<div class="col-sm-6">
			<label class="form-label" for="client-phone">Votre n° de téléphone <span class="text-danger">*</span></label>
			<input class="form-control" type="tel" id="client-phone" name="phone" placeholder="Votre n° de téléphone" required>
		</div>
		<div class="col-sm-12">
			<label class="form-label" for="activity_type">Type D'activité <span class="text-danger">*</span></label>
			<select class="form-control select-dropdown" name="activity_type" id="activity-type" required>
				<option value="">Type D'activité</option>
				<option value="Underground">Activité Underground</option>
				<option value="Comfort">Activité Confort</option>
				<option value="Lux">Activité Culturelle</option>
			</select>
		</div>

		<div class="col-sm-6">
			<label class="form-label" for="pax">Nombre de personnes <span class="text-danger">*</span></label>
			<div class="input-icon quantity quantity-input">
				<i class="icon fas text-warning fa-users"></i>
				<input class="form-control" type="number" id="pax" min="1" max="30" value="1" name="pax" placeholder="Nombre de personnes" required>
				<div class="quantity-nav">
					<button class="quantity-button quantity-up" type="button">+</button>
					<button class="quantity-button quantity-down" type="button">-</button>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<label class="form-label" for="activity_type">Budget approximatif <span class="text-danger">*</span> </label>
			<select class="form-control select-dropdown" name="budget" id="budget" required>
				<option value="">Budget approximatif</option>
				<option value="de 500DT a 1000DT">de 500DT a 1000DT</option>
				<option value="de 1001DT à 2000DT">de 1001DT à 2000DT</option>
				<option value="de 2001DT à 5000DT">de 2001DT à 5000DT</option>
				<option value="de 5001DT à plus">de 5001DT à plus</option>
			</select>
		</div>
		<div class="col-12">
			<label class="form-label" for="client-message">Plus de détails <span class="text-danger">*</span></label>
			<textarea class="form-control" id="client-message" rows="6" name="details" placeholder="Décrivez vos préférences" required=""></textarea>
		</div>
		<div class="col-12 d-none" js-feedback-form>
			<div class="alert alert-danger mb-0">
				<h6 class="text-white fs-sm mb-0">{{ __('Erreurs lors de la soumission du formulaire','wtd') }}</h6>
				<div class="detail small">{{ __("Merci d'actualisez la page et réessayez.",'wtd') }}</div>
			</div>
		</div>
		<div class="col-12">

			<input type="hidden" name="action" value="custom_activity_req">
			<input type="hidden" name="nonce" value="{{ wp_create_nonce('custom_activity_req') }}">
			<input type="hidden" name="token" id="token" value="">

			<button class="btn btn-primary" type="submit">
				<span class="spinner-border spinner-border-sm me-2 d-none" js-spinner role="status" aria-hidden="true"></span>
				Envoyer votre demande
			</button>
		</div>
	</div>
</form>

<x-modals.form-success></x-modals.form-success>
