@extends('layouts.app')
@section('content')

	@include('partials.home-hero')


	<section id="about" class="features-section py-3 py-lg-4 bg-dark text-white">
		<div class="container">

			<div class="row text-center text-md-start">
				<div class="col-4">
					<div class="features-section__list d-flex flex-column flex-md-row align-items-center">
						<img src="@asset('images/icon-support.svg')" class="icon me-md-3" alt="Support client">
						<div>
							<h6 class="mb-1 text-white">Support client</h6>
							<p class="m-0 fs-xs d-none d-md-block opacity-75">Contactez-nous par téléphone, e-mail ou sur WhatsApp.</p>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="features-section__list d-flex flex-column flex-md-row align-items-center">
						<img src="@asset('images/icon-custom-exp.svg')" class="icon me-md-3" alt="Expériences authentiques">
						<div>
							<h6 class="mb-1 text-white">Expériences authentiques</h6>
							<p class="m-0 fs-xs d-none d-md-block opacity-75">Créez vos propre expériences avec l'aide de nos experts.</p>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="features-section__list d-flex flex-column flex-md-row align-items-center">
						<img src="@asset('images/icon-low-budget.svg')" class="icon me-md-3" alt="À petit budget">
						<div>
							<h6 class="mb-1 text-white">À petit budget</h6>
							<p class="m-0 fs-xs d-none d-md-block opacity-75">Profitez d'expériences et d’itinéraires inoubliables.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<x-activity-carousel type="featured"  title="les activités en vedette"/>

	@php
		$selected_category = get_field( 'selected_category' );
		$term = get_term_by( 'id', $selected_category, 'product_cat' );
		$btn = [
			'link' => get_category_link($term->term_id),
			'title' =>  __('Voir les activités ') . $term->name
		];
		$title = __('Les activités ') . $term->name;


	@endphp


	<x-activity-carousel type="category" :term="$term->term_id" :btn="$btn" :title="$title" class="bg-white"/>



	<?php
	$num_queries_start = get_num_queries();
	$custom_activity_image = get_field( 'custom_activity_image' );
	$custom_activity_link = get_field( 'custom_activity_link' );

	?>
	@if($custom_activity_link)
	<section class="section custom-activity-section section-padding bg-primary">
		<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6">
						<div class="img-cover h-100 card p-1">

							<?php if ( $custom_activity_image ) :  ?>
							<a href="{{ esc_url( $custom_activity_link['url']) }}">
								{!! wp_get_attachment_image( $custom_activity_image, 'large',false, ['class' => 'rounded'] ) !!}
{{--								<img src="{{ esc_url( $custom_activity_image['sizes']['large'] ) }}" class="img-fluid rounded" alt="{{ esc_attr( $custom_activity_image['alt'] ) }}">--}}
							</a>
							<?php endif; ?>

						</div>
					</div>
					<div class="col-lg-6 rounded-end">
						<div class="text-white p-4 p-lg-5">
							<div class="section-title section-title--decoration">
								<span class="text-white">{{ the_field( 'custom_activity_sub_title' ) }}</span>
								<h2 class="h4 text-secondary">{{ the_field( 'custom_activity_title' ) }}</h2>
							</div>

							<p>{{ the_field( 'custom_activity_description' ) }}</p>

							<?php if ( $custom_activity_link ) : ?>
							<a href="{{ esc_url( $custom_activity_link['url']) }}" class="btn btn-tertiary">{{ esc_html( $custom_activity_link['title'] ) }}</a>
							<?php endif; ?>
						</div>
					</div>
			</div>
		</div>
	</section>
	@endif



	<section class="section activity-section section-padding">
		<div class="container">

			<div class="section-title text-center mb-5">
				<h2 class="text-secondary">{{ __('Les activités par région', 'wtd') }}</h2>
				<p class="lead opacity-75">{{ __('Découvrer toutes les activités par région, facilement et à un tarif exceptionnel!', 'wtd') }}</p>
			</div>

			<div class="activity__card-carousel swiper-container">
				<div class="home-activities-carousel swiper">
					<!-- Additional required wrapper -->
					<div class="swiper-wrapper">
						<!-- Slides -->
						@foreach($activitiesRegions as $region)
						<!-- Slide -->
						<div class="swiper-slide region-slide pb-4">
							<article class="category-card">
								<a href="{{ esc_url( get_term_link($region)) }}">
									<div class="category-card__thumb">
										<?php $image = get_field('image', 'region_'. $region->term_id); ?>
										{!! wp_get_attachment_image($image,'medium') !!}
									</div>
									<h4 class="category-card__label">{{ __('Découvrir la région', 'wtd') }}</h4>
									<div class="category-card__info">
										<h3 class="category-card__info__name mb-0">{{$region->name}}</h3>
										<p class="category-card__info__number m-0">{{ __('Voir les activités', 'wtd') }}</p>
									</div>
								</a>
							</article>
						</div>
						@endforeach

					</div>
					<!-- If we need pagination -->
					<div class="swiper-pagination"></div>

					<!-- If we need navigation buttons -->
				</div>
{{--				<div class="swiper-button-prev swiper-nav-btn">--}}
{{--					<i class="fas fa-arrow-left"></i>--}}
{{--				</div>--}}
{{--				<div class="swiper-button-next swiper-nav-btn">--}}
{{--					<i class="fas fa-arrow-right"></i>--}}
{{--				</div>--}}
			</div>

		</div>
	</section>



	<section class="section activity-section section-padding bg-dark pb-0 pb-md-5">
		<div class="container-fluid px-0">

			<div class="section-title text-center mb-5">

				<h2 class="text-secondary">Les activités par catégorie</h2>
			</div>

			<div class="row g-0 g-md-4  justify-content-center">
				@foreach($activitiesCategories as $category)
					<div class="col-6 col-md-5 col-lg-3">

						<article class="category-card">
							<a href="{{ esc_url( get_term_link($category)) }}">
								<div class="category-card__thumb">
{{--									{!! woocommerce_subcategory_thumbnail( $category ); !!}--}}
									{!! wp_get_attachment_image( get_term_meta( $category->term_id, 'thumbnail_id', true ),'category_thumbnail' ); !!}

								</div>
								<span class="category-card__label h4">{{ __('Découvrir', 'wtd') }}</span>
								<div class="category-card__info">
									<h3 class="category-card__info__name mb-0">{{$category->name}}</h3>
									<p class="category-card__info__number m-0">{{ __('Voir les activités', 'wtd') }}</p>
								</div>
							</a>
						</article>

					</div>
				@endforeach
			</div>
		</div>
	</section>

	<section class="section testimonials-section section-padding bg-white">
		<div class="container">
			<div class="section-title section-title--decoration text-center mb-5">
				<span>Avis</span>
				<h2>Nos clients sont satisfaits</h2>
			</div>
			<div class="row justify-content-center align-items-stretch">


				@foreach($testimonials as $comment)

					<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
						<a href="{{ get_permalink( $comment->comment_post_ID ) }}" class="card blockquote-card shadow-none border-0 bg-light mb-5 h-100 text-body" >
								<blockquote  class="p-4 px-md-5 mb-0 comment_container h-100">
									<div class="meta mb-4">

										<div class="d-flex align-items-center">
											<div class="rounded-circle overflow-hidden d-inline-block me-3">
												{!! get_avatar( $comment->comment_author_email, $size = '48' ) !!}
											</div>

											<div>
												<h6 class="mb-0 woocommerce-review__author">
													{{ $comment->comment_author }}
												</h6>
												<span class="rating-stars">
													{!! the_rating_stars(get_comment_meta( $comment->comment_ID, 'rating', true )) !!}
												</span>
											</div>

										</div>
									</div>

									<p class="mt-2 mb-1 small">
										{{ limit_text($comment->comment_content, 20) }}
									</p>

									<footer class="fs-xs mt-2 text-capitalize">
										<?php
										echo '<i class="fas fa-check-circle text-success me-1"></i> <span class="woocommerce-review__verified verified opacity-75">' . esc_attr__( 'verified owner', 'woocommerce' ) . '</span> <span class="woocommerce-review__dash">&ndash;</span>';
										$timestamp = strtotime( $comment->comment_date ); //Changing comment time to timestamp
										$date = date('d F Y', $timestamp);
										?>

										<time class="woocommerce-review__published-date opacity-75" datetime="{{ $date }}">
											{{ $date }}
										</time>
									</footer>


								</blockquote>
						</a>
					</div>
				@endforeach

			</div>
		</div>
	</section>

	<section class="section faq-section section-padding" style="background-image: url('https://www.whattodotunisia.com/wp-content/uploads/2022/05/faq-wtd.jpg')">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-4 mb-5 mb-lg-0">
					<?php
						$faq_page_link = get_field( 'faq_page_link' );
						?>
					<div class="section-title section-title--decoration">
						<span>FAQ</span>
						<h2 class="text-white">{{ the_field( 'faq_title' ) }}</h2>
					</div>
					<p class="lead text-white opacity-75">{!! the_field( 'faq_description' ) !!}</p>
					<a href="<?php echo esc_url( $faq_page_link); ?>" class="link-secondary">{{ __('Voir plus', 'wtd') }} <i class="fas fa-plus-circle ms-2"></i></a>
				</div>

				<div class="col-lg-8">
					<!-- Accordion -->
					<div class="accordion accordion-flush rounded shadow overflow-hidden accordion-faq" id="accordionFAQ">
						<!-- Accordion Item -->

						<?php $faqs = get_field( 'faqs' ); ?>
						@if($faqs)
							@foreach ( $faqs as $faq )
								<?php setup_postdata( $faq ); ?>

								@if($loop->index == 0)
									<div class="accordion-item">
										<div class="accordion-header" id="headingCurious-{{$loop->index}}">
											<a class="accordion-button" role="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$loop->index}}" aria-expanded="true" aria-controls="collapse-{{$loop->index}}">
												<i class="fas fa-info-circle text-secondary me-2"></i> {{ $faq->post_title }}
											</a>
										</div>
										<div id="collapse-{{$loop->index}}" class="accordion-collapse collapse show" aria-labelledby="headingCurious-{{$loop->index}}" data-bs-parent="#accordionFAQ">
											<div class="accordion-body">
												@content
											</div>
										</div>
									</div>
								@else
									<div class="accordion-item">
										<div class="accordion-header" id="headingCurious-{{$loop->index}}">
											<a class="accordion-button collapsed" role="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$loop->index}}" aria-expanded="false" aria-controls="collapse-{{$loop->index}}">
												<i class="fas fa-info-circle text-secondary me-2"></i> {{ $faq->post_title }}
											</a>
										</div>
										<div id="collapse-{{$loop->index}}" class="accordion-collapse collapse" aria-labelledby="headingCurious-{{$loop->index}}" data-bs-parent="#accordionFAQ">
											<div class="accordion-body">
												@content
											</div>
										</div>
									</div>
								@endif
							@endforeach
							<?php wp_reset_postdata(); ?>
						@endif
						<!-- End Accordion Item -->

					</div>
					<!-- End Accordion -->
				</div>


			</div>


		</div>
		<!-- End FAQ -->
	</section>


	<section class="section blog-section section-padding bg-white">
		<div class="container">
			<div class="section-title section-title--decoration text-center mb-5">
				<span>Blog</span>
				<h2>Les dernières actualités</h2>
			</div>
			<div class="row">
				@query([
					'post_type' => 'post',
					'posts_per_page' => 2
				])
				@posts()
				<div class="col-lg-6 mb-5 mb-lg-0">
					<article class="card article-card border-0">
						<div class="row g-0 align-items-center">
							<div class="col-md-4 card-image h-100">
								<div class="img-cover">
									@thumbnail
								</div>
							</div>
							<div class="col-md-8">
								<div class="card-body p-4">
									<div class="card-date mb-1 fs-xs fw-bold text-uppercase">
										<i class="fas fa-calendar me-1 text-secondary"></i>
										<span class="opacity-50">@published</span>
									</div>
									<h3 class="card-title h4 mb-2">@title</h3>
									<p class="card-text mt-0 mb-3">@excerpt</p>
									<a href="@permalink" class="stretched-link small text-end d-block">{{ __('Lire la suite', 'wtd') }}</a>
								</div>
							</div>
						</div>
					</article>
				</div>
				@endposts

			</div>
			<div class="text-center mt-5">
				<a href="{{ get_permalink( get_option( 'page_for_posts' ) ) }}" class="btn btn-primary">{{ __('Voir le blog', 'wtd') }}</a>
			</div>

		</div>
	</section>


	@php(the_content())

	{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}

@endsection
