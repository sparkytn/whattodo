@include('partials.header')

@php(do_action('wtd_after_header_alert'))
  <main class="main">
    @yield('content')
  </main>

{{--  @hasSection('sidebar')--}}
{{--    <aside class="sidebar">--}}
{{--      @yield('sidebar')--}}
{{--    </aside>--}}
{{--  @endif--}}
@include('partials.footer')
