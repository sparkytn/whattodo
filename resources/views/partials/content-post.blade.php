<article class="card article-card border-0">
	<div class="card-image">
		<a href="@permalink" class="img-cover">
			@thumbnail
		</a>
	</div>
	<div class="card-body">
		<div class="card-date mb-1 fs-xs fw-bold text-uppercase">
			<i class="fas fa-calendar-alt me-1 text-secondary"></i>
			<span class="opacity-50">@published</span>
		</div>
		<h3 class="card-title h4 mb-2">@title</h3>
		<div class="text-card">
			@excerpt
		</div>
		<a href="@permalink" class="stretched-link small text-end d-block">Lire la suite</a>
	</div>
</article>
