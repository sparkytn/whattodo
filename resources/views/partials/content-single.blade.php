<article @php(post_class())>

	{{--  <header>--}}
	{{--    @include('partials/entry-meta')--}}
	{{--  </header>--}}

	<div class="entry-content">
		@php(the_content())
	</div>



	{{--  @php(comments_template())--}}

	<footer>

		<section class="section share-section mt-5">
			<label class="form-label text-tertiary d-block d-sm-inline-block me-4">{{ __('Partager cette article') }} :</label>
			<a btn-social-share class="btn btn-sm btn-share btn-twitter me-2 my-2" href="#" target="_blank" id="tw-share"><i class="fab fa-twitter"></i> <span class="d-none d-md-inline">Twitter</span></a>
			<a btn-social-share class="btn btn-sm btn-share btn-linkedin me-2 my-2" href="#" target="_blank" id="in-share"><i class="fab fa-linkedin"></i>  <span class="d-none d-md-inline">Linked In</span></a>
			<a btn-social-share class="btn btn-sm btn-share btn-facebook my-2" href="#" target="_blank" id="fb-share"><i class="fab fa-facebook-f"></i>  <span class="d-none d-md-inline">Facebook</span></a>
		</section>

		{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
	</footer>

</article>
