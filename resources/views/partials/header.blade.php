<?php // todo : finish top header and clear header.blade file ?>
<div class="topbar bg-dark d-none d-md-block">
  <div class="container d-flex">
      <ul class="nav">
		  <li class="nav-item">
			  <a class="nav-link px-2 me-2" href="tel:+21622016016"><i class="fas fa-headset me-2"></i>+216 22 016 016</a>
		  </li>
		  <li class="nav-item">
			  <a class="nav-link px-2 me-2" href="mailto:support@whattodotunisia.com"><i class="fas fa-envelope me-2"></i>support@whattodotunisia.com</a>
		  </li>
{{--        <li class="nav-item">--}}
{{--          <a class="nav-link px-2 me-2 text-uppercase" href="#">--}}
{{--			  <i class="fas fa-user-plus me-2 text-secondary"></i>--}}
{{--			  Fournisseur--}}
{{--		  </a>--}}
{{--        </li>--}}
      </ul>

      <ul class="nav ms-auto">
        <li class="nav-item">
          <a class="nav-link px-2" href="https://www.facebook.com/whattodotunisia" target="_blank"><i class="fab fa-facebook-f"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link px-2" href="https://www.youtube.com/channel/UC3Imc9EXFYyQ3cDk_QG_-tg/featured" target="_blank"><i class="fab fa-youtube"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link ps-2 pe-0" href="https://www.instagram.com/whattodotunisia/" target="_blank"><i class="fab fa-instagram"></i></a>
        </li>
      </ul>
  </div>
</div>


<header class="navbar navbar-expand-lg navbar-light bg-white shadow-sm sticky-header">
	<div class="container">
		<a href="{{ site_url() }}" class="navbar-brand me-auto me-xl-4">
			@svg('images.what-to-do-in-tunisia','main-logo', ['aria-label' => 'Logo','alt'=> 'What To Do Tunisia'])
{{--			@svg('images.what-to-do-in-tunisia-mobile','main-logo d-sm-none', ['aria-label' => 'Logo','alt'=> 'What To Do Tunisia'])--}}
		</a>


		@php($cart_items = count( WC()->cart->get_cart() ))
		@if($cart_items)
			<div class="order-lg-2 order-0 ms-auto">
				<a href="{{ get_permalink( wc_get_page_id( 'cart' ) ) }}" class="d-flex align-items-center nav-link">
					<div class="cart-notification position-relative">
						<i class="fas fa-cart-plus fs-5 text-secondary"></i>
						<span class="position-absolute badge rounded-pill bg-primary fs-xxs" style="right: -13px;top: -8px;"> {{ $cart_items }}</span>
					</div>
				</a>
			</div>
		@endif


{{--		@php($bookings = get_user_confirmed_bookings() )--}}
{{--		@if( $bookings && 0 === $cart_items)--}}
{{--			<div class="dropdown dropdown-hover order-0 order-lg-2 ms-auto">--}}
{{--				<a href="#" class="d-flex align-items-center nav-link px-3" data-bs-toggle="dropdown" aria-expanded="false">--}}
{{--					<div class="booking-notification position-relative">--}}
{{--						<i class="fas fa-bell fs-5 text-secondary fa-shake"></i>--}}
{{--						<span class="position-absolute badge rounded-pill bg-primary fs-xxs" style="right: -13px;top: -8px;"> {{ count($bookings) }}</span>--}}
{{--					</div>--}}
{{--				</a>--}}
{{--				<ul class="dropdown-menu dropdown-menu-end overflow-hidden">--}}
{{--					<?php--}}
{{--					$view_booking_endpoint = yith_wcbk()->endpoints->get_endpoint( 'view-booking' );--}}
{{--					$accountPermalink = wc_get_page_permalink( 'myaccount' );--}}
{{--					?>--}}
{{--					<h6 class="dropdown-header opacity-75">--}}
{{--						<span class="d-sm-none"> {{ __('Résa. confirmée', 'wtd') }}</span>--}}
{{--						<span class="d-none d-sm-block"> {{ __('Réservation confirmée', 'wtd') }}</span>--}}
{{--					</h6>--}}
{{--					@for( $i=0; $i < count($bookings); $i++)--}}

{{--						<a href="{{ wc_get_endpoint_url($view_booking_endpoint, $bookings[$i],  $accountPermalink) }}" class="dropdown-item fs-sm">--}}
{{--							<span class="d-sm-none"> {{ __('Résa.', 'wtd') }}</span>--}}
{{--							<span class="d-none d-sm-inline"> {{ __('Réservation', 'wtd') }}</span>--}}
{{--							N° {{ $bookings[$i] }} <span class="text-white px-2 py-1 bg-success rounded ms-2 fs-xs">{{ __('Voir', 'wtd') }}</span> <br>--}}
{{--													{{ __('Est confirmée', 'wtd') }}--}}
{{--						</a>--}}
{{--					@endfor--}}
{{--					<div  class="fs-sm text-center pt-3 pb-2">--}}
{{--						<a href="{{ wc_get_endpoint_url('bookings','', $accountPermalink) }}" class="btn btn-sm btn-tertiary btn-sm"><span>{{ __('Voir toutes', 'wtd') }}</span></a>--}}
{{--					</div>--}}
{{--				</ul>--}}
{{--			</div>--}}
{{--		@endif--}}





		@if(is_user_logged_in())
		<div class="dropdown dropdown-hover user-dropdown order-lg-2 ms-1">
			<a href="{{get_permalink( wc_get_page_id( 'myaccount' ) )}}" class="d-flex align-items-center py-1 text-body" data-bs-toggle="dropdown" aria-expanded="false">
				<div class="user-avatar">
					{!! get_avatar( wp_get_current_user()->ID, $size = '32', '', wp_get_current_user()->display_name, ['class'=>'rounded-circle border'] ) !!}
				</div>
				<div class="ms-2 flex-column align-content-center d-none d-lg-flex">
					<span class="text-uppercase fs-xs opacity-75">{{ __('Bonjour', 'wtd') }}</span>
					<span class="text-uppercase fw-bold text-primary fs-sm">{{ wp_get_current_user()->first_name }} {{ wp_get_current_user()->last_name }}</span>
				</div>

			</a>
			<ul class="dropdown-menu dropdown-menu-end overflow-hidden">

				@foreach(wc_get_account_menu_items() as $endpoint => $label)
					@if($endpoint != 'orders')
						<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>" class="dropdown-item woocommerce-MyAccount-navigation-link">
							<i class="nav-icon nav-icon__{{$endpoint}}"></i>
							<?php echo esc_html( $label ); ?>
						</a>
					@endif
				@endforeach
			</ul>
		</div>
		@else
			<div class="order-lg-2 ms-1">
				<a href="{{get_permalink( wc_get_page_id( 'myaccount' ) )}}" class="d-flex align-items-center py-1 text-body">
					<div class="user-avatar">
						<i class="fas fa-user fs-4 text-secondary"></i>
					</div>
					<div class="ms-2 flex-column align-content-center d-none d-lg-flex">
						<span class="text-uppercase fs-xxs opacity-75">{{__('Bonjour','wtd')}}</span>
						<span class="text-uppercase fw-bold text-primary fs-xs">{{__('Connexion','wtd')}}</span>
					</div>

				</a>
			</div>
		@endif

		<button type="button" class="navbar-toggler order-3 border-0 ms-1" data-bs-toggle="offcanvas" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>




		<div class="offcanvas offcanvas-end" id="navbarNav">

			<div class="offcanvas-header shadow-sm bg-primary">
				<div>
					@svg('images.what-to-do-in-tunisia-white','menu-logo d-block', ['aria-label' => 'Logo','alt'=> 'What To Do Tunisia'])
{{--					<h6 class="offcanvas-title text-white opacity-75">--}}
{{--						{{ __('Menu', 'wtd') }}--}}
{{--					</h6>--}}
				</div>

				<button type="button" class="btn-close text-white" data-bs-dismiss="offcanvas"></button>
			</div>
			<div class="offcanvas-body">
				<ul class="navbar-nav mt-0">
					<li class="nav-item d-lg-none bg-tertiary">
						<a href="{{ $shopURL }}" class="nav-link text-white">{{ __('Toutes les activités', 'wtd') }} <i class="fas fa-chevron-right ms-2"></i></a>
					</li>
					<li class="nav-item dropdown dropdown-hover has-megamenu me-lg-2">
						<a href="#" class="nav-link align-items-center border-end-lg pe-lg-4 dropdown-toggle"  data-bs-toggle="dropdown" aria-expanded="false">
							<i class="fas fa-list me-1 text-secondary d-none d-lg-inline-block"></i>
							{{ __('Les  Activités', 'wtd') }} <span class="d-lg-none">{{ __('par Catégorie', 'wtd') }}</span>
						</a>
						<div class="dropdown-menu megamenu">
							<div class="container">
								<div class="row">
									<div class="col-12 my-2 d-none d-lg-inline-block">
										<a href="{{ $shopURL }}" class="btn btn-tertiary btn-sm fs-xs text-uppercase fw-bold mx-2">{{ __('Toutes les activités', 'wtd') }} <i class="fas fa-chevron-right ms-2"></i></a>
									</div>
									<div class="col-md-6 col-lg-5 border-end-md mb-3 mb-lg-0">
										<h6 class="dropdown-header small fs-xs text-muted pt-3">
											<i class="fas fa-list me-1 text-secondary "></i>
											<span class="opacity-75">{{ __('Par Catégorie', 'wtd') }} :</span>
										</h6>
										@foreach($activitiesCategories as $category)
											<div>
												<a class="dropdown-item" href="{{ esc_url( get_term_link($category)) }}" title="{{ __('Trouvez toutes les activités', 'wtd') }} {{$category->name}}">
													<i class="fa-{{$category->slug}} text-tertiary me-1 icon-activity"></i> {{$category->name}}
												</a>
											</div>
										@endforeach
										<div>
											<a class="dropdown-item" href="/activite-personnalisee" title="{{ __('Trouvez toutes les activités', 'wtd') }}">
												<i class="fa-custom-activity text-tertiary me-1 icon-activity"></i> Personnalisée
											</a>
										</div>
									</div>
									<div class="col-md-6 col-lg-7">
										<h6 class="dropdown-header small fs-xs text-muted pt-3">
											<i class="fas fa-map-marker-alt me-1 text-secondary "></i>
											<span class="opacity-75">{{ __('Par Région', 'wtd') }} :</span>
										</h6>
										<div class="row">
											@foreach($activitiesRegions as $regions)
												<div class="col-lg-6">
													<a class="dropdown-item" href="{{ esc_url( get_term_link($regions)) }}" title="{{ __('Trouvez toutes les activités à', 'wtd') }} {{$regions->name}}">
														{{$regions->name}}
													</a>
												</div>
											@endforeach
										</div>
									</div>

								</div>
							</div>

						</div>
					</li>
				</ul>

				@if (has_nav_menu('primary_navigation'))
					{!! wp_nav_menu($primaryMenuArgs) !!}
				@endif
			</div>

			<div class="offcanvas-footer bg-light border-top  pt-3 pb-3">
				<div class="offcanvas-footer__item d-flex mb-2">
					<i class="fas fa-headset fs-4 text-tertiary"></i>
					<div class="ps-3">
						<div class="fs-xs opacity-75 label">Support</div>
						<a class="text-body h6 fs-sm" href="tel:+21622016016">+216 22 016 016</a>
					</div>
				</div>
				<div class="offcanvas-footer__item d-flex mb-1">
					<i class="fas fa-envelope-open fs-4 text-tertiary"></i>
					<div class="ps-3">
						<div class="fs-xs opacity-75 label">Email</div>
						<a class="text-body h6 fs-sm" href="mailto:support@whattodotunisia.com">support@whattodotunisia.com</a>
					</div>
				</div>
				<div class="offcanvas-footer__item d-block">
					<h6 class="pt-3 pb-1 text-body h6 fs-xs text-uppercase opacity-75">{{ __('Suivez-nous', 'wtd') }}</h6>
					<a class="btn-social bs-outline bs-facebook me-2 mb-1" href="https://www.facebook.com/whattodotunisia" target="_blank"><i class="fab fa-facebook"></i></a>
					<a class="btn-social bs-outline bs-youtube me-2 mb-1" href="https://www.youtube.com/channel/UC3Imc9EXFYyQ3cDk_QG_-tg/featured" target="_blank"><i class="fab fa-youtube"></i></a>
					<a class="btn-social bs-outline bs-instagram me-2 mb-1" href="https://www.instagram.com/whattodotunisia/" target="_blank"><i class="fab fa-instagram"></i></a>
				</div>
			</div>
		</div>
	</div>
</header>

