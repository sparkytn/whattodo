<section class="bg-dark home-hero">

	<div class="home-hero-carousel swiper">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->

			<?php $banner_images_images = get_field( 'banner_images' ); ?>
			<?php if ( $banner_images_images ) :  ?>
			<?php foreach ( $banner_images_images as $banner_images_image ): ?>

			<div class="swiper-slide">
				<div class="object-fit h-100">
					{!! wp_get_attachment_image( $banner_images_image, 'full') !!}
{{--					<img src="<?php echo esc_url( $banner_images_image['url']); ?>" alt="<?php echo esc_attr( $banner_images_image['alt'] ); ?>" />--}}
					<div class="swiper-lazy-preloader"></div>
				</div>
			</div>
			<?php endforeach; ?>
			<?php endif; ?>

		</div>
	</div>

	<div class="home-hero-form">
		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-10">
					<div class="text-center text-lg-start">
						<p class="text-uppercase lead text-white fw-bold text-shadow subtitle">{{ the_field( 'banner_sub_title' ) }}</p>
						<h1 class="title display-4 text-white fw-bold text-uppercase">{{ the_field( 'banner_title' ) }}</h1>
					</div>

				</div>
			</div>
			<div class="form-bar-search">
				{!! do_shortcode("[booking_search_form id=354]") !!}
			</div>
		</div>
	</div>
</section>


