<footer class="main-footer position-relative z-index-10 d-print-none bg-dark">
	<!-- Main block - menus, subscribe form-->
	<div class="py-4 py-md-6 bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-xl-5 mb-4 mb-xl-0">
					<a class="navbar-brand" href="{{ home_url('/') }}">
						@svg('images.what-to-do-in-tunisia-white','main-logo', ['aria-label' => 'Logo'])
					</a>
					<p class="small my-2">Plateforme digitale de promotion d’artisans de tourisme alternatif, sportif et culturel. <br>
						Nous sélectionnons pour vous le meilleur et les incontournables en Tunisie. Avec What to do Tunisia il n’a jamais été aussi simple et accessible de vivre des moments inoubliables.</p>
					<section class="footer-widget mt-3">
						<div class="title text-capitalize text-success small"><i class="fas fa-lock"></i> Payment en ligne sécurisé</div>
						<ul class="list-unstyled list-inline">
							<li class="list-inline-item"><img src="@asset('images/master-c-logo.svg')" width="40px" alt="Payment en ligne avec Master Card"></li>
							<li class="list-inline-item"><img src="@asset('images/visa-logo.svg')" width="40px" alt="Payment en ligne avec Visa"></li>
						</ul>
					</section>
				</div>
				<div class="col-xl-7 col-lg-12">
					<div class="row">
						<div class="col-md-4 col-6 mb-4 mb-lg-0">
							@php dynamic_sidebar('widget-footer-area-1') @endphp
						</div>
						<div class="col-md-4 col-6 mb-4 mb-lg-0">
							@php dynamic_sidebar('widget-footer-area-2') @endphp
						</div>
						<div class="col-md-4">
							{{--					<section class="footer-widget mb-5">--}}
							{{--						<h3 class="h6 title">Newsletter</h3>--}}
							{{--						<div class="mb-3">--}}
							{{--							<div class="input-group">--}}
							{{--								<span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>--}}
							{{--								<input type="text" class="form-control" placeholder="email@email.com" aria-label="Example text with button addon" aria-describedby="button-addon1">--}}
							{{--								<button class="btn btn-secondary" type="button" id="button-addon1">Valider</button>--}}
							{{--							</div>--}}
							{{--						</div>--}}
							{{--					</section>--}}
							<section class="footer-widget">
								<h3 class="h6 title">Suivez-nous</h3>
								<ul class="list-inline">
									<li class="list-inline-item"><a href="https://www.facebook.com/whattodotunisia" target="_blank" title="What to do Tunisia - Facebook"><i class="fab fa-facebook"></i></a></li>
									<li class="list-inline-item"><a href="https://www.instagram.com/whattodotunisia/" target="_blank" title="What to do Tunisia - Instagram"><i class="fab fa-instagram"></i></a></li>
									<li class="list-inline-item"><a href="https://www.youtube.com/channel/UC3Imc9EXFYyQ3cDk_QG_-tg/featured" target="_blank" title="What to do Tunisia - Youtube"><i class="fab fa-youtube"></i></a></li>
								</ul>

							</section>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Copyright section of the footer-->
	<div class="py-3 fw-light footer-bot text-muted">
		<div class="container">
			<div class="row text-center text-md-start">
				<div class="col-md-6">
					<p class="text-sm mb-md-0">© {{ date('Y') }}, What To Do Tunisia.  All rights reserved.</p>
				</div>
				<div class="col-md-6 text-center text-md-end">
					<span class="small opacity-75">
						Site réalisé par <a href="http://www.ramijegham.com" class="" target="_blank">Sparky</a>
					</span>
				</div>
			</div>
		</div>
	</div>
</footer>
