@extends('layouts.app')

@section('content')
			  <div class="container">
				  @if (! have_posts())
					  <div class="container py-5 mb-lg-3">
						  <div class="row justify-content-center pt-lg-4 text-center">
							  <div class="col-lg-5 col-md-7 col-sm-9 mb-4">
								  <img class="d-block mx-auto mb-5 img-fluid" src="@asset('images/404-img.svg')" width="300" alt="404 Error">
								  <h1 class="h2 text-tertiary mb-1">404 error</h1>
								  <p class="lead  mt-2 mt-0">Vous vous êtes perdu ?</p>
								  <p class="m-0">Il semblerait que la page que vous cherchez n’est pas ici.</p>
							  </div>
						  </div>

						  <div class="row mt-4">
							  <div class="col-lg-4 mb-3"><a class="card h-100 border-0 shadow-sm" href="{{ home_url('/') }}">
									  <div class="card-body">
										  <div class="d-flex align-items-center"><i class="fas fa-home text-tertiary h4 mb-0"></i>
											  <div class="ps-3">
												  <h5 class="fs-sm mb-0">Accueil</h5><span class="text-muted fs-xs">Retour à l'accueil</span>
											  </div>
										  </div>
									  </div></a></div>
							  <div class="col-lg-4 mb-3"><a class="card h-100 border-0 shadow-sm" href="{{ get_permalink( wc_get_page_id( 'shop' ) ) }}">
									  <div class="card-body">
										  <div class="d-flex align-items-center"><i class="fas fa-list text-tertiary h4 mb-0"></i>
											  <div class="ps-3">
												  <h5 class="fs-sm mb-0">Activitiés</h5><span class="text-muted fs-xs">Réservez une activité</span>
											  </div>
										  </div>
									  </div></a></div>
							  <div class="col-lg-4 mb-3"><a class="card h-100 border-0 shadow-sm" href="{{ get_permalink(FAQ_PAGE) }}">
									  <div class="card-body">
										  <div class="d-flex align-items-center"><i class="fas fa-life-ring text-tertiary h4 mb-0"></i>
											  <div class="ps-3">
												  <h5 class="fs-sm mb-0">Aide &amp; Support</h5><span class="text-muted fs-xs">Visitez notre centre d’aide</span>
											  </div>
										  </div>
									  </div></a></div>
						  </div>

					  </div>

{{--					{!! get_search_form(false) !!}--}}
				  @endif
			  </div>
@endsection
