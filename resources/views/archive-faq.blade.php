{{--
  Template Name: FAQ Archive
--}}

@extends('layouts.app')

@section('content')

	@include('partials.page-header', ['pageTitle' => 'A Propos'])
	<div class="page-body">
		<div class="container p-0">
			<div class="page-content-card mb-5">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-10 col-md-11">

							<div class="mb-5 entry-content">
								@php(the_content())
							</div>
							<div class="faqs-wrapper" id="accordionFAQ">
								<?php
								$taxonomy = 'faq_cat';
								$terms = get_terms(
									array(
										"taxonomy" => $taxonomy,
										'hide_empty' => false,
										'meta_key' => 'term_order',
										'orderby' => 'meta_value',
										'order' => 'ASC',
									));

								foreach($terms as $tax_term) { // loop through the terms
								?>
								<div class="section-title section-title--decoration text-center mb-4">
									<span><i class="fas fa-comments text-secondary fa-lg"></i></span>
									<h3>{{ $tax_term->name }}</h3>
								</div>

								<?php

								$faqs = get_posts(
									array(
										'post_type' => 'faq',
										'posts_per_page' => -1,
										'orderby' => 'menu_order',
										'order' => 'ASC',
										'tax_query' => array( // https://developer.wordpress.org/reference/classes/wp_tax_query/
											array(
												'taxonomy' => $taxonomy,
												'field' => 'id',
												'terms' => array($tax_term->term_id)
											)
										),
									));

								?>

								<div class="accordion rounded  overflow-hidden accordion-faq mb-5" id="accordionFAQ{{ $tax_term->term_id }}">

									@foreach ( $faqs as $faq )

										<div class="accordion-item">
											<div class="accordion-header" id="headingCurious-{{$faq->ID}}">
												<a class="accordion-button collapsed" role="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$faq->ID}}" aria-expanded="false" aria-controls="collapse-{{$faq->ID}}">
													<i class="fas fa-info-circle text-secondary me-2"></i> {{ $faq->post_title }}
												</a>
											</div>
											<div id="collapse-{{$faq->ID}}" class="accordion-collapse collapse" aria-labelledby="headingCurious-{{$faq->ID}}" data-bs-parent="#accordionFAQ">
												<div class="accordion-body">
													{!! wpautop($faq->post_content) !!}
												</div>
											</div>
										</div>

									@endforeach

								</div>


								<?php }?>
							</div>

							<section class="container text-center pt-1">
								<div class="card card-body shadow-none d-block border-0">
									<h2 class="h5">{{ the_field( 'faq_not_found_title' ) }}</h2>
									<i class="fas fa-life-ring fa-3x text-tertiary d-block mb-4"></i>
									<?php
									$contact_link = get_field('faq_not_found_link');

									?>
									<a class="btn btn-tertiary btn-sm" href="{{ $contact_link['url'] }}">{{ $contact_link['title'] }}</a>
									@if(get_field( 'faq_not_found_small_text' ))
										<p class="pt-2 text-muted small">{{ the_field( 'faq_not_found_small_text' ) }}</p>
									@endif
								</div>
							</section>

						</div>
					</div>
				</div>
			</div>
		</div>

		<x-activity-carousel type="featured" class="bg-light" title="les activités en vedette"/>

@endsection
