{{--
  Template Name: Custom Activity Page
--}}
@extends('layouts.app')

@section('content')

{{--	@include('partials.page-header')--}}

<div class="bg-primary section-padding" style="background: url(@asset('images/custom-activity-bg-header.jpg')) top center; background-size: cover;">
	<div class="container mb-4">
		<x-breadcrumb></x-breadcrumb>

		<h1 class="h5 title mb-0 text-white  mt-4 mb-2">{!! the_title() !!}</h1>
		<div class="row">
			<div class="col-lg-6 col-md-8 col-11">
				<h2 class="display-4 text-secondary fw-bold text-uppercase">
					PREPARONS
					ENSEMBLE DES
					VACANCES QUI VOUS
					RESSEMBLENT
				</h2>
				<p class="lead text-white">Une base de données de 250 prestataires et une expérience confirmée dans la conception et l’organisation d’aventures exceptionnelles, les experts WTD sont à votre service!</p>

				<a href="#request" class="btn btn-tertiary  mt-3 mb-4">CONSULTATION GRATUITE</a>
			</div>
		</div>

	</div>
</div>

<div class="bg-primary section-padding pt-0 page-header--overlap" style="background: url(@asset('images/custom-activity-bg-content.jpg')) top center; background-size: cover;">

	<div class="container mb-3">
		<div class="boxes">
			<div class="row justify-content-between">
				<div class="col-lg-4 mb-4 mb-lg-0">

					<div class="d-flex align-items-center border rounded p-3 bg-white h-100" style="margin: -50px 0 50px">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="45px"  viewBox="0 0 37.3 37.4" xml:space="preserve">
									<path fill="#243E90" d="M0,12.1c0.1-0.4,0.1-0.9,0.2-1.3c0.4-2,1.2-3.8,2.5-5.4c0,0,0.1-0.1,0.1-0.1c3.1,3.1,6.2,6.2,9.2,9.2c-0.5,0.7-0.7,1.5-0.3,2.3c0.2,0.4,0.4,0.7,0.7,1c2.3,2.4,4.7,4.7,7.1,7.1c0.7,0.7,1.9,1.2,3.3,0.3c3.1,3.1,6.2,6.1,9.2,9.2c-0.9,0.8-1.9,1.4-3,1.9c-1.6,0.7-3.2,1-4.9,1c-2.8-0.1-5.3-1-7.4-2.8c-0.4-0.3-0.7-0.7-1.1-1c-4-4-8-8-12.1-12.1c-1.4-1.4-2.4-2.9-3-4.8c-0.3-0.9-0.4-1.8-0.6-2.7C0,14,0,13.9,0,13.8C0,13.2,0,12.7,0,12.1z"/>
							<path fill="#243E90" d="M13.7,12.9c-3.1-3.1-6.2-6.2-9.3-9.3c1.2-1.6,3-2.2,4.7-1.3C9.4,2.6,9.7,2.8,9.9,3c1.5,1.4,2.9,2.9,4.4,4.4c1.5,1.5,1.5,3.4,0,4.9C14.1,12.5,13.9,12.7,13.7,12.9z"/>
							<path fill="#243E90" d="M24.5,23.7c0.6-0.6,1.2-1.3,2.1-1.5c1.3-0.3,2.4-0.1,3.4,0.8c1.5,1.5,3,3,4.5,4.5c1.4,1.4,1.3,3.3,0,4.8c-0.2,0.2-0.4,0.5-0.6,0.7C30.7,29.9,27.6,26.8,24.5,23.7z"/>
							<g>
								<path fill="#AFBBBF" d="M34.3,17.5c-0.2-0.1-0.3-0.2-0.5-0.3c-1-0.6-1.9-1.3-2.9-1.9c-0.1-0.1-0.2-0.1-0.4-0.1c-1.8,0-3.6,0-5.4,0c-0.7,0-1.3-0.3-1.6-1c-0.1-0.2-0.2-0.5-0.2-0.8c0-0.3,0-0.6,0-0.9c0-0.1,0-0.2,0.1-0.2c0.3-0.2,0.6-0.4,0.9-0.6c0.1,0,0.2-0.1,0.3-0.1c1.7,0,3.4,0,5.2,0c1.3,0,2.4-0.8,2.8-2c0.1-0.3,0.2-0.7,0.2-1c0-1.3,0-2.5,0-3.8c0-0.1,0-0.1,0-0.2c0.1,0,0.1,0,0.1,0c0.9,0,1.8,0,2.7,0c0.7,0,1.3,0.3,1.6,1c0.1,0.2,0.1,0.4,0.2,0.5c0,2.5,0,4.9,0,7.4c0,0,0,0,0,0.1c-0.1,0.9-0.9,1.5-1.8,1.5c-0.1,0-0.3,0-0.5,0c0,0.5,0,1,0,1.5c0,0.4-0.1,0.6-0.4,0.8C34.4,17.5,34.4,17.5,34.3,17.5z"/>
								<path fill="#FD5431" d="M19.7,10.5c-0.2,0-0.3,0-0.5,0c-1.1,0-1.8-0.7-1.8-1.8c0-2.1,0-4.2,0-6.3c0-0.2,0-0.4,0-0.6c0-1.1,0.8-1.8,1.8-1.8c1.2,0,2.5,0,3.7,0c2.2,0,4.5,0,6.7,0c0.8,0,1.3,0.3,1.7,1c0.1,0.2,0.2,0.5,0.2,0.8c0,2.3,0,4.6,0,6.9c0,1.1-0.8,1.8-1.8,1.8c-1.8,0-3.5,0-5.3,0c-0.3,0-0.5,0.1-0.7,0.2c-1,0.7-1.9,1.3-2.9,2c-0.1,0.1-0.2,0.1-0.4,0.2c-0.4,0.1-0.7-0.2-0.7-0.6C19.7,11.6,19.7,11.1,19.7,10.5z M24.4,4.7c0.8,0,1.6,0,2.4,0c0.6,0,1.2,0,1.7,0c0.4,0,0.6-0.3,0.6-0.7c-0.1-0.3-0.3-0.5-0.7-0.5c-2.7,0-5.4,0-8,0c-0.1,0-0.1,0-0.2,0c-0.2,0-0.4,0.2-0.5,0.4c-0.1,0.4,0.1,0.8,0.6,0.8C21.7,4.7,23.1,4.7,24.4,4.7z M22.7,7c0.8,0,1.6,0,2.3,0c0.3,0,0.5-0.1,0.6-0.4c0.1-0.4-0.1-0.8-0.6-0.8c-1.3,0-2.6,0-3.8,0c-0.3,0-0.6,0-0.9,0c-0.3,0-0.5,0.2-0.5,0.4C19.7,6.7,19.9,7,20.4,7C21.1,7,21.9,7,22.7,7z"/>
							</g>
								</svg>

						<div class="ps-3">
							<h6 class="fs-base text-primary mb-0 text-uppercase">PARTAGEZ AVEC NOUS VOS PREFERENCES</h6>
							<p class="mb-0 fs-ms text-dark opacity-50 m-0 small">Contactez-nous et discutez avec l’un de nos experts.</p>
						</div>
					</div>

				</div>

				<div class="col-lg-4 mb-4 mb-lg-0">


					<div class="d-flex align-items-center border rounded p-3 bg-white h-100" style="margin: -50px 0 50px">
						<!-- Generator: Adobe Illustrator 25.2.0, SVG Export Plug-In  -->
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="45px" viewBox="0 0 35.5 48.3" xml:space="preserve">
									<path fill="#AFBBBF" d="M32.4,0c0.2,0.1,0.5,0.1,0.7,0.2c1.4,0.5,2.4,1.8,2.4,3.4c0,3.6,0,7.1,0,10.7c0,2-1.6,3.6-3.6,3.6c-2.9,0-5.9,0-8.8,0c-2,0-3.6-1.6-3.6-3.6c0-3.6,0-7.1,0-10.7c0-1.7,1.2-3.1,2.9-3.5c0.1,0,0.2-0.1,0.3-0.1C25.9,0,29.1,0,32.4,0z"/>
							<path fill="#FD5431" d="M14.4,0c0.3,0.1,0.5,0.1,0.8,0.2c1.5,0.6,2.3,1.7,2.3,3.3c0.1,3.6,0,7.2,0,10.8c0,1.6-1.1,3-2.6,3.4c0-0.2,0-0.3,0-0.5c0-1.3,0-2.6,0-4c-0.1-2.5-1.7-4.5-4.1-5.1c-2.3-0.6-4.8,0.5-6,2.6c-0.5,0.8-0.7,1.7-0.7,2.7c0,1.3,0,2.6,0,3.9c0,0.1,0,0.3,0,0.5c-1.2-0.4-2-1.1-2.3-2.2c-0.2-0.5-0.2-1-0.2-1.5c0-3.4,0-6.7,0-10.1c0-2,1-3.3,3-3.8c0,0,0.1,0,0.1-0.1C7.9,0,11.2,0,14.4,0z"/>
							<path fill="#243E90" d="M12.2,22.2c0.8-1.4,1.8-2,3.1-1.8c1.3,0.2,1.9,1.1,2.2,3c0.3-0.7,0.6-1.3,1.3-1.6c0.6-0.3,1.2-0.4,1.9-0.3c1.3,0.3,1.8,1.2,2,3.2c0.1-0.1,0.1-0.2,0.1-0.3c0.5-1.2,1.6-1.8,2.8-1.6c1.3,0.2,2.2,1.2,2.2,2.5c0,4.1,0.1,8.2-0.2,12.3c-0.2,3.8-1.6,7.3-3.7,10.5c-0.1,0.1-0.3,0.2-0.4,0.2c-5.1,0-10.1,0-15.2,0c-0.2,0-0.4-0.1-0.5-0.2C5,45.3,2.9,42,2.1,38.1c-0.6-2.9-1.3-5.8-2-8.7c-0.3-1.4,0.4-2.4,1.8-2.6c0.7-0.1,1.3-0.2,2,0.2c0.8,0.4,1.2,1,1.4,1.9c0.5,2.1,1,4.1,1.4,6.2c0,0.2,0.1,0.3,0.1,0.5c0,0,0.1,0,0.1,0c0-0.2,0-0.3,0-0.5c0-7.1,0-14.2,0-21.3c0-0.3,0-0.7,0.1-1c0.3-1.3,1.5-2.1,2.8-1.9c1.3,0.2,2.2,1.3,2.3,2.7c0,2.3,0,4.7,0,7C12.2,21,12.2,21.6,12.2,22.2z"/>
								</svg>

						<div class="ps-3">
							<h6 class="fs-base text-primary mb-0 text-uppercase">VALIDEZ VOTRE PLAN DE VACANCES PERSONNALISE</h6>
							<p class="mb-0 fs-ms text-dark opacity-50 m-0 small">Confirmez les activités et les options d’hébergement choisies et procédez au paiement.</p>
						</div>
					</div>

				</div>

				<div class="col-lg-4 mb-4 mb-lg-0">


					<div class="d-flex align-items-center border rounded p-3 bg-white h-100" style="margin: -50px 0 50px">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="45px"viewBox="0 0 40.7 48.4" xml:space="preserve">
									<path fill="#FD5431" d="M9.6,0c1.3,0.7,2.6,1.4,3.8,2.1c1.9,1.1,3.9,2.2,5.8,3.3c1.2,0.7,1.7,1.9,1.3,3.2c-0.3,1.1-1.1,1.8-2.2,2c-2,0.4-4,0.8-6.1,1.2c0.2,1,0.4,2,0.6,3c-0.9,0-1.8,0-2.7,0c-0.1,0-0.2-0.2-0.3-0.3c-0.2-0.7-0.3-1.4-0.4-2.1c-1.4,0.3-2.7,0.5-4.1,0.8c-0.7,0.1-1.4,0.3-2.1,0.4c-1.2,0.2-2.3-0.3-2.9-1.3c-0.6-1-0.6-2.2,0.2-3.2c2.1-2.7,4.2-5.3,6.3-7.9C7.4,0.5,8,0.2,8.6,0C8.9,0,9.3,0,9.6,0z"/>
							<path fill="#243E90" d="M18.4,17.6c4.2,0,8.4,0,12.6,0c0.7,0,1.3,0.2,1.7,0.8c0.4,0.7,0.3,1.5-0.2,2.1c-0.6,0.6-1.2,1.2-1.7,1.7c-3.2,3.2-6.5,6.5-9.7,9.7c-0.2,0.2-0.3,0.5-0.3,0.8c0,3.1,0,6.1,0,9.2c0,0.3,0.1,0.5,0.3,0.7c1.7,1.3,3.3,2.6,5,3.9c0.2,0.1,0.4,0.2,0.5,0.4c0.1,0.3,0.3,0.7,0.2,1c-0.1,0.3-0.4,0.4-0.7,0.6c-0.1,0.1-0.2,0-0.4,0c-4.8,0-9.5,0-14.3,0c-0.2,0-0.5,0-0.7-0.2c-0.6-0.4-0.6-1.1,0-1.6c1.6-1.3,3.3-2.6,5-3.9c0.4-0.3,0.6-0.7,0.6-1.2c0-2.9,0-5.7,0-8.6c0-0.5-0.2-0.9-0.5-1.2C11.8,28,8.1,24.3,4.4,20.6c-0.5-0.5-0.8-1.1-0.6-1.8c0.2-0.6,0.6-1,1.3-1.1c0.2,0,0.5,0,0.7,0C10,17.6,14.2,17.6,18.4,17.6z"/>
							<path fill="#AFBBBF" d="M32,24.9c0.8-0.8,1.6-1.6,2.4-2.4c0.8-0.8,1.3-1.9,1.3-3c0.1-2.6-1.9-4.6-4.5-4.7c-1.4,0-2.9,0-4.3,0c-0.2,0-0.3,0-0.5,0c0.5-2.9,4.9-5.2,8.3-4.4c4.2,0.9,6.8,5.1,5.6,9.3C39.2,23.9,34.9,25.6,32,24.9z"/>
							<path fill="#AFBBBF" d="M24,14.7c-1,0-1.9,0-2.9,0c0.1-0.7,0.2-1.3,0.3-2c0.4-2.2,0.8-4.4,1.2-6.6c0.4-2.1,2-3.2,4.1-2.8c1.2,0.3,2.4,0.5,3.6,0.7c0.7,0.2,1.2,0.7,1.2,1.4c0,0.6-0.4,1.2-1.1,1.4c-0.3,0.1-0.5,0-0.8,0c-1.2-0.2-2.3-0.5-3.5-0.7c-0.6-0.1-0.7-0.1-0.8,0.6c-0.5,2.5-0.9,5-1.4,7.6C24.1,14.4,24,14.6,24,14.7z"/>
								</svg>

						<div class="ps-3">
							<h6 class="fs-base text-primary mb-0 text-uppercase">VIVEZ UNE AVENTURE UNIQUE</h6>
							<p class="mb-0 fs-ms text-dark opacity-50 m-0 small">Profitez de vos vacances et faites le plein d’émotions!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center align-items-center">
			<div class="col-md-4 text-center mb-4">
				<img src="@asset('images/logo-y.png')" width="160px" class="img-fluid" alt="">
			</div>
			<div class="col-md-8 text-white mb-4">
				<p>What To Do est le spécialiste du tourisme alternatif en Tunisie, notamment le tourisme d’aventure. <br>
				Notre mission est de vous faire découvrir la Tunisie autrement, loin des sentiers battus et des hôtels de plage, pour vous faire vivre des expériences dont vous vous souviendrez toute votre vie.</p>
				<p>Partagez avec nous vos préférences et vos contraintes et bénéficiez de notre expertise pour faire le plein d’émotions intenses!</p>
			</div>
		</div>

	</div>
</div>


<div class="page-body">

		<div class="container p-0" id="request">
			<div class="page-content-card mb-5">
				<div class="container">

					<div class="row justify-content-between">

						<div class="col-lg-4 mb-5">
							<h5 class="mb-3 text-tertiary">Par téléphone ou email</h5>
							<div class="card shadow-none mb-3">
								<div class="card-body">
									<div>
										<h3 class="h6 mb-0"><i class="fas fa-phone me-3 text-tertiary"></i> <a class="nav-link-style" href="tel:+21622016016">+216 22 016 016</a></h3>
										<ul class="list-unstyled fs-sm mb-0">
											<li><span class="text-muted me-1">Appelez nous sur WhatsApp ou par téléphone</span></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="card shadow-none">
								<div class="card-body">
									<div>
										<h3 class="h6 mb-0"><i class="fas fa-envelope me-3 text-tertiary"></i> <a class="nav-link-style" href="mailto:booking@whattodotunisia.com">booking@whattodotunisia.com</a></h3>
										<ul class="list-unstyled fs-sm mb-0">
											<li><span class="text-muted me-1">Décrivez votre besoin par email</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-7 col-lg-offset-1">

							@include('forms.custom-activity-form')

						</div>


					</div>
				</div>

			</div>
		</div>
	</div>

	<x-activity-carousel type="featured" class="bg-light" title="les activités en vedette"/>


	<x-modals.form-success></x-modals.form-success>

	{{--	@includeFirst(['partials.content-page', 'partials.content'])--}}

@endsection
