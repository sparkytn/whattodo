{{--
  Template Name: Contact Page
  TODO Make the content translatable
--}}
@extends('layouts.app')

@section('content')

	@include('partials.page-header')
	<div class="page-body">

		<div class="container p-0">
			<div class="page-content-card mb-4">
				<div class="container">

					<div class="row">
						<div class="col-md-12 mb-4">
							 <h3>Besoin d'information ?</h3>
							<p class="lead">Contactez nous ou remplissez le formulaire et notre équipe essaiera de vous répondre dans le plus bref délai.</p>
						</div>
						<div class="col-lg-5">
							<div class="card shadow-none mb-3">
								<div class="card-body">
										<i class="fas fa-map-marker-alt me-3 text-tertiary fa-2x"></i>
										<div>
											<h3 class="h6 mb-0">Adresse</h3>
											<p class="mt-2 mb-2 text-muted">Imm SCI, Lac Toba, 1053 La Marsa, Tunisie</p>
										</div>
								</div>
							</div>
							<div class="card shadow-none mb-3">
								<div class="card-body">

									<i class="fas fa-phone me-3 text-tertiary fa-2x"></i>
									<div>
										<h3 class="h6 mb-0">Numéro de téléphone</h3>
										<ul class="list-unstyled fs-sm mb-0">
											<li><span class="text-muted me-1">Support :</span><a class="nav-link-style" href="tel:+21622016016">+216 22 016 016</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="card shadow-none mb-3">
								<div class="card-body">

									<i class="fas fa-envelope me-3 text-tertiary fa-2x"></i>
									<div>
										<h3 class="h6 mb-0">Emails</h3>
										<ul class="list-unstyled fs-sm mb-0">
											<li><span class="text-muted me-1">Booking :</span><a class="nav-link-style" href="mailto:booking@whattodotunisia.com">booking@whattodotunisia.com</a></li>
											<li class="mb-0"><span class="text-muted me-1">Support :</span><a class="nav-link-style" href="mailto:support@whattodotunisia.com">support@whattodotunisia.com</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-7 ps-lg-5">
							<h5 class="mb-4 text-tertiary">Formulaire de contact</h5>
							<form id="sparkyContactForm" class="needs-validation mb-3" novalidate action="#" method="post" data-url="{{ admin_url('admin-ajax.php') }}" data-nonce="{{ wp_create_nonce('sparky_save_user_contact_form') }}">
								<div class="row g-3">
									<div class="col-sm-6 has-error">
										<label class="form-label" for="client-name">Votre nom et prénom <span class="text-danger">*</span></label>
										<input class="form-control" type="text" id="client-name" name="full_name" placeholder="Votre nom et prénom" required="">
									</div>
									<div class="col-sm-6">
										<label class="form-label" for="client-email">Votre adresse email <span class="text-danger">*</span></label>
										<input class="form-control" type="email" id="client-email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" placeholder="Votre adresse email" required="">
									</div>
									<div class="col-sm-6">
										<label class="form-label" for="client-phone">Votre n° de téléphone <span class="text-danger">*</span></label>
										<input class="form-control" type="tel" id="client-phone" name="phone" pattern="[0-9]*" placeholder="Votre n° de téléphone" required role="presentation"  autocomplete="off">
									</div>
									<div class="col-sm-6">
										<label class="form-label" for="client-subject">Sujet <span class="text-danger">*</span></label>
										<input class="form-control" type="text" id="client-subject" name="subject" placeholder="Sujet du message" required="">
									</div>
									<div class="col-12">
										<label class="form-label" for="client-message">Message <span class="text-danger">*</span></label>
										<textarea class="form-control" id="client-message" name="message" rows="6" placeholder="Détail de votre demande" required=""></textarea>
									</div>
									<div class="col-12 d-none" js-feedback-form>
										<div class="alert alert-danger mb-0">
											<h6 class="text-white fs-sm mb-0">{{ __('Erreurs lors de la soumission du formulaire','wtd') }}</h6>
											<div class="detail small">{{ __("Merci d'actualisez la page et réessayez.",'wtd') }}</div>
										</div>
									</div>
									<div class="col-12">
										<button class="btn btn-primary" type="submit">
											<span class="spinner-border spinner-border-sm me-2 d-none" js-spinner role="status" aria-hidden="true"></span>
											Envoyer le message
										</button>
									</div>
								</div>
							</form>
						</div>



						</div>
					</div>

				</div>
			</div>
	</div>

	<x-activity-carousel type="featured" class="bg-light"  title="les activités en vedette"/>


	<x-modals.form-success></x-modals.form-success>

{{--	@includeFirst(['partials.content-page', 'partials.content'])--}}

@endsection
