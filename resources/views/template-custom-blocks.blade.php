{{--
  Template Name: Custom Blocks Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
	  @include('partials.page-header', ['pageTitle' => 'A Propos'])
	  <div class="page-body">
		  <div class="container">
			  <div class="row">
				@include('partials.content-page')
			  </div>
		  </div>
	  </div>
  @endwhile
@endsection
