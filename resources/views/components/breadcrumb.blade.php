@if (! empty($items))
	<nav aria-label="Breadcrumb" >
		<ol class="breadcrumb">
			@foreach ($items as $item)
				@if (empty($item['url']))
					<li class="breadcrumb-item active">
						  {{ $item['label'] }}
					</li>
				@else
				  <li class="breadcrumb-item">
					  <a title="Go to {!! $item['label'] !!}."
						  href="{{ $item['url'] }}"
					  >
							<span>
								@if ($loop->first)
									<i class="fas fa-home"></i>
									<span class="sr-only">{!! $item['label'] !!}</span>
								@else
									{!! $item['label'] !!}
								@endif
							</span>
					  </a>
				  </li>
				@endif
		@endforeach
	</nav>
@endif
