<article class="card product-card">
	<a href="/detail" class="stretched-link">
		<div class="card-activity-category">
			<span class="badge bg-dark text"><i class="fas fa-tree text-secondary"></i> Terre</span>
		</div>
		<div class="card-image object-fit">

			<img class="img-fluid" src="@asset('images/dummy/act-2.jpg')" alt="Rome, Italty">
		</div>
		<div class="card-body flex-grow-1">

				<div class="badge-wrapper">
					<span class="badge bg-danger">Promo -10%</span>
				</div>

				<div class="d-flex justify-content-between  mb-3">
					<h3 class="card-title text-dark mb-0">
						Balade à cheval à Sidi Thabet avec dîner
					</h3>
					<div class="card-rating small flex-shrink-0 ms-2">
						<span class="text-warning small me-1">
							<i class="feather-icon icon-star-s"></i>
						</span>
						<b>4.8</b>
					</div>
				</div>


				<ul class="list-unstyled card-list-info small mb-0">
					<li class="mb-1">
						<div class="d-flex align-items-center">
							<i class="feather-icon icon-user me-2"></i>
							<span class="small">Par personne</span>
						</div>
					</li>
					<li class="mb-1">
						<div class="d-flex align-items-center">
							<i class="feather-icon icon-clock me-2"></i>
							<span class="small">03 jours et 02 nuits</span>
						</div>
					</li>
					<li class="mb-1">
						<div class="d-flex align-items-center">
							<i class="feather-icon icon-map-pin me-2"></i>
							<span class="small">Sousse</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="card-footer bg-white">
				<div class="card-price-wrap">
					<p class="mb-0 text-primary">
						<span class="card-price-label">A Partir de : <b class="text-decoration-line-through text-dark"> 250 TND</b></span>
						<span class="card-price text-tertiary">200 TND</span>
						<small class="text-muted">Par Persone</small>
					</p>
				</div>

			</div>

	</a>
</article>
