<div class="modal fade" id="bookingModal" tabindex="-1" aria-labelledby="bookingModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body">
				<p class="text-center mt-3">
					<i class="fas fa-check-circle text-success fa-3x"></i>
				</p>
				<div class="text-center">
					<h4 class="mb-2">{{ __('Votre demande de confirmation a été envoyé !', 'wtd') }}</h4>
					<p class=" mt-0">{{ __('Nous reviendrons vers vous rapidement ! Vous recevrez un email sur cette adresse :','wtd') }} <br> <b>{{ wp_get_current_user()->user_email }}</b></p>
				</div>

				<p class="form-label mb-2">{{ __('Détail de la demande' , 'wtd') }}</p>
				<div class="card bg-primary shadow-none text-white">
					<div class="card-body">
						<h6 bm-title class="mb-2 text-secondary"></h6>
						<ul class="list-unstyled small ms-2">
							<li><i style="width: 15px;" class="text-secondary me-1 fas fa-check-circle"></i> {{ __('N° de réservation','wtd') }}  : <a href="" class="link-secondary" bm-url> <b bm-id></b> - <small>{{ __('Allez à la page.','wtd') }}</small></a></li>
							<li><i style="width: 15px;" class="text-secondary me-1 fas fa-calendar-check"></i> {{ __('Date de réservation','wtd') }} : <b bm-date></b></li>
							<li><i style="width: 15px;" class="text-secondary me-1 fas fa-users"></i> {{ __('Nombre de personne','wtd') }} : <b bm-pax></b></li>
						</ul>
						<p></p>
						<p></p>
					</div>
				</div>

			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-outline-primary btn-sm" onClick="window.location.replace(window.location.href)">{{ __('Fermer','wtd') }}</button>
				<a href="/" type="button" class="btn btn-secondary btn-sm">{{ __("Page d'accueil", 'wtd') }}</a>
			</div>
		</div>
	</div>
</div>
