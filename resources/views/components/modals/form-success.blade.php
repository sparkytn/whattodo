<div class="modal fade" id="formSubmittedModal" tabindex="-1"  aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body text-center">
				<p class="">
					<i class="fas fa-envelope-open-text text-success fa-4x"></i>
				</p>

				<h5>{{ __('Votre demande a été envoyée avec succès', 'wtd') }}</h5>

				<p>{{ __('Nous reviendrons vers vous rapidement !', 'wtd') }}</p>

			</div>
			<div class="modal-footer justify-content-center">
				<button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal">{{ __('Fermer','wtd') }}</button>
				<a href="/" type="button" class="btn btn-secondary">{{ __("Page d'accueil", 'wtd') }}</a>
			</div>
		</div>
	</div>
</div>
