<div class="flash flash-{{ $type }} collapse show" id="flash" role="alert">
  @if($full === true)
    <div class="container position-relative">
      @endif
      <div class="flash__inner position-relative">
        <div class="flash__header">
          <div class="flash__ico">
            <i class="fas fa-virus" aria-hidden=""></i>
          </div>
          <h4 class="flash__title">
            {{ $title }}
          </h4>
        </div>
        <div class="flash__body">
          {!! $message ?? $slot !!}
        </div>
      </div>
      <button class="flash__close" aria-label="close flash" type="button" data-bs-toggle="collapse" data-bs-target="#flash" aria-expanded="false" aria-controls="flash">
        <i class="fas fa-times"></i>
      </button>
  @if($full === true)
    </div>
  @endif
</div>
