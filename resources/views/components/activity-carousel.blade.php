<section class="section activity-section section-padding {{ $class }}">
	<div class="container">

		<div class="section-title section-title--decoration mb-4">
			<span>Découvrir</span>
			<h2>{{ $title }}</h2>
		</div>

		<!-- Slider main container -->
		<div class="activity__card-carousel swiper-container">
			<div class="home-activities-carousel swiper">
				<!-- Additional required wrapper -->
				<div class="swiper-wrapper">
					<!-- Slides -->

					@posts($activities)
					<!-- Slide -->
					<div class="swiper-slide product-slide pb-3">
						@php
							wc_get_template_part( 'content', 'product' );
						@endphp
					</div>
					@endposts

				</div>
				<!-- If we need pagination -->
				<div class="swiper-pagination"></div>

				<!-- If we need navigation buttons -->
			</div>
			<div class="swiper-button-prev swiper-nav-btn">
				<i class="fas fa-arrow-left"></i>
			</div>
			<div class="swiper-button-next swiper-nav-btn">
				<i class="fas fa-arrow-right"></i>
			</div>
		</div>


		<div class="text-center mt-3">
			<a href="{{ $btn['link'] }}" class="btn btn-primary">{{ $btn['title']  }}</a>
		</div>

	</div>
</section>
