<div class="flash flash-success" role="alert">
	<div class="container position-relative">
		<div class="flash__inner position-relative">
			<div class="flash__header">
				<div class="flash__ico">
					<i class="fas fa-virus" aria-hidden=""></i>
				</div>
				<h4 class="flash__title">
					{{ __('Youpi! Votre réservation est confirmé') }}

				</h4>
			</div>
			<div class="flash__body">

				@foreach($bookings as $booking)
					<div class="card shadow-none mb-4">
						<div class="card-body">

						<div class="row align-items-center">
							<div class="col-12 col-md">
								<div class="form-label text-muted"><?= __('Réservsation n°','wtd')?> <span class="text-tertiary">#{{$booking->get_id()}}</span></div>
								<h5 class="mt-0 mb-2"><a href="<?=$url?>"><?=$booking->title ?></a></h5>
								<div class="fs-xs text-muted"><?=__('Réservation placé le','wtd')?> <b><?=date_i18n( get_option( 'date_format' ), strtotime( $booking->post->post_date ) )?></b>.</div>
								<!-- End Row -->
							</div>
							<div class="col-auto mt-3 mt-md-0">
								<?php do_action( 'yith_wcbk_show_booking_actions', $booking, true ); ?>
							</div>

						</div>

						<hr>

						<div class="row">
							<div class="col-6 col-md mb-3 mb-md-0">
								<small class="text-cap mb-0"><?=$account_bookings_columns['booking-from']?></small>
								<small class="text-dark fw-bold"><?=$booking->get_formatted_date( 'from' ); ?></small>
							</div>
							<!-- End Col -->

							<div class="col-6 col-md mb-3 mb-md-0">
								<small class="text-cap mb-0"><?=$account_bookings_columns['booking-to']?></small>
								<small class="text-dark fw-bold"><?=$booking->get_formatted_date( 'to' ); ?></small>
							</div>
							<!-- End Col -->

							<div class="col-6 col-md mb-3 mb-md-0">
								<small class="text-cap mb-0"><?=__('ETAT','wtd')?></small>
								<small class="text-dark fw-bold">
									<span class="badge bg-<?=$booking->get_status()?>"><?=$booking->get_status_text()?></span>
								</small>
							</div>
							<!-- End Col -->
							<div class="col-6 col-md mb-3 mb-md-0">
								<small class="text-cap mb-0"><?=__('Total','wtd')?></small>
								<small class="text-dark fw-bold"><?=$booking->get_calculated_price()?> <?=get_woocommerce_currency()?></small>
							</div>
							<!-- End Col -->

							<div class="col-12 mt-2">

							</div>

						</div>
						<!-- End Row -->
					</div>
					</div>
				@endforeach
			</div>
		</div>

	</div>
</div>
