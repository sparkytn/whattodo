@extends('layouts.app')

@section('content')
	@include('partials.page-header')

	<div class="page-body">
		<div class="container p-0">
			<div class="page-content-card mb-5">
				<div class="container">
				  @while(have_posts()) @php(the_post())
					@includeFirst(['partials.content-page', 'partials.content'])
				  @endwhile
				</div>
			</div>
		</div>
	</div>
@endsection
