{{--
  Template Name: Custom Blocks Template
--}}

@extends('layouts.app')

@section('content')
	@while(have_posts()) @php(the_post())
		<div class="blocks bg-white">
				@include('partials.content-page')
		</div>

	@endwhile
@endsection
