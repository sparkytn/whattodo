@extends('layouts.app')

@section('content')
	<header class="page-header page-header--blog-post section-padding page-header--overlap" style="background-image: url(@thumbnail('full', false))">
		<div class="container content">
			<div class="row align-items-center justify-content-between">
				<div class="col-md-4 col-lg-5  text-center order-md-2 mb-4 mb-md-0">
					{!! get_the_post_thumbnail(null,'woocommerce_single', ['class'=> 'img-fluid post-featured-image', 'alt'=> strip_tags(get_the_title())]) !!}
				</div>
				<div class="col-md-7 col-lg-7 order-md-1 mt-3 mt-md-0">
					<x-breadcrumb></x-breadcrumb>
					<h1 class="title display-5 fw-bold mb-2">{!! get_the_title() !!}</h1>
				</div>


			</div>
		</div>
	</header>


	@while(have_posts()) @php(the_post())
	<div class="page-body container">

			<div class="page-content-card mb-4">
				<div class="row justify-content-center">
					<div class="col-lg-10 col-md-11">
						<div class="container">
							<div class="fs-xs fw-bold text-uppercase page-header__post-meta  mb-4">
								<i class="fas fa-tag text-secondary me-1"></i>

								<span class="opacity-75">
									Catégorie :
									{!! get_the_category_list( '-' ) !!}
								</span>

								<span class="px-2">•</span>

								<i class="fas fa-calendar-alt text-secondary me-1"></i>
								<span class="opacity-75">@published</span>
							</div>
							@includeFirst(['partials.content-single-' . get_post_type(), 'partials.content-single'])
					</div>
				</div>
			</div>
		</div>
	@endwhile


	<x-activity-carousel type="featured" class="bg-light"  title="les activités en vedette"/>

@endsection
