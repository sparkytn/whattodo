@extends('layouts.app')

@section('content')
	@while(have_posts()) @php(the_post())
	@include('partials.page-header', ['pageTitle' => 'Mes informations'])
	<div class="page-body">

		<div class="container p-0">
			<div class="row">
				<div class="col-md-4 col-lg-3">

					<div class="page-content-card mb-5 px-0 pt-4">
						<div class="container">
							<div class="text-center mb-4">
								<div class="avatar avatar-circle mb-2 d-none d-lg-block">
									<img class="avatar-img rounded-circle" width="80px" src="@asset('images/dummy/user-avatar.jpg')" alt="Image Description">
								</div>

								<h5 class="card-title mb-0">Natalie Curtis</h5>
								<p class="card-text small mt-0 opacity-50">natalie@example.com</p>
							</div>

							<span class="text-cap h6 text-uppercase mb-2">Mon Compte</span>

							<ul class="nav flex-column flex-wrap nav-menu-column mb-auto">
											<li class="nav-item">
												<a href="/orders" class="nav-link">
													<i class="fas fa-shopping-basket nav-icon"></i>
													Mes réservations
												</a>
											</li>
											<li class="nav-item">
												<a href="#" class="nav-link active">
													<i class="fas fa-user-edit nav-icon"></i>
													Mes informations
												</a>
											</li>
											<li class="nav-item">
												<a href="#" class="nav-link">
													<i class="fas fa-comment-dots nav-icon"></i>
													Mes Messages
												</a>
											</li>
											<li class="nav-item">
												<a href="#" class="nav-link">
													<i class="fas fa-sign-out-alt nav-icon"></i>
													Déconnexion
												</a>
											</li>
										</ul>
						</div>
					</div>

				</div>
				<div class="col-md-8 col-lg-9">
					<div class="page-content-card mb-5 px-0 pt-4">
						<div class="container">
							<h4 class="mb-3">Mes informations</h4>
							<form class="needs-validation" novalidate="" data-nordpass-autofill="identity" data-np-checked="1">
						<div class="row g-3">
							<div class="col-sm-6">
								<label for="firstName" class="form-label">First name</label>
								<input type="text" class="form-control" id="firstName" placeholder="" value="" required="" data-np-checked="1" data-nordpass-autofill="name_first" data-nordpass-uid="1tyud23z893" autocomplete="off">
								<div class="invalid-feedback">
									Valid first name is required.
								</div>
								<span data-nordpass-uid="1tyud23z893" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 368.5px; top: 381.828px;"></span></div>

							<div class="col-sm-6">
								<label for="lastName" class="form-label">Last name</label>
								<input type="text" class="form-control" id="lastName" placeholder="" value="" required="" data-np-checked="1" data-nordpass-autofill="name_last" data-nordpass-uid="0x6k43cwmrch" autocomplete="off">
								<div class="invalid-feedback">
									Valid last name is required.
								</div>
								<span data-nordpass-uid="0x6k43cwmrch" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 680.5px; top: 381.828px;"></span></div>

							<div class="col-12">
								<label for="username" class="form-label">Username</label>
								<div class="input-group has-validation">
									<span class="input-group-text">@</span>
									<input type="text" class="form-control" id="username" placeholder="Username" required="" data-np-checked="1">
									<div class="invalid-feedback">
										Your username is required.
									</div>
								</div>
							</div>

							<div class="col-12">
								<label for="email" class="form-label">Email <span class="text-muted">(Optional)</span></label>
								<input type="email" class="form-control" id="email" placeholder="you@example.com" data-np-checked="1" data-nordpass-autofill="identity_email" data-nordpass-uid="yo0sgt1b5s" autocomplete="off">
								<div class="invalid-feedback">
									Please enter a valid email address for shipping updates.
								</div>
								<span data-nordpass-uid="yo0sgt1b5s" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 680.5px; top: 553.828px;"></span></div>

							<div class="col-12">
								<label for="address" class="form-label">Address</label>
								<input type="text" class="form-control" id="address" placeholder="1234 Main St" required="" data-np-checked="1" data-nordpass-autofill="identity_address1" data-nordpass-uid="c0us8uraf3w" autocomplete="off">
								<div class="invalid-feedback">
									Please enter your shipping address.
								</div>
								<span data-nordpass-uid="c0us8uraf3w" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 680.5px; top: 639.828px;"></span></div>

							<div class="col-12">
								<label for="address2" class="form-label">Address 2 <span class="text-muted">(Optional)</span></label>
								<input type="text" class="form-control" id="address2" placeholder="Apartment or suite" data-np-checked="1" data-nordpass-autofill="identity_address2" data-nordpass-uid="6hv8rf2rhc" autocomplete="off">
							</div>

							<div class="col-md-5">
								<label for="country" class="form-label">Country</label>
								<select class="form-select" id="country" required="" data-nordpass-autofill="identity_country" data-nordpass-uid="pziviqwvjvn" autocomplete="off">
									<option value="">Choose...</option>
									<option>United States</option>
								</select>
								<div class="invalid-feedback">
									Please select a valid country.
								</div>
								<span data-nordpass-uid="pziviqwvjvn" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 282.5px; top: 811.828px;"></span></div>

							<div class="col-md-4">
								<label for="state" class="form-label">State</label>
								<select class="form-select" id="state" required="" data-nordpass-autofill="identity_state" data-nordpass-uid="9d1rt05v13u" autocomplete="off">
									<option value="">Choose...</option>
									<option>California</option>
								</select>
								<div class="invalid-feedback">
									Please provide a valid state.
								</div>
								<span data-nordpass-uid="9d1rt05v13u" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 490.5px; top: 811.828px;"></span></div>

							<div class="col-md-3">
								<label for="zip" class="form-label">Zip</label>
								<input type="text" class="form-control" id="zip" placeholder="" required="" data-np-checked="1" data-nordpass-autofill="identity_zip_code" data-nordpass-uid="slk32lo2wfb" autocomplete="off">
								<div class="invalid-feedback">
									Zip code required.
								</div>
								<span data-nordpass-uid="slk32lo2wfb" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 680.5px; top: 811.828px;"></span></div>
						</div>

						<hr class="my-4">

						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="same-address" data-np-checked="1">
							<label class="form-check-label" for="same-address">Shipping address is the same as my billing address</label>
						</div>

						<div class="form-check">
							<input type="checkbox" class="form-check-input" id="save-info" data-np-checked="1">
							<label class="form-check-label" for="save-info">Save this information for next time</label>
						</div>

						<hr class="my-4">

						<h4 class="mb-3">Payment</h4>

						<div class="my-3">
							<div class="form-check">
								<input id="credit" name="paymentMethod" type="radio" class="form-check-input" checked="" required="" data-np-checked="1">
								<label class="form-check-label" for="credit">Credit card</label>
							</div>
							<div class="form-check">
								<input id="debit" name="paymentMethod" type="radio" class="form-check-input" required="" data-np-checked="1">
								<label class="form-check-label" for="debit">Debit card</label>
							</div>
							<div class="form-check">
								<input id="paypal" name="paymentMethod" type="radio" class="form-check-input" required="" data-np-checked="1">
								<label class="form-check-label" for="paypal">PayPal</label>
							</div>
						</div>

						<div class="row gy-3">
							<div class="col-md-6">
								<label for="cc-name" class="form-label">Name on card</label>
								<input type="text" class="form-control" id="cc-name" placeholder="" required="" data-np-checked="1">
								<small class="text-muted">Full name as displayed on card</small>
								<div class="invalid-feedback">
									Name on card is required
								</div>
							</div>

							<div class="col-md-6">
								<label for="cc-number" class="form-label">Credit card number</label>
								<input type="text" class="form-control" id="cc-number" placeholder="" required="" data-np-checked="1" data-nordpass-autofill="cc_number" data-nordpass-uid="0ybxlpxt7jw" autocomplete="off">
								<div class="invalid-feedback">
									Credit card number is required
								</div>
								<span data-nordpass-uid="0ybxlpxt7jw" style="width: 24px; min-width: 24px; height: 24px; background-image: url(&quot;chrome-extension://fooolghllnmhmmndgjiamiiodkpenpbb/icons/icon.svg&quot;); background-repeat: no-repeat; background-position: left center; background-size: auto; border: none; display: inline; visibility: visible; position: absolute; cursor: pointer; z-index: 1001; padding: 0px; transition: none 0s ease 0s; pointer-events: all; left: 680.5px; top: 1166.53px;"></span></div>

							<div class="col-md-3">
								<label for="cc-expiration" class="form-label">Expiration</label>
								<input type="text" class="form-control" id="cc-expiration" placeholder="" required="" data-np-checked="1">
								<div class="invalid-feedback">
									Expiration date required
								</div>
							</div>

							<div class="col-md-3">
								<label for="cc-cvv" class="form-label">CVV</label>
								<input type="text" class="form-control" id="cc-cvv" placeholder="" required="" data-np-checked="1">
								<div class="invalid-feedback">
									Security code required
								</div>
							</div>
						</div>

						<hr class="my-4">

						<button class="w-100 btn btn-primary" type="submit">Continue to checkout</button>
					</form>
						</div>
					</div>
				</div>
			</div>

		</div>


		<section class="section section-padding bg-light">
			<div class="container">
				<div class="section-title section-title--decoration mb-5">
					<span>Découvrir</span>
					<h2>D'autres activités</h2>
				</div>
				<div class="position-relative">
					<div class="detail-activities-carousel swiper">
						<!-- Additional required wrapper -->
						<div class="swiper-wrapper">
							<!-- Slides -->
							@for ($i = 0; $i < 9; $i++)
								<div class="swiper-slide pb-4">
									<x-activity-card></x-activity-card>
								</div>
							@endfor
						</div>
						<!-- If we need pagination -->
						<div class="swiper-pagination"></div>

						<!-- If we need navigation buttons -->
					</div>
					<div class="swiper-button-prev swiper-nav-btn">
						<i class="fas fa-arrow-left"></i>
					</div>
					<div class="swiper-button-next swiper-nav-btn">
						<i class="fas fa-arrow-right"></i>
					</div>

				</div>
			</div>

		</section>
	</div>

	{{--	@includeFirst(['partials.content-page', 'partials.content'])--}}
	@endwhile
@endsection
