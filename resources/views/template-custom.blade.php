{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
	  @include('partials.page-header', ['pageTitle' => 'A Propos'])
	  <div class="page-body">
		  <div class="container p-0">
			  <div class="page-content-card mb-5">
				  <div class="container">
					@include('partials.content-page')
				  </div>
			  </div>
		  </div>
	  </div>
  @endwhile
@endsection
