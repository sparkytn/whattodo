<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>
<div class="row mb-5">
	<div class="col-12 mb-2">
		<h3>{{ __('Détail du panier', 'wtd') }} :</h3>
	</div>
	<div class="col-lg-8 mb-4">
		<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<?php do_action( 'woocommerce_before_cart_table' ); ?>

			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
			?>
			<div class="woocommerce-cart-form__cart-item rounded p-3 border mb-4 <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<div class="row">
					<div class="col-md-2 col-lg-3 mb-md-0 mb-4">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $product_permalink ) {
							echo $thumbnail; // PHPCS: XSS ok.
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						}
						?>
					</div>
					<div class="col-md-10 col-lg-9" >

						<div class="d-flex justify-content-between align-items-center w-100 mb-3">
							<h5 class="mb-0 product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
								<?php
								if ( ! $product_permalink ) {
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
								} else {
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
								}
								?>
							</h5>
							<?php
								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="remove btn btn-danger px-2 btn-sm fs-xs" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="fas fa-trash"></span> <span class="d-none d-md-inline"> %s</span></a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() ),
										esc_html__( 'Remove', 'woocommerce' )
									),
									$cart_item_key
								);
							?>
						</div>
						<?php

								do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

								// Meta data.
								echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

								// Backorder notification.
								if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
									echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
								}
								?>

						<div class="d-flex justify-content-between align-items-center" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
							<div class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
								<span class="h5 fs-5 text-muted text-uppercase"><?php esc_attr_e( 'Price', 'woocommerce' ); ?></span>
							</div>
							<div class="h4 text-success">
								<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
								?>
							</div>
								{{--									<div class="col">--}}
								{{--										<span class="product-quantity form-label d-block"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></span>--}}
								{{--										<?php--}}
								{{--										if ( $_product->is_sold_individually() ) {--}}
								{{--											$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );--}}
								{{--										} else {--}}
								{{--											$product_quantity = woocommerce_quantity_input(--}}
								{{--												array(--}}
								{{--													'input_name'   => "cart[{$cart_item_key}][qty]",--}}
								{{--													'input_value'  => $cart_item['quantity'],--}}
								{{--													'max_value'    => $_product->get_max_purchase_quantity(),--}}
								{{--													'min_value'    => '0',--}}
								{{--													'product_name' => $_product->get_name(),--}}
								{{--												),--}}
								{{--												$_product,--}}
								{{--												false--}}
								{{--											);--}}
								{{--										}--}}

								{{--										echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.--}}
								{{--										?>--}}
								{{--									</div>--}}
						</div>

					</div>
				</div>
			</div>
			<?php
				}
			}
			?>

				<?php do_action( 'woocommerce_cart_contents' ); ?>

				<div class="actions rounded p-3 border mb-4">
					<h5>{{ __('Vous avez un code promo ?') }}</h5>
					<?php if ( wc_coupons_enabled() ) { ?>
					<div class="coupon">
						<label for="coupon_code" class="form-label"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label>
						<div class="input-group mb-3">
							<input type="text" name="coupon_code" class="input-text form-control" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" />
							<button type="submit" class="btn btn-primary btn-sm" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Valider', 'wtd' ); ?></button>
						</div>
						<?php do_action( 'woocommerce_cart_coupon' ); ?>
					</div>
					<?php } ?>

{{--					<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>--}}

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</div>


				<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
		</form>
	</div>
	<div class="col-lg-4">
		<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>
		<div class="cart-collaterals">
			<?php
			/**
			 * Cart collaterals hook.
			 *
			 * @hooked woocommerce_cross_sell_display
			 * @hooked woocommerce_cart_totals - 10
			 */
			do_action( 'woocommerce_cart_collaterals' );
			?>
		</div>
		<?php do_action( 'woocommerce_after_cart' ); ?>
	</div>
</div>






