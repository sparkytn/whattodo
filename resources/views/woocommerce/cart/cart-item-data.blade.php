<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-item-data.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

{{--<div class="">--}}
	<dl class="row border-bottom mb-3 mx-0 card-item-detail">
		<?php foreach ( $item_data as $data ) : ?>
			@if( $data['key'] !== 'Services avec la réservation')
			<div class="col-6 col-sm mb-2 ps-0  card-item-detail__item">
				<dt class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?> form-label d-block mb-0 opacity-50"><?php echo wp_kses_post( $data['key'] ); ?>:</dt>
				<dd class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?> text-muted small"><?php echo wp_kses_post( $data['display'] ); ?></dd>
			</div>
{{--			@else--}}
{{--				<div class="col-6 col-sm-6 mb-2 ps-0  card-item-detail__item">--}}
{{--					<dt class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?> form-label d-block mb-0 opacity-50"><?php echo wp_kses_post( $data['key'] ); ?>:</dt>--}}
{{--					<dd class="<?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?> text-muted"><?php echo wp_kses_post( $data['display'] ); ?></dd>--}}
{{--				</div>--}}
			@endif
		<?php endforeach; ?>
	</dl>
{{--</div>--}}
