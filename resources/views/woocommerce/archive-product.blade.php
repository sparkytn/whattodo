{{--
The Template for displaying product archives, including the main shop page which is a post type archive

This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see https://docs.woocommerce.com/document/template-structure/
@package WooCommerce/Templates
@version 3.4.0
--}}

@extends('layouts.app')

@section('content')
	@php
		do_action('get_header', 'shop');
		do_action('woocommerce_before_main_content');
	@endphp
  @include('partials.page-header')

	<div class="page-body mb-6">
		<div class="container">
			<div class="page-content-card">
				<div class="container">
					<div class="row">
						<aside class="col-xl-3 col-lg-4 order-lg-2 mb-3">
							<a class="btn btn-secondary btn-sm d-lg-none mb-3 mt-2" data-bs-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter">
								<i class="fas fa-filter"></i> {{ __('Filtrer les activités ') }}
							</a>
							<div class="collapse d-lg-block" id="collapseFilter">
								@php
									dynamic_sidebar('sidebar-shop')
								@endphp
							</div>
						</aside>
						<div class="col-xl-9 col-lg-8 order-lg-1">
							@if (woocommerce_product_loop())
							<div class="d-flex justify-content-between flex-wrap align-items-center">
								@php
								do_action('woocommerce_before_shop_loop');
								@endphp
							</div>
							  @php
								  woocommerce_product_loop_start();
							  @endphp

							  @if (wc_get_loop_prop('total'))
								  @while (have_posts())
									  @php
										  the_post();
										  do_action('woocommerce_shop_loop');
										  wc_get_template_part('content', 'product');
									  @endphp
								  @endwhile
							  @endif

							  @php
								  woocommerce_product_loop_end();
								  do_action('woocommerce_after_shop_loop');
							  @endphp
							@else
							  @php
								  do_action('woocommerce_no_products_found')
							  @endphp
							@endif
						</div>

					</div>
	  </div>

  </section>


  @php
    do_action('woocommerce_after_main_content');
    do_action('get_sidebar', 'shop');
    do_action('get_footer', 'shop');
  @endphp
@endsection
