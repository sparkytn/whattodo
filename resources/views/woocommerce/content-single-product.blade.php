<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$duration_label      = get_field('duration');
$minPerson = $product->get_minimum_number_of_people();


/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

?>

<div id="product-<?php the_ID(); ?>" >
	<div class="activity-gallery-header position-relative">
		<?php

		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
		do_action('sparky_show_product_detail_badge', get_field( 'product_badge' ), 'fs-5')
		?>

	</div>

	<div class="row">
			<div class="col-lg-8 mb-5 pe-lg-5 summary entry-summary">


				<div class="row product-info-header rounded border mx-0">
					<div class="product-info-header__item col-lg-3 col-sm-6 col-6 mb-4 mb-lg-0">
						<i class="fas fa-money-bill-wave product-info-header__item__icon"></i>
						<div class="product-info-header__item__text">
							<span class="label">{{ __('Prix', 'wtd') }} :</span>
							<b class="value">{!! $product->get_base_price() !!} {{ get_woocommerce_currency_symbol() }} </b>
						</div>

					</div>
					@if(get_field('destination'))
						<div class="product-info-header__item col-lg-3 col-sm-6 col-6 mb-4 mb-lg-0">
							<i class="fas fa-map-marker-alt product-info-header__item__icon"></i>
							<div class="product-info-header__item__text">
								<span class="label">{{ __('Lieu', 'wtd') }} :</span>
								<b class="value">{{ get_field('destination') }}</b>
							</div>
						</div>
					@endif

					<div class="product-info-header__item col-lg-3 col-sm-6 col-6">
						<i class="fas fa-{{ $minPerson > 1 ? 'users' : 'user' }} product-info-header__item__icon"></i>
						<div class="product-info-header__item__text">
							<span class="label">{{ __('Activité', 'wtd') }} :</span>
							<b class="value">{{ $minPerson > 1 ? __('Par groupe', 'wtd') : __('Par Personne', 'wtd') }}</b>
						</div>
					</div>

					<div class="product-info-header__item col-lg-3 col-sm-6 col-6">
						<i class="fas fa-hourglass product-info-header__item__icon"></i>
						<div class="product-info-header__item__text">
							<span class="label">{{ __('Durée', 'wtd') }} :</span>
							<b class="value">{{ $duration_label }}</b>
						</div>
					</div>

				</div>

				<?php
					do_action('sparky_show_product_detail_badge_detail', get_field( 'product_badge' ), 'fs-5')
				?>

				@if($product->last_minute_discount)
					<div class="alert alert-tertiary mb-4"><div class="d-flex align-items-center">
							<div class="me-3">
								<svg xmlns="http://www.w3.org/2000/svg" class="text-white" viewBox="0 0 576 512" width="40px">
									<path d="M543.8 256c0-36.5-18.86-68.38-46.86-86.75c6.875-32.88-2.517-68.63-28.14-94.25c-25.62-25.75-61.61-35.13-94.36-28.25C355.1 18.75 324.1 0 287.8 0S219.5 18.75 200.1 46.75C168.1 39.88 132.5 49.38 106.8 75C81.09 100.6 71.61 136.5 78.48 169.3C50.36 187.8 31.84 219.8 31.84 256s18.64 68.25 46.64 86.75C71.61 375.6 81.21 411.4 106.8 437c25.62 25.75 61.14 35.13 94.14 28.25C219.5 493.4 251.6 512 287.8 512c36.38 0 68.14-18.75 86.64-46.75c33 6.875 68.73-2.625 94.36-28.25c25.75-25.62 35.02-61.5 28.14-94.25C525.1 324.3 543.8 292.3 543.8 256zM223.8 160c17.62 0 31.1 14.38 31.1 32S241.4 224 223.8 224s-32-14.38-32-32S206.2 160 223.8 160zM240.1 336.1C236.3 341.7 230.2 344 224 344s-12.28-2.344-16.97-7.031c-9.375-9.375-9.375-24.56 0-33.94l128-128c9.375-9.375 24.56-9.375 33.94 0s9.375 24.56 0 33.94L240.1 336.1zM351.8 352c-17.62 0-32-14.38-32-32s14.38-32 32-32s32 14.38 32 32S369.4 352 351.8 352z" fill="currentColor"/>
								</svg>
							</div>
							<div>
								<h6 class="mb-1 text-white">Remise de dérniére minute</h6>
								<p class="m-0 small">Profitez d'une remise de <b class="h6 text-white">{{ $product->last_minute_discount }}%</b> pour toute réservation <b>avant le {{ date("Y-m-d", strtotime(wp_date('d-m-Y')."+ ". $product->last_minute_discount ." days")) }}</b> </p>
							</div>
					</div></div>
				@endif


				<?php
				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				//do_action( 'woocommerce_single_product_summary' );
				?>
				<p>{!! $product->get_short_description() !!}</p>

				<div class="rounded mb-4 fw-bold p-3 bg-light">
					<div class="d-flex align-items-start">
						<div class="me-2">
							<i class="fas fa-question-circle fs-5 text-info"></i>
						</div>
						<div>
							<p class="m-0 small">{{ __('Besoin de plus de détails ? Contactez nous au') }} <a href="tel:+21622016016"><small>+216</small> 22 016 016</a> ou visiter la <a href="/contact">page contact</a> </p>
						</div>
					</div>
				</div>





{{--				@foreach($product->price_rules as $price_rule)--}}
{{--					@if($price_rule->is_enabled())--}}
{{--						@php--}}
{{--							$conditions        = $price_rule->get_conditions();--}}
{{--						@endphp--}}
{{--					@endif--}}
{{--				@endforeach--}}

				<?php

				if( have_rows('nb-activite') ): ?>
					<h2 class="h3">{{ __('A propos de cette activité','wtd') }}</h2>

					<ul class="list-unstyled text-muted">
						<?php while  ( have_rows('nb-activite') ) : the_row(); ?>
						<li class="mb-2 d-flex align-items-baseline {{ the_sub_field('nb-activite-couleur') }}">
							<i class="fa-solid {{ the_sub_field('nb-activite-icon') }} me-3 text-success" aria-hidden="true"></i>
							<?php the_sub_field('item-activi'); ?>
						</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>


				<h2 class="h3">{{ __('Description','wtd') }}</h2>
				<div class="more">
					{!! $product->get_description() !!}
				</div>


				<h2 class="h3">{{ __("Avant l'activité",'wtd') }}</h2>

				<div class="row">
				@if(get_field('destination'))
					<div class="col-md-4">
						<h3 class="h6 text-muted">{{ __("Destination",'wtd') }}</h3>
					</div>
					<div class="col-md-8">
						<p><?php echo get_field('destination');?></p>

					</div>
					<div class="col-12 py-4"> <hr> </div>
				@endif

{{--				@if( get_field('departurereturn_location') || get_field('departurereturn_location-c') )--}}
{{--					<div class="col-md-4">--}}
{{--						<h3 class="h6 text-muted">--}}
{{--							{{ __("Point de rencontre",'wtd') }}--}}
{{--						</h3>--}}
{{--					</div>--}}
{{--					<div class="col-md-8">--}}
{{--						<?php if(get_field('departurereturn_location-c')): echo '<p>'.get_field('departurereturn_location-c').'</p>'; endif;?>--}}

{{--						<a href="<?php echo get_field('departurereturn_location');?>" class="btn btn-primary btn-sm"  target="_blank">--}}
{{--							<i class="fas fa-route me-2"></i> {{ __("Ouvrir dans la map",'wtd') }}--}}
{{--						</a>--}}
{{--					</div>--}}
{{--					<div class="col-12 py-4"> <hr> </div>--}}
{{--				@endif--}}

				<?php if( have_rows('nb-expr') ): ?>

					<div class="col-md-4">
						<h3 class="h6 text-muted collapse-mobile__ctrl collapsed" data-bs-toggle="collapse" href="#collapseExperience"  aria-expanded="true" aria-controls="collapseExperience">
							{{ __("Votre expérience",'wtd') }}
						</h3>
					</div>
					<div class="col-md-8">
						<div id="collapseExperience" class="d-md-block collapse-mobile__content collapse">
							<ul>
							<?php
								while ( have_rows('nb-expr') ) : the_row();
							?>
								<li><?php the_sub_field('item-expr'); ?></li>
							<?php
								endwhile;
							?>
							</ul>
						</div>

					</div>
					<div class="col-12 py-4"> <hr> </div>
				<?php endif;?>

				<?php
					if( have_rows('nb-bnsv') ) :
				?>
					<div class="col-md-4">
						<h3 class="h6 text-muted collapse-mobile__ctrl collapsed" data-bs-toggle="collapse" href="#collapseInfoImport"  aria-expanded="true" aria-controls="collapseInfoImport">
							{{ __("Informations importantes",'wtd') }}
						</h3>
					</div>
					<div class="col-md-8">
						<div id="collapseInfoImport" class="d-md-block collapse-mobile__content collapse">
							<ul>
								<?php
								while ( have_rows('nb-bnsv') ) : the_row();?>
								<li><?php the_sub_field('item-bnsv'); ?></li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
				<?php
					endif;
				?>


				</div>


{{--				<section class="section provider-section bg-light p-3 my-3 mb-4 mt-5 rounded">--}}
{{--					<div class="form-label">--}}
{{--						@php--}}
{{--							$vendor = yith_get_vendor( $product, 'product' );--}}
{{--						@endphp--}}
{{--						<div class="d-flex flex-row align-items-center">--}}
{{--							<span class="provider-section__label me-4">{{ __("Activité proposer par ",'wtd') }} : </span>--}}
{{--							<div class="overflow-hidden rounded-circle me-3 provider-section__logo">--}}
{{--								{!! wp_get_attachment_image( $vendor->avatar, 'thumbnail', true, ['class'=>'img-fluid'] ) !!}--}}
{{--							</div>--}}
{{--							<a href="{{ $vendor->get_url() }}" class="provider-section__name">{{ $vendor->term->name }}</a>--}}
{{--						</div>--}}

{{--					</div>--}}
{{--				</section>--}}

				<section class="section share-section bg-light p-3 my-3 mb-3 mt-5 rounded">
					<label class="form-label d-block d-sm-inline-block me-4">{{ __('Partager cet activité') }} :</label>

					<a btn-social-share class="btn btn-sm btn-share btn-facebook me-2 my-2" href="#" target="_blank" id="fb-share"><i class="fab fa-facebook-f"></i>  <span class="d-none d-md-inline">Facebook</span></a>
					<a btn-social-share class="btn btn-sm btn-share btn-twitter me-2 my-2" href="#" target="_blank" id="tw-share"><i class="fab fa-twitter"></i> <span class="d-none d-md-inline">Twitter</span></a>
					<a btn-social-share class="btn btn-sm btn-share btn-linkedin my-2" href="#" target="_blank" id="in-share"><i class="fab fa-linkedin"></i>  <span class="d-none d-md-inline">Linked In</span></a>

				</section>



				<section class="section review-section mt-5" id="review">
					@php
						comments_template();
					@endphp
				</section>


			</div>
			<div class="col-lg-4 ps-lg-0 mb-5 sidebar">

				<div id="booking-card" class="sticky">
					<div class="card booking-card border-0 shadow">
						@php
							$args = array(
								'product'   => $product,
							);
							wc_get_template( 'single-product/add-to-cart/booking-form/widget-booking-form.php', $args );
						@endphp
					</div>
				</div>
			</div>
		</div>
</div>

<x-modals.booking-submitted></x-modals.booking-submitted>

<?php do_action( 'woocommerce_after_single_product' ); ?>
