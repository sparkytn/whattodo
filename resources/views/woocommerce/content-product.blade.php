<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

// get number of columns to show per row
$cols = 12 / esc_attr( wc_get_loop_prop( 'columns' ) );

$duration_label      = get_field('duration');
$minPerson = $product->get_minimum_number_of_people();
$location = $product->get_location() ?? 'NUll';
$category = get_the_category_by_ID($product->get_category_ids()[0]);
$rating_avrg = $product->get_average_rating();
$currencySymbol = get_woocommerce_currency_symbol();
$price = $product->get_base_price() ? $product->get_base_price() : 0;
$discount = '';
if($product->get_last_minute_discount()){
	$discount = $price.' '.$currencySymbol;
}
$finalPrice = $price - ( $price * $product->get_last_minute_discount() / 100 );

?>
<div <?php wc_product_class( 'h-100 flex-grow-1 product-card-col g-col-12 g-col-md-6 g-col-xl-'. $cols , $product ); ?>>
	<article class="card product-card h-100">
		<?php
		/**
		 * Hook: woocommerce_before_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */

		do_action( 'woocommerce_before_shop_loop_item' );
		?>
		<div class="product-img-wrapper position-relative">
			<div class="card-activity-category">
				<span class="badge bg-dark text badge-cat">
					<i class="fa-{{ sanitize_title($category) }} icon-activity text-secondary"></i> {{ $category }}
				</span>
			</div>
			@php
				$label = get_field( 'label' );

				if($label){
					$label_image = get_the_post_thumbnail_url( $label, 'full' );
					if( $label_image ){
			@endphp
				<div class="product-label__wrapper">
					<img src="<?php echo esc_url( $label_image ); ?>" class="product-label" alt="Label du produit" />
				</div>

			@php
					}
				}
			@endphp



			<div class="card-image object-fit">
				<?php
					/**
					 * Hook: woocommerce_before_shop_loop_item_title.
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */

					do_action( 'woocommerce_before_shop_loop_item_title' );
				?>
			</div>

		</div>
		<?php
		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		?>

		<div class="p-3 flex-grow-1 position-relative">

			@php( do_action('sparky_show_product_card_badge', get_field( 'product_badge', '' )) )

			<h3 class="card-title mb-3">
				{!! get_the_title() !!}
			</h3>


			<ul class="list-unstyled card-list-info small mb-0">
				<li class="mb-1">
					<div class="d-flex align-items-center">
						<i class="fas fa-user me-2"></i>
						<span class="small">{{ __('Minimum', 'wtd') }}  <b>{{ $minPerson }} {{ __('pax.', 'wtd') }}</b></span>
					</div>
				</li>
				<li class="mb-1">
					<div class="d-flex align-items-center">
						<i class="fas fa-clock me-2"></i>
						<span class="small">{{ __('Durée', 'wtd') }} :  <b>{{ $duration_label }}</b></span>
					</div>
				</li>
				@if($location)
					<li class="mb-1">
						<div class="d-flex align-items-center">
							<i class="fas fa-map-marker-alt me-2"></i>
							<span class="small">{{ $location  }}</span>
						</div>
					</li>
				@endif
			</ul>
		</div>

		<div class="card-footer bg-white mt-auto">
			<div class="card-price-wrap d-flex justify-content-between align-items-center">
				<p class="mb-0 text-primary">
					<span class="card-price-label">{{ __('A Partir de','wtd') }} : <b class="text-decoration-line-through text-dark"> {{ $discount  }}</b></span>
					<span class="card-price text-tertiary">{{$finalPrice}} {{ $currencySymbol }}</span>
				</p>
				@if($rating_avrg != 0)
					<div class="card-rating small ms-2 text-end">
						<label class="card-price-label mb-0">{{ __('Note','wtd') }}:</label>
						<span class="text-warning small">
							<i class="fas fa-star"></i>
						</span>
						<b>{{ $rating_avrg }}</b>
					</div>
				@endif
			</div>
	<?php
		/**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item_title' );
		/**
		 * Hook: woocommerce_after_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_close - 5
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		echo '</div>';
		do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</article>
</div>
