<?php
/**
 * Checkout Order Receipt Template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/order-receipt.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="row mb-4 justify-content-center mt-md-3">
	<div class="col-md-6 col-lg-5">
		<div class="card shadow-none">
			<div class="card-header bg-primary">
				<h5 class="m-0 text-white">{{$order->get_billing_first_name() }} {{ $order->get_billing_last_name() }}</h5>
				<small class="text-white opacity-75">{{ $order->get_billing_email() }}</small>
			</div>

			<ul class="order_details list-group list-group-flush mb-4">
				<li class="order list-group-item py-3">
					@foreach ( $order->get_items() as $item_id => $item )
						<h5 class="mb-2"> {{ $item->get_name() }} </h5>
						@php
							$details = $item->get_meta_data();
						@endphp
						<div class="row text-muted">
							<div class="col-sm mb-2 mb-sm-0">
								<span class="d-block text-uppercase  fs-xs">{{ __('Nbr Personne','wtd') }} :</span>
								<b class="small">{{ $details[0]->value['persons']}}</b>
							</div>
							<div class="col-sm mb-2 mb-sm-0">
								<span class="d-block text-uppercase  fs-xs">{{ __('Date', 'wtd') }} :</span>
								<b class="small">{{ wp_date('d M Y', $details[0]->value['from']) }}</b>
							</div>
							<div class="col-sm">
								<span class="d-block text-uppercase  fs-xs">{{ __('Réservation', 'wtd') }} :</span>
								<b class="small">#{{ $details[0]->value['_booking_id'] }}</b>
							</div>
						</div>

					@endforeach

				</li>
				<li class="order list-group-item py-3 align-items-center d-flex justify-content-between">
					<label class="form-label mb-0 text-muted"> <?php esc_html_e( 'Order number:', 'woocommerce' ); ?></label>
					<strong class="h5 mb-0 fs-base fw-normal ">#<?php echo esc_html( $order->get_order_number() ); ?></strong>
				</li>
				<li class="date list-group-item py-3 align-items-center d-flex justify-content-between">
					<label class="form-label mb-0 text-muted"> <?php esc_html_e( 'Order date:', 'woocommerce' ); ?></label>
					<strong class="h5 mb-0 fs-base fw-normal "><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></strong>
				</li>
				<?php if ( $order->get_payment_method_title() ) : ?>
				<li class="method list-group-item py-3 align-items-center d-flex justify-content-between">
					<label class="form-label mb-0 text-muted"> <?php esc_html_e( 'Payment method:', 'woocommerce' ); ?></label>
					@if($order->get_payment_method() == 'wc_gateway_paymee')
						<img src="@asset('images/logo_paymee.png')" alt="Paymee" width="90px">
					@else
						<strong class="h5 mb-0 fs-base fw-normal "><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					@endif
				</li>
				<?php endif; ?>
				<li class="total list-group-item align-items-center d-flex justify-content-between fw-bold pt-4">
					<label class="form-label mb-0 text-muted"> <?php esc_html_e( 'Total:', 'woocommerce' ); ?></label>
					<strong class="h5 mb-0 text-success "><?php echo wp_kses_post( $order->get_formatted_order_total() ); ?></strong>
				</li>
			</ul>

			<div class="card-body pt-0">
				<div class="form">
					<?php do_action( 'woocommerce_receipt_' . $order->get_payment_method(), $order->get_id() ); ?>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="clear"></div>
