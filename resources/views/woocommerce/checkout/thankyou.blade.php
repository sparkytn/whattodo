<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>
			<div class="row justify-content-center text-center">
				<div class="col-md-8">
					<div class="mb-5">
						<i class="fa-solid fa-triangle-exclamation fa-3x text-danger"></i>
						<h2 class="text-danger">
							{{ __('Échec de paiement !', 'wtd') }}
						</h2>

						<p class="lead woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

						<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">

							<?php if ( is_user_logged_in() ) : ?>
							<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="btn btn-primary me-2 pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
							<?php endif; ?>
							<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="btn btn-success pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
						</p>
					</div>
				</div>
			</div>



		<?php else : ?>

			<div class="text-center mb-4">
				<i class="fa-solid fa-check-circle fa-3x text-success"></i>
				<h2 class="text-success woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h2>
			</div>


			<div class="card card-body shadow-none mb-5">
				<div class="container">
					<div class="woocommerce-order-overview woocommerce-thankyou-order-details order_details row">

				<div class="col-lg col-md-4 mb-3 woocommerce-order-overview__order order">
					<div class="form-label"><?php esc_html_e( 'Order number:', 'woocommerce' ); ?></div>
					<?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</div>

				<div class="col-lg col-md-4 mb-3 woocommerce-order-overview__date date">
					<div class="form-label">
						<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					</div>
					<?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</div>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<div class="col-lg col-md-4 mb-3 woocommerce-order-overview__email email">
						<div class="form-label"><?php esc_html_e( 'Email:', 'woocommerce' ); ?></div>
						<?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</div>
				<?php endif; ?>

				<div class="col-lg col-md-4 mb-3 woocommerce-order-overview__total total">
					<div class="form-label"><?php esc_html_e( 'Total:', 'woocommerce' ); ?></div>
					<?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</div>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<div class="col-lg col-md-4 mb-3 woocommerce-order-overview__payment-method method">
						<div class="form-label"><?php esc_html_e( 'Payment method:', 'woocommerce' ); ?></div>
						<?php echo wp_kses_post( $order->get_payment_method_title() ); ?>
					</div>
				<?php endif; ?>

			</div>
				</div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<div class="text-center mb-4">
			<i class="fa-solid fa-check-circle fa-3x text-success"></i>
			<h2 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h2>
		</div>



	<?php endif; ?>

</div>
