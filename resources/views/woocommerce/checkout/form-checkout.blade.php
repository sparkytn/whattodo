<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
<div class="row">
	<div class="col-md-7 col-lg-8 mb-4 mb-md-0">
		<div class="" id="customer_details">
			<div class="">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>
	</div>


	<div class="col-md-5 col-lg-4 side mb-4">
		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		<?php endif; ?>

		<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

		<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

		<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

		<div id="order_review" class="woocommerce-checkout-review-order">
			<?php do_action( 'woocommerce_checkout_order_review' ); ?>
		</div>

		<div class="rounded mb-4 fw-bold p-3 border mt-4 text-center">

			<i class="fas fa-question-circle fs-5 text-info"></i>
			<p class="m-0 small">Besoin d'aide ? <br> Contactez nous au <a href="tel:+21622016016"><small>+216</small> 22 016 016</a> </p>

		</div>


	</div>
	<div class="col-md-7 col-lg-8">
		<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
	</div>
</div>


</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
