<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

?>
<div class="form-label m-0 fw-bold">
	{{ __('A PARTIR DE', 'wtd') }} :
</div>
<div class="m-0 con">

	<div class="price-head lh-1">
		<?php
		$basePrice = $product->get_base_price() ? $product->get_base_price() : 0;

		if ($product->get_price() && $product->get_price() % $basePrice === 0) {
			$tot = $basePrice;
		} else {
			$tot = $basePrice - ($basePrice * $product->get_last_minute_discount() / 100);
		}

		$price_html = '<div class="woocommerce-Price-amount amount h1 text-tertiary mb-0 lh-1">' . $tot . '<span class="h6">' . get_woocommerce_currency_symbol() . '</span></div> par pax.';
		echo $price_html;
		?>
	</div>

</p>
