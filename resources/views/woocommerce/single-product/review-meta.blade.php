<?php
/**
 * The template to display the reviewers meta data (name, verified owner, review date)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review-meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $comment;

$rating = get_comment_meta($comment->comment_ID, 'rating', true);
if ( '0' === $comment->comment_approved ) { ?>
	<div class="meta">
		<p class="woocommerce-review__awaiting-approval text-info">
			<i class="fas fa-clock"></i>
			<?php esc_html_e( 'Your review is awaiting approval', 'woocommerce' ); ?>
		</p>
	</div>
<?php } else { ?>

	<div class="meta mb-4">

		<div class="d-flex align-items-center">
			<div class="rounded-circle overflow-hidden d-inline-block me-3">
				{!! get_avatar( $comment->comment_author_email, $size = '48' ) !!}
			</div>

			<div>
				<h6 class="mb-0 woocommerce-review__author">
					{{ comment_author() }}

				</h6>
				<span class="rating-stars">
					{!! the_rating_stars($rating) !!}
				</span>
			</div>

		</div>
	</div>

	<?php
}
?>
