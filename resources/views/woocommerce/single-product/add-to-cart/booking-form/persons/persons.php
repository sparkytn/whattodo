<?php
/**
 * Booking form persons - persons
 *
 * @author        Leanza Francesco <leanzafrancesco@gmail.com>
 *
 * @var WC_Product_Booking $product
 */

!defined( 'ABSPATH' ) && exit; // Exit if accessed directly

$default_persons   = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'persons' );
$min               = $product->get_minimum_number_of_people();
$max               = $product->get_maximum_number_of_people();
$custom_attributes = "step='1' min='1' data-min-pax='{$min}'";
$custom_attributes .= $max > 0 ? " max='{$max}'" : '';

?>
<div class="yith-wcbk-form-section yith-wcbk-form-section-persons mb-3">

<!--	Added by Sparky
		Add Max pax to label
		Changed the label design and add + - buttons
-->
	<?php
	$input_help = '';
	if($max > 0){
		$input_help = sprintf(__( '- Min %d pax et max %d pax', 'wtd'), $min,  $max);
	}
	?>
	<!--	added by Sparky-->
    <label class='yith-wcbk-booking-form__label'><?php echo yith_wcbk_get_label( 'people' ) ?> <small class="opacity-75"> <?php echo $input_help ?> </small></label>
	<div class="input-icon quantity quantity-input">
		<i class="icon fas text-warning fa-users"></i>
    <?php
		yith_wcbk_print_field( array(
								   'type'              => 'number',
								   'id'                => 'yith-wcbk-booking-persons',
								   'name'              => 'persons',
								   'custom_attributes' => $custom_attributes,
								   'value'             => 1,//max( $min, $default_persons ),
								   'class'             => 'yith-wcbk-booking-persons yith-wcbk-number-minifield form-control rounded-bottom-0 rounded-top',
							   ) );
    ?>
		<div class="quantity-nav">
			<button class="quantity-button quantity-up" type="button">+</button>
			<button class="quantity-button quantity-down" type="button">-</button>
		</div>
	</div>

	<?php
	if($product->get_minimum_number_of_people() > 1):
	?>
	<div class="rounded-bottom mb-4 fw-bold p-3 bg-light border-bottom border-end border-start small  alert-min-pax">
		<div class="d-flex align-items-start">
			<div class="me-2">
				<i class="fas fa-info-circle fs-6 text-tertiary"></i>
			</div>
			<div>
				<p class="m-0 small  fw-normal"><a href="/activite-personnalisee" target="_blank" class="text-decoration-underline"> Contactez-nous</a> si vous avez moins de <b><?=$min?> personnes</b></p>
			</div>
		</div>
	</div>
	<?php
	endif;
	?>
</div>
