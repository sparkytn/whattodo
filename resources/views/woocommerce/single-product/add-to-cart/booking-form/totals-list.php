<?php
/**
 * Booking Totals
 *
 * @author        Leanza Francesco <leanzafrancesco@gmail.com>
 * @var array              $totals     array of totals
 * @var string             $price_html the total price of the booking product
 * @var WC_Product_Booking $product    the booking product
 */
!defined( 'ABSPATH' ) && exit;
?>
<div class="yith-wcbk-booking-form-totals__list mt-3">
    <?php foreach ( $totals as $key => $total ):

        $label = $total[ 'label' ];
        $value = $total[ 'value' ];
        $is_discount = $value < 0;
        $price = isset( $total[ 'display' ] ) ? $total[ 'display' ] : ( yith_wcbk_get_formatted_price_to_display( $product, $total[ 'value' ] ) );
        $extra_classes = "yith-wcbk-booking-form-total__" . esc_attr( $key );
        $extra_classes .= $is_discount ? " yith-wcbk-booking-form-total--discount" : '';
        ?>
        <div class="yith-wcbk-booking-form-total d-flex justify-content-between <?php echo $extra_classes ?>">
            <div class="yith-wcbk-booking-form-total__label small"><?php echo $label ?></div>
            <div class="yith-wcbk-booking-form-total__value fw-bold"><?php echo $price ?></div>
        </div>
    <?php endforeach; ?>
	<hr class="my-3">
    <div class="yith-wcbk-booking-form-total  yith-wcbk-booking-form-total--total-price mb-3">
		<div class="text-center h3 mt-0">
			<small class="d-block small fs-xs text-muted yith-wcbk-booking-form-total__label"><?php _e( 'Total', 'yith-booking-for-woocommerce' ) ?></small>
			<span class="yith-wcbk-booking-form-total__value"><?php echo wp_kses_post( $price_html ); ?>
				<?php
//					$result = explode('<span class="unit-price">',$price_html);
//					dump($result);
//					echo $price_html;
				?>
			</span>
		</div>
    </div>
</div>
