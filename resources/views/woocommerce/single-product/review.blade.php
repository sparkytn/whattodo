<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div <?php comment_class('card blockquote-card shadow-none border-0 bg-light mb-5'); ?> id="li-comment-<?php comment_ID(); ?>">
	<blockquote id="comment-<?php comment_ID(); ?>" class="p-4 px-md-5 mb-0 comment_container">

			<?php
			/**
			 * The woocommerce_review_before_comment_meta hook.
			 *
			 * @hooked woocommerce_review_display_rating - 10
			 */
			//do_action( 'woocommerce_review_before_comment_meta', $comment );

			/**
			 * The woocommerce_review_meta hook.
			 *
			 * @hooked woocommerce_review_display_meta - 10
			 */
			do_action( 'woocommerce_review_meta', $comment );

			do_action( 'woocommerce_review_before_comment_text', $comment );

			/**
			 * The woocommerce_review_comment_text hook
			 *
			 * @hooked woocommerce_review_display_comment_text - 10
			 */

			echo '<div class="my-2 small">';
			do_action( 'woocommerce_review_comment_text', $comment );
			echo '</div>';

			do_action( 'woocommerce_review_after_comment_text', $comment );
			?>
		<footer class="fs-xs mt-2 text-capitalize">
			<?php
				$verified = wc_review_is_from_verified_owner( $comment->comment_ID );
				if ( 'yes' === get_option( 'woocommerce_review_rating_verification_label' ) && $verified ) {
					echo '<i class="fas fa-check-circle text-success me-1"></i> <span class="woocommerce-review__verified verified opacity-75">' . esc_attr__( 'verified owner', 'woocommerce' ) . '</span> <span class="woocommerce-review__dash">&ndash;</span>';
				}
			?>

			<time class="woocommerce-review__published-date opacity-75" datetime="<?php echo esc_attr( get_comment_date( 'c' ) ); ?>">
				<?php echo esc_html( get_comment_date( wc_date_format() ) ); ?>
			</time>
		</footer>

	</blockquote>
</div>
