<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

do_action('woocommerce_before_customer_login_form'); ?>

<?php if ( 'yes' === get_option('woocommerce_enable_myaccount_registration') ) : ?>
<div class="container px-0">
	<div class="row justify-content-between" id="customer_login">

	<div class="col-md-12">
		<div class="page-content-card mb-4 px-0 pt-4 pb-1">
			<div class="container">
				<h4 class="mb-2">Accés rapide</h4>
				<p class="lead mt-2">Connectez-vous ou inscrivez-vous rapidement avec :</p>

				{!!  do_shortcode('[miniorange_social_login view="vertical" heading=""]') !!}
			</div>
		</div>
	</div>


	<div class="col-12  mb-4">
		<div class="">
			<div class="login-or">
				<hr class="hr-or">
				<span class="span-or">Ou</span>
			</div>
		</div>
	</div>

	<div class="col-md-5">
		<div class="page-content-card mb-5 px-0 pt-4">
			<?php endif; ?>
			<div class="container">
				<h2><?php esc_html_e('Login', 'woocommerce'); ?></h2>

{{--				<h4 class="text-center  text-muted">Connectez-vous avec :</h4>--}}
{{--				<div class="">--}}
{{--					<div class="login-or">--}}
{{--						<hr class="hr-or">--}}
{{--						<span class="span-or">Ou</span>--}}
{{--					</div>--}}
{{--				</div>--}}

				<form class="woocommerce-form woocommerce-form-login login" method="post">

					<?php do_action('woocommerce_login_form_start'); ?>

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="username"><?php esc_html_e('Username or email address', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password"/>
					<div class="woocommerce-LostPassword lost_password mt-2">
						<a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'woocommerce'); ?></a>
					</div>

					</p>

					<?php do_action('woocommerce_login_form'); ?>

					<p class="form-row">
					<div class="form-check">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox form-check-input" name="rememberme" type="checkbox" id="rememberme" value="forever"/>
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme form-check-label" for="rememberme">
							<span class="form-label"><?php esc_html_e('Remember me', 'woocommerce'); ?></span>
						</label>
					</div>

					<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>

					</p>
					<p class="form-row text-center">
						<button type="submit" class="woocommerce-button btn btn-primary woocommerce-form-login__submit" name="login" value="<?php esc_attr_e('Log in', 'woocommerce'); ?>"><?php esc_html_e('Log in', 'woocommerce'); ?></button>


					</p>



					<?php do_action('woocommerce_login_form_end'); ?>

				</form>
			</div>
			<style>
				.mo-openid-app-icons>a{
					display: inline-block;
					margin: 10px;
					cursor: pointer;
				}
				.mo-openid-app-icons>a:hover{
					transform: scale(1.03);
				}

				.login-or {
					position: relative;
					color: #243e90;
					font-weight: bold;
					font-size: 20px;
					margin-top: 10px;
					margin-bottom: 10px;
					padding-top: 10px;
					padding-bottom: 10px;
				}

				.span-or {
					display: block;
					position: absolute;
					left: 50%;
					top: -8px;
					margin-left: -25px;
					background-color: #f5f8fb;
					width: 50px;
					text-align: center;
				}

				.hr-or {
					height: 1px;
					margin-top: 0px !important;
					margin-bottom: 0px !important;
				}
			</style>

			<?php if ( 'yes' === get_option('woocommerce_enable_myaccount_registration') ) : ?>
		</div>
	</div>

	<div class="col-md-7">
		<div class="page-content-card mb-5 px-0 pt-4">
			<div class="container">
				<h2><?php esc_html_e('Register', 'woocommerce'); ?></h2>

				<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action('woocommerce_register_form_tag'); ?> >

					<?php do_action('woocommerce_register_form_start'); ?>
					<div class="row">
						<div class="col-md-6">
							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
								<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_first_name" id="reg_billing_first_name" required value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
							</p>
						</div>
						<div class="col-md-6">
							<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
								<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_last_name" id="reg_billing_last_name" required value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
							</p>
						</div>
					</div>



					<?php if ( 'no' === get_option('woocommerce_registration_generate_username') ) : ?>

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="reg_username"><?php esc_html_e('Username', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
					</p>

					<?php endif; ?>

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="reg_email"><?php esc_html_e('Email address', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
					</p>


					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="billing_phone"><?php _e('Num. Téléphone', 'wtd'); ?> <span class="required">*</span></label>
						<input type="tel" class="woocommerce-Input woocommerce-Input--text input-text" name="billing_phone" id="billing_phone" placeholder="" value="<?php echo (!empty($_POST['billing_phone'])) ? esc_attr(wp_unslash($_POST['billing_phone'])) : ''; ?>"/>
					</p>


					<?php if ( 'no' === get_option('woocommerce_registration_generate_password') ) : ?>

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="reg_password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password"/>
					</p>

					<?php else : ?>

					<p><?php esc_html_e('A link to set a new password will be sent to your email address.', 'woocommerce'); ?></p>

					<?php endif; ?>
					<div class="small">
						<?php do_action('woocommerce_register_form'); ?>
					</div>
					<p class="woocommerce-form-row form-row  text-center">
						<?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
						<button type="submit" class="woocommerce-Button woocommerce-button btn btn-primary woocommerce-form-register__submit" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
					</p>

					<?php do_action('woocommerce_register_form_end'); ?>

				</form>
			</div>
		</div>

	</div>

</div>
</div>
<?php endif; ?>

<?php do_action('woocommerce_after_customer_login_form'); ?>
