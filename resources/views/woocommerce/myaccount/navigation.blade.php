<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="col-md-4 col-lg-3">
	<div class="page-content-card mb-5 px-0 pt-4">
		<div class="container">

			@php
				do_action( 'woocommerce_before_account_navigation' );
			@endphp

			<nav class="woocommerce-MyAccount-navigation">
				<?php
					$current_user = wp_get_current_user();
					global $wp;
				?>
				<div class="text-center mb-4">
					<div class="avatar avatar-circle mb-2 d-none d-lg-block">
						{!! get_avatar( $current_user->ID, apply_filters( 'yith_wcmap_filter_avatar_size', 80 ), 'retro', $current_user->display_name ,['class'=> 'avatar-img rounded-circle'] ) !!}
					</div>

					<h6 class="card-title mb-0">{{ $current_user->first_name }} {{ $current_user->last_name }}</h6>
					<p class="card-text fs-xs mt-0 opacity-50">{{$current_user->user_email}}</p>
				</div>

				<span class="text-cap h6 text-uppercase mb-2">{{ __('Mon Compte','wtd') }}</span>
				<ul class="nav flex-column flex-wrap nav-menu-column mb-auto">
					<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
						<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?> {{ $endpoint == 'bookings' &&isset( $wp->query_vars['view-booking'] ) ? 'is-active' : '' }} nav-item">
							<a class="nav-link nav-link__{{$endpoint}}" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
						</li>
					<?php endforeach; ?>
				</ul>

			</nav>

			@php
				do_action( 'woocommerce_after_account_navigation' );
			@endphp

		</div>
	</div>
</div>
