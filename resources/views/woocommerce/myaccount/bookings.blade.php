<?php
/**
 * Bookings
 * Shows booking on the account page.
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/bookings.php.
 *
 * @var YITH_WCBK_Booking[] $bookings
 * @var bool                $has_bookings
 * @var int                 $max_num_pages
 * @var int                 $current_page
 * @var int                 $total
 *
 * @package YITH\Booking
 */

defined( 'YITH_WCBK' ) || exit; // Exit if accessed directly.

$bookings_endpoint = apply_filters( 'yith_wcbk_endpoint_bookings', 'bookings' );
$endpoint          = yith_wcbk()->endpoints->get_endpoint( $bookings_endpoint );

/*
 * ToDo : Sparky -- Change the cards of confirmed booking and cart booking to hook or another template and change add tabs to show by status with other endpoint pages
*/
?>

<?php do_action( 'yith_wcbk_before_account_bookings', $has_bookings ); ?>

<?php if ( $has_bookings ) : ?>


	@if(! WC()->cart->is_empty())

		<?php
		$cart = WC()->cart->get_cart();
		foreach ($cart as $cart_item_key => $cart_item){
		$booking = yith_get_booking($cart_item['yith_booking_data']['_booking_id']);
		?>
		<div class="card shadow-none bg-light mb-4">
			<div class="card-header bg-success  border-0">
				<h6 class="form-label text-white mb-0"><i class="fas fa-cart-plus me-2"></i>{{ __('Vous avez une réservation dans le panier','wtd') }}</h6>
			</div>
			<div class="card-body">
				<h5 class="mb-1">{{ $booking->get_title() }}</h5>
				<div class="fs-xs text-muted"><?=__('Réservation placé le','wtd')?> <b><?=date_i18n( get_option( 'date_format' ), strtotime( $booking->post->post_date ) )?></b>.</div>

				<p class="mt-0 small">{{ __('Vous avez 2 jours pour valider la réservation. Passez au paiement ou consultez les détails de la réservation.','wtd') }}</p>

				<a href="{{ get_permalink( wc_get_page_id( 'checkout' ) ) }}" class="btn btn-success btn-sm fs-xs me-2 mb-2">{{ __('Passer au paiement','wtd') }}</a>
				<a href="{{ $booking->get_view_booking_url() }}" class="btn btn-primary btn-sm fs-xs mb-2">{{ __('Plus de Détails','wtd') }}</a>

			</div>
		</div>

		<?php
		}
		?>
		<hr class="mb-4">
	@endif



	@php($confirmedBookings = get_user_confirmed_bookings() )
	@if( $confirmedBookings)
		<?php
		$view_booking_endpoint = yith_wcbk()->endpoints->get_endpoint( 'view-booking' );
		$accountPermalink = wc_get_page_permalink( 'myaccount' );
		?>
		<h5 class="form-label text-dark fs-6">{{ __('Les réservations Confirmées','wtd') }} :</h5>
		@for( $i=0; $i < count($confirmedBookings); $i++)
			<div class="alert alert-success mb-4">
				<h6 class="form-label text-white mb-2 fs-6"><i class="fas fa-circle-plus me-2 "></i>{{ __('Vous avez une réservation confirmé','wtd') }} : <span class="badge bg-dark"> #{{ $confirmedBookings[$i] }}</span></h6>
				<div class="d-lg-flex align-items-center">
					<p class="mt-0 small mb-2 pe-4">{{ __( 'Vous avez 2 jours pour valider la réservation. Cliquez sur le bouton pour voir les  détails de la réservation.', 'wtd' ) }}</p>
					<a href="{{ wc_get_endpoint_url($view_booking_endpoint, $confirmedBookings[$i],  $accountPermalink) }}" class="btn btn-primary btn-sm fs-xs text-decoration-none">{{ __('Plus de Détails') }}</a>
				</div>
			</div>
		@endfor
		<hr class="mb-4">
	@endif

	<h5 class="form-label text-dark mb-2 fs-6">{{ __('Toutes les réservations','wtd') }} :</h5>
	<p class="small mt-0">{{ __('Vous avez ','wtd') }} <b>{{  $total  }} {{ __('réservations','wtd') }} {{ __('en totale.','wtd') }}</b></p>


<?php do_action( 'yith_wcbk_show_bookings_table', $bookings ); ?>

	<?php do_action( 'yith_wcbk_before_account_bookings_pagination' ); ?>

	<?php if ( 1 < $max_num_pages ) : ?>
		<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
			<div class="text-center form-label">{{ __('Page','wtd') }} {{ $current_page }}/{{ $max_num_pages }}</div>
			<?php
			if ( 1 !== $current_page ) :
				$prev_url = wc_get_endpoint_url( $endpoint, $current_page - 1 );
				?>
				<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( $prev_url ); ?>"><?php esc_html_e( 'Previous', 'yith-booking-for-woocommerce' ); ?></a>
			<?php endif; ?>

			<?php
			if ( intval( $max_num_pages ) !== $current_page ) :
				$next_url = wc_get_endpoint_url( $endpoint, $current_page + 1 );
				?>
				<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( $next_url ); ?>"><?php esc_html_e( 'Next', 'yith-booking-for-woocommerce' ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

<?php else : ?>
	<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Go to the Shop', 'yith-booking-for-woocommerce' ); ?>
		</a>
		<?php esc_html_e( 'No booking has been made yet.', 'yith-booking-for-woocommerce' ); ?>
	</div>
<?php endif; ?>

<?php do_action( 'yith_wcbk_after_account_bookings', $has_bookings ); ?>
