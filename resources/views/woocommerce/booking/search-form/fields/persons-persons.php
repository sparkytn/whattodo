<?php
/**
 * Booking Search Form Field Persons
 * This template can be overridden by copying it to yourtheme/woocommerce/booking/search-form/fields/persons-persons.php.
 *
 * @var YITH_WCBK_Search_Form $search_form
 */

!defined( 'YITH_WCBK' ) && exit;

$persons = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( "persons" );
$persons = !!$persons ? $persons : '';
?>

<div class="inner">
    <label class="form-label">
        <?php echo yith_wcbk_get_label( 'people' ); ?>
    </label>
    <div class="input-icon quantity quantity-input">
		<i class="icon fas text-warning fa-users"></i>
        <input type="number" class="form-control" name="persons" min="1" step="1" max="100" placeholder="Nb. de personnes" value="<?php echo $persons ?>" />
		<div class="quantity-nav">
			<button class="quantity-button quantity-up" type="button">+</button>
			<button class="quantity-button quantity-down" type="button">-</button>
		</div>
    </div>
</div>
