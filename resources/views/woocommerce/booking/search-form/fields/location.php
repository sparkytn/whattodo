<?php
/**
 * Booking Search Form Field Location
 * This template can be overridden by copying it to yourtheme/woocommerce/booking/search-form/fields/location.php.
 *
 * @var YITH_WCBK_Search_Form $search_form
 */

!defined( 'YITH_WCBK' ) && exit;

//$default_location_range = apply_filters( 'yith_wcbk_booking_search_form_default_location_range', 30 );
$location               = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'location' );
//$location_range         = YITH_WCBK_Search_Form_Helper::get_searched_value_for_field( 'location_range' );
//$location_range         = !!$location_range ? $location_range : $default_location_range;
?>

<div class="inner">
	<label class="form-label" id="activity-location-input">
		<?php _e( 'Location', 'yith-booking-for-woocommerce' ); ?>
	</label>

	<div class="input-icon">
		<i class="fas fa-map-marker-alt text-warning icon"></i>
		<input type="text" name="location" id="activity-location-input" class="form-control yith-wcbk-google-maps-places-autocomplete" value="<?php echo $location ?>"/>
	</div>
</div>
