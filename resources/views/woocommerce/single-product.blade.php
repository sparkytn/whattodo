{{--
The Template for displaying all single products

This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see         https://docs.woocommerce.com/document/template-structure/
@package     WooCommerce\Templates
@version     1.6.4
--}}

@extends('layouts.app')

@section('content')
	@while(have_posts())
	@php
		the_post();
			global $product;
			$category = get_term($product->get_category_ids()[0]);
			$rating_avrg = $product->get_average_rating();
	@endphp
	@endwhile

	<header class="activity-booking-cta yith_wcbk_widget_booking_form_mouse_trap">
		<div class="container">
			<div class="d-flex align-items-center justify-content-between">
				<div class="pe-2">
					<h6 class="title h6 small text-wrap mb-0">{!! get_the_title() !!}</h6>
					<b class="fs-xs text-secondary"> {{ $product->get_base_price() }} {{ get_woocommerce_currency_symbol() }} {{ __('par pax', 'wtd') }}</b>
				</div>
				<div>
					<a href="#booking-card" class="btn btn-secondary btn-sm text-nowrap fs-xs">
						{{ __('Réserver', 'wtd') }}
					</a>
				</div>
			</div>
		</div>
	</header>


	<div class="page-header section-padding page-header--overlap">
		<div class="container">
			<x-breadcrumb></x-breadcrumb>

			<h1 class="h2 title mb-2">{!! get_the_title() !!}</h1>

			<div class="d-flex page-header-meta mb-2">
				<div class="me-3">
					<a href="{{ get_category_link($category) }}" class="text-white-5 detail">
						<i class="fa-{{ $category->slug }}  icon-activity text-secondary"></i>
						{{ $category->name }}
					</a>
				</div>
				@if($rating_avrg)
				<div>
					<a href="#review" class="ps-3 flex-shrink-0">
						<i class="fas fa-star text-secondary me-1"></i>
						{{ floatval($rating_avrg) }}
					</a>
				</div>
				@endif
			</div>

		</div>
	</div>

	<div class="page-body">
		<div class="container p-0">
			<div class="page-content-card mb-5">
				<div class="container">
					@php
						do_action('get_header', 'shop');
						do_action('woocommerce_before_main_content');
					@endphp

					@while(have_posts())
						@php
							the_post();

							wc_get_template_part('content', 'single-product');
						@endphp
					@endwhile

					@php
						do_action('woocommerce_after_main_content');
						do_action('get_sidebar', 'shop');
						do_action('get_footer', 'shop');
					@endphp
				</div>
			</div>
		</div>
	</div>



	<x-activity-carousel type="featured" class="bg-light"  title="les activités en vedette"/>



@endsection
