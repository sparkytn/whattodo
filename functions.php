<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our theme. We will simply require it into the script here so that we
| don't have to worry about manually loading any of our classes later on.
|
*/

use App\Modules\LazyLoader;

if (! file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    wp_die(__('Error locating autoloader. Please run <code>composer install</code>.', 'sage'));
}

require $composer;

/*
|--------------------------------------------------------------------------
| Register Sage Theme Files
|--------------------------------------------------------------------------
|
| Out of the box, Sage ships with categorically named theme files
| containing common functionality and setup to be bootstrapped with your
| theme. Simply add (or remove) files from the array below to change what
| is registered alongside Sage.
|
*/

collect(['helpers', 'setup', 'filters', 'admin'])
    ->each(function ($file) {
        $file = "app/{$file}.php";

        if (! locate_template($file, true, true)) {
            wp_die(
                sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file)
            );
        }
    });

/*
|--------------------------------------------------------------------------
| Enable Sage Theme Support
|--------------------------------------------------------------------------
|
| Once our theme files are registered and available for use, we are almost
| ready to boot our application. But first, we need to signal to Acorn
| that we will need to initialize the necessary service providers built in
| for Sage when booting.
|
*/

add_theme_support('sage');

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We are ready to bootstrap the Acorn framework and get it ready for use.
| Acorn will provide us support for Blade templating as well as the ability
| to utilize the Laravel framework and its beautifully written packages.
|
*/

new Roots\Acorn\Bootloader();



// Testing Yith customization;
remove_action('yith_wcbk_booking_form_meta', 'yith_wcbk_booking_form_meta', 10);
add_action( 'yith_wcbk_booking_form_meta', 'sparky_wcbk_booking_form_meta', 10, 1 );
function sparky_wcbk_booking_form_meta(){
	global $product;

	$checkin  = $product->get_check_in();
	$checkout = $product->get_check_out();
	$is_fixed_blocks     = $product->is_type_fixed_blocks();
	$min                 = $product->get_minimum_duration();
	$max                 = $product->get_maximum_duration();
	$show_duration_field = !$is_fixed_blocks && $min !== $max;
	$duration_number = $product->get_duration();
	$duration_unit = $product->get_duration_unit();
	$duration_label      = yith_wcbk_get_product_duration_label( $duration_number, $duration_unit, true );

	if ( !!$checkin || !!$checkout ):
?>

	<div class="row mb-3">
		<?php if ( !!$checkin ): ?>
			<div class="col">
				<div class="form-label opacity-75 fs-xxs mb-0">
					<?php echo sprintf( __( '%s:', 'yith-booking-for-woocommerce' ), yith_wcbk_get_label( 'check-in' ) ) ?>
				</div>
				<?php echo sprintf( __( '%s', 'yith-booking-for-woocommerce' ), $checkin ) ?>
			</div>
		<?php endif; ?>

		<?php if ( !!$checkout ): ?>
			<div class="col">
				<div class="form-label opacity-75 fs-xxs mb-0">
					<?php echo sprintf( __( '%s:', 'yith-booking-for-woocommerce' ), yith_wcbk_get_label( 'check-out' ) ) ?>
				</div>
				<?php echo sprintf( __( '%s', 'yith-booking-for-woocommerce' ), $checkout ) ?>
			</div>
		<?php endif; ?>

		<?php if ( !!$checkout ): ?>
			<div class="col">
				<div class="form-label opacity-75 fs-xxs mb-0">
					<?php echo  __('Durée', 'wtd')  ?>
				</div>
				<?php echo  $duration_label  ?>
			</div>
		<?php endif; ?>
	</div>
	<hr>

<?php endif;

}

//$bookings = YITH_WCBK()->booking_helper->get_bookings_by_user(get_current_user_id());
//foreach ( $bookings as $booking ){
//	$url   = esc_url( $booking->get_view_booking_url() );
//	$title = $booking->get_title();
//	echo "<a href='$url'>$title</a>";
//	echo $booking->get_status_text();
//	echo $booking->get_formatted_date( 'from' );
//	echo $booking->get_formatted_date( 'to' ) .'</br>';
//
//}



require_once 'includes/acf/acf.php';
require_once 'includes/contact-form.php';
require_once 'includes/cutom-post-type-and-tax.php';
require_once 'includes/woocommerce-cutom.php';
require_once 'includes/remove-styles-and-scripts.php';
require_once 'includes/yith-multi-vendors.php';
require_once 'includes/woocommerce-user-account-fields.php';
//require_once 'includes/CustomPostTypeSortable/CustomPostTypeSortable.php';
//$faqs = new CustomPostTypeSortable('faq', 'FAQs');



use App\Modules\CustomActivityForm;
use App\Modules\WooCommerce;
new CustomActivityForm();
new WooCommerce();


use App\Helpers\SortableList\SortableCustomPostAndTax;
new SortableCustomPostAndTax('faq', 'faq_cat','FAQs');
new SortableCustomPostAndTax('faq', 'faq_cat', 'FAQ Categories', true);


require_once 'includes/admin/user-profile.php';
require_once 'includes/admin/yith-custom-admin.php';
require_once 'includes/admin/custom-admin-design.php';


add_action( 'template_redirect', 'sparky_redirect_faq' );
function sparky_redirect_faq() {
	if ( is_singular( 'faq' ) || is_tax('faq_cat') ) {
		wp_redirect( home_url(), 301 );
		exit;
	}
}
define( 'FAQ_PAGE', get_field( 'faq_page', 'option' ) );

//add_action( 'wp_insert_post_data', 'biscuit_cpt_parent_page', 99, 1 );
//function biscuit_cpt_parent_page( $data ) {
//	global $post;
//
//	if ( is_admin() || ! $post ) return $data;
//
//	if ( defined('DOING_AJAX') && DOING_AJAX ) return $data;
//
//
//	// if ( !wp_verify_nonce( $_POST['staff_parent_custom_box'], 'stc_cpt' ) )
//	//     return $data;
//
//	// verify if this is an auto save routine.
//	// If it is our form has not been submitted, so we dont want to do anything
//	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
//		return $data;
//
//	if ( $post->post_type == "faq" ){
//		$data['post_parent'] = FAQ_PAGE ;
//	}
//
//	return $data;
//}



//add_action('yith_wcbk_after_request_confirmation_action', 'persist_booking_requested_modal_in_session', 10, 3 );
function persist_booking_requested_modal_in_session($success,$booking,$product) {
	WC()->session->set( 'bookingRequest-id',  $booking->id);
	WC()->session->set( 'bookingRequest-title',  $booking->title);
	WC()->session->set( 'bookingRequest-start-date',  $_REQUEST['start-date']);
	WC()->session->set( 'bookingRequest-pax',  $_REQUEST['persons']);
	WC()->session->set( 'bookingRequest-url',  $booking->get_view_booking_url());
	WC()->session->set( 'bookingRequest-success',  $success);
}


//add_filter('yith_wcbk_redirect_after_request_confirmation_action', 'sparky_redirect_to_same_product_page_after_reuqest_booking',10,4);
function sparky_redirect_to_same_product_page_after_reuqest_booking($enabled, $success, $booking, $product) {
    return $product->get_permalink();
}




function my_block_render_callback( $attributes, $content ) {
// On the front-end (or non REST API requests), we use $content.

		if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) {
			return '<h3>MyServerSideRender output</h3>' . $attributes['title'];
		}
		return $content;

		/* Or you could instead just do:
		return $content ? $content : $attributes['innerContent'];
		*/
}


//add_action( 'init', 'sparky_booking_notice');
function sparky_booking_notice() {

	if ( is_admin()  ) return;

	if (  defined('DOING_AJAX') && DOING_AJAX ) return;

	if ( ! WC()->session ) return;
	if ( ! WC()->session->get( 'bookingRequest-success')  ) return;

	$booking_id = WC()->session->get( 'bookingRequest-id');
	$product_title = WC()->session->get( 'bookingRequest-title');
	$start_date = WC()->session->get( 'bookingRequest-start-date');
	$pax = WC()->session->get( 'bookingRequest-id')['pax'];
	$booking_url = WC()->session->get( 'bookingRequest-url') ;

	add_action('wp_footer', function () use ($booking_id, $product_title, $start_date, $pax,$booking_url) {
		echo'
			<script>
				var myModal = new window.Modal(document.getElementById(\'bookingModal\'), {
					backdrop: "static"
				})
				myModal.show();
				jQuery("[bm-url]").attr("href", "'. $booking_url .'");
				jQuery("[bm-id]").html("#'. $booking_id .'");
				jQuery("[bm-title]").html("'. $product_title .'");
				jQuery("[bm-date]").html("'. $start_date .'");
				jQuery("[bm-pax]").html("'. $pax .'");
			</script>
			';
	}, 100);

	WC()->session->set( 'bookingRequest-id', null);
	WC()->session->set( 'bookingRequest-title', null);
	WC()->session->set( 'bookingRequest-start-date', null);
	WC()->session->set( 'bookingRequest-pax', null);
	WC()->session->set( 'bookingRequest-url', null);
	WC()->session->set( 'bookingRequest-success', null);
}





//add_filter( 'manage_edit-faq_sortable_columns', function( $sortable_columns ) {
//	$sortable[ 'taxonomy-faq_cat' ] = ['orderby', 'asc'];
//	return $sortable;
//}, 10, 1 );




add_action('sparky_show_product_card_badge','sparky_show_product_badge');
add_action('sparky_show_product_detail_badge','sparky_show_product_badge',10, 2);
function sparky_show_product_badge($badge, $size = '') {

    if( ! $badge){
    	return ;
	}

    if( ! get_field('show_in_card', 'badge_'.$badge) ){
    	return ;
	}

    // exit if badges dates is expired;
	$badge_range_dates = get_field('product_badge_dates');
    if( isset($badge_range_date['start']) ){
    	if( ! isDateBetweenDates( $badge_range_dates['start'] ,$badge_range_dates['end'] ) ){
			return ;
		}
	}

    $badge_field = $badge;
    //Get badge icon if set
    if( $icon = get_field('badge_icon', $badge_field)){
		$icon = '<i class="fas '.$icon.' me-1"></i>';
	}else{
    	$icon = '';
	}
	//Get badge color
	$color = get_field('badge_color', $badge_field);

    // Add Action show badge detail bloc
	if( get_field('show_in_product_detail', $badge_field) && $size != ''){

		add_action('sparky_show_product_detail_badge_detail', function () use ($icon, $color, $badge_field, $badge_range_dates) {
			$startDate = '<b>'.wp_date('d F Y',strtotime($badge_range_dates['start'])).'</b>';
			$endDate = '<b>'.wp_date('d F Y',strtotime($badge_range_dates['end'])).'</b>';

			$description = get_field('badge_description', $badge_field );
			$description = str_replace("{start_date}", $startDate, $description);
			$description = str_replace("{end_date}", $endDate, $description);

			$icon = $icon != '' ? '<div class="me-3 fa-2x">'. $icon .'</div>' : $icon;
			$body  = '<div><h6 class="mb-1 text-white">'.get_field('badge_title', $badge_field ).'</h6>' . '<p class="m-0 small">'.$description.'</p> </div>';
			$alert = '<div class="alert '.$color.' mb-4"><div class="d-flex align-items-start">' . $icon . $body . '</div></div>';

			echo $alert;
			return;
		});
	}

	//Render badge
    $badge_html = '<div class="badge-wrapper"><span class="badge '.$color.' '.$size.'">'. $icon . get_the_title($badge) .'</span> </div>';

    echo $badge_html;
	return;
}

function isDateBetweenDates(  $startDate, $endDate) {
	$date = new DateTime('now',wp_timezone());
	$startDate = new DateTime( $startDate,wp_timezone());
	$endDate = new DateTime( $endDate. ' 23:59:59',wp_timezone());
	return $date >= $startDate && $date <= $endDate;
}



function limit_text($text, $limit = 15) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos   = array_keys($words);
		$text  = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
}




// add home Icon to main menu
//add_filter('walker_nav_menu_start_el','sparky_navbar_bs',10,4);
//function sparky_navbar_bs($item_output, $item, $depth, $args) {
//	if($item->type_label === 'Front Page'){
//		//dd($item);
//		$pos = strpos($item_output,">");
//		$icon = '<i class="fas fa-home me-2 me-lg-0 d-none d-lg-inline"></i> <span class="d-lg-none">';
//		$item_output = substr_replace($item_output, $icon, $pos+1, 0);
//		$pos = strpos($item_output,"</a>");
//		$item_output = substr_replace($item_output, '</span>', $pos, 0);
//	}
//
//	return $item_output;
//}








//=====================
// Phone country code

add_action( 'wp_footer', 'callback_wp_footer',100 );
function callback_wp_footer(){
	if ( ! is_checkout() || ! is_account_page()) {
	 return;
	}
	?>
	<script type="text/javascript">
		( function( $ ) {
			$( document.body ).on( 'updated_checkout', function(data) {
				var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>",
					country_code = $('#billing_country').val();

				var ajax_data = {
					action: 'append_country_prefix_in_billing_phone',
					country_code: $('#billing_country').val()
				};

				$.post( ajax_url, ajax_data, function( response ) {
					// let phone = $('#billing_phone').val()
					window.phoneNumberInput.setCountry(response)

				});


			} );
		} )( jQuery );
	</script>
	<?php
}

add_action( 'wp_ajax_nopriv_append_country_prefix_in_billing_phone', 'country_prefix_in_billing_phone' );
add_action( 'wp_ajax_append_country_prefix_in_billing_phone', 'country_prefix_in_billing_phone' );
function country_prefix_in_billing_phone() {
	$country_code = isset( $_POST['country_code'] ) ? $_POST['country_code'] : 'tn';
	echo strtolower($country_code);
	die();
}


//---------------------
// Append country code (eg +216) in phone number field

add_action('woocommerce_checkout_posted_data', function ( $data) {
	if( $_POST['full_phone'] !== ''){
		$data['billing_phone'] = $_POST['full_phone'];
	}
	return $data;
},1,10);



/*
|--------------------------------------------------------------------------
| Add default Term Order to faq category after creation
|--------------------------------------------------------------------------
|
*/
add_action('edited_term', function($term_id, $tax_id, $taxonomy){
	if($taxonomy === 'faq_cat'){
		add_term_meta( $term_id, 'term_order', 100 );
	}
},20,3);
add_action('create_term', function($term_id, $tax_id, $taxonomy){
	if($taxonomy === 'faq_cat'){
		add_term_meta( $term_id, 'term_order', 100 );
	}
},20,3);





/*
|--------------------------------------------------------------------------
| Lazy load images add class and change src and srcset to data attributes
|--------------------------------------------------------------------------
|
*/
//
//new LazyLoader;



