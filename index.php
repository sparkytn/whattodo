<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="dns-prefetch" href="//maps.google.com">
	<link rel="dns-prefetch" href="//www.googletagmanager.com">
	<link rel="dns-prefetch" href="//connect.facebook.net">
	<link rel="dns-prefetch" href="//facebook.net">
	<link rel="preload" href="/wp-content/themes/whattodo/public/fonts/fa/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="/wp-content/themes/whattodo/public/fonts/fa/fa-solid-900.ttf" as="font" type="font/ttf" crossorigin>
	<link rel="preload" href="/wp-content/themes/whattodo/public/fonts/fa/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="/wp-content/themes/whattodo/public/fonts/fa/fa-brands-400.ttf" as="font" type="font/ttf" crossorigin>

	<meta name="facebook-domain-verification" content="tdfxrjyfsvucu8i4v62zg6xog8zdpg" />
	<!-- Meta Pixel Code -->
	<script nonce="I9mpJ87h0R">
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '169161665168394');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" class="pixel" src="https://www.facebook.com/tr?id=169161665168394&ev=PageView&noscript=1"/></noscript>
	<!-- End Meta Pixel Code -->
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script nonce='kzOXKv0fxQ'>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;var n=d.querySelector('[nonce]');
			n&&j.setAttribute('nonce',n.nonce||n.getAttribute('nonce'));f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-K6F22PX');</script>
	<!-- End Google Tag Manager -->

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6F22PX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php wp_body_open(); ?>
<?php do_action('get_header'); ?>

<div id="app">
	<?php echo \Roots\view(\Roots\app('sage.view'), \Roots\app('sage.data'))->render(); ?>
</div>

<?php do_action('get_footer'); ?>
<?php wp_footer(); ?>
<?php
	if( is_checkout() || is_account_page() || is_page('contact') || is_page('activite-personnalisee')){
		echo '<script src="https://www.google.com/recaptcha/api.js?render=6LeQ4nYfAAAAAMyykUj7Xazlegxz7Js-zYfuX8Os" async defer></script>';
	}
?>

<script>
	jQuery(document).on('change','#yith-wcbk-booking-persons',function (e){
		var $this = jQuery(this);
		minpax=  $this.data('min-pax');
		console.log($this.val() < minpax, $this.val() , minpax)

		if($this.val() < minpax){
			jQuery('.booking-remaining-pax').html(minpax - $this.val())
			jQuery('.alert-min-pax').fadeIn(300)
			console.log('not yet')
		}else {
			jQuery('.alert-min-pax').fadeOut(300)
			console.log('c bon')
		}
	})
</script>


</body>
</html>
