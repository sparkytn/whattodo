<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Post Types
    |--------------------------------------------------------------------------
    |
    | Here you may specify the post types to be registered by Poet using the
    | Extended CPTs library. <https://github.com/johnbillion/extended-cpts>
    |
    */

    'post' => [
        'faq' => [
            'enter_title_here' => 'Enter a question here',
            'menu_icon' => 'dashicons-sos',
            'supports' => ['title', 'editor', 'page-attributes'],
            'show_in_rest' => true,
            'has_archive' => false,
			'rewrite' => array( 'slug' => 'faq', 'with_front' => false),
            'labels' => [
                'singular' => 'FAQ',
                'plural' => 'FAQs',
            ],
        ],
		'badge' => [
			'enter_title_here' => 'Enter a Badge here',
			'public'              => false,
			'menu_position'       => 56,
			'show_ui'             => true,
			'menu_icon' => 'dashicons-tag',
			'show_in_rest' => false,
			'exclude_from_search' => true,
			'supports' => ['title'],
			'rewrite'             => true,
			'has_archive'         => true,
			'labels' => [
				'singular' => 'Badge',
				'plural' => 'Badges',
			],
		],
		'label' => [
			'enter_title_here' => 'Enter a Label here',
			'public'              => false,
			'menu_position'       => 56,
			'show_ui'             => true,
			'show_in_menu'             => 'edit.php?post_type=product',
			'menu_icon' => 'dashicons-tag',
			'show_in_rest' => false,
			'exclude_from_search' => true,
			'supports' => ['title', 'thumbnail'],
			'rewrite'             => false,
			'has_archive'         => false,
			'labels' => [
				'singular' => 'Label',
				'plural' => 'Labels',
			],
		],
		'sparky_contact_form' => [
			'public'              => false,
			'show_ui'             => true,
			'menu_icon' => 'dashicons-email-alt',
			'show_in_rest' => false,
			'exclude_from_search' => true,
			'supports' => ['author'],
			'has_archive'         => false,
			'hierarchical'		=> false,
			'capability_type' => 'post',
			'capabilities' => array(
				'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				'update_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				'delete_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
			),
			'map_meta_cap' => true,
			'labels' => [
				'singular' => 'Message',
				'plural' => 'Messages',
			],
		],
		'custom_activity_req' => [
			'public'              => false,
			'show_ui'             => true,
			'menu_icon' => 'dashicons-star-filled',
			'show_in_rest' => false,
			'exclude_from_search' => true,
			'supports' => ['title','author'],
			'has_archive'         => false,
			'hierarchical'		=> false,
			'capability_type' => 'post',
			'capabilities' => array(
				'create_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				'update_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
				'delete_posts' => false, // Removes support for the "Add New" function ( use 'do_not_allow' instead of false for multisite set ups )
			),
			'map_meta_cap' => true,
			'labels' => [
				'singular' => 'Custom Activity',
				'plural' => 'Custom Activities',
			],
		],
    ],

    /*
    |--------------------------------------------------------------------------
    | Taxonomies
    |--------------------------------------------------------------------------
    |
    | Here you may specify the taxonomies to be registered by Poet using the
    | Extended CPTs library. <https://github.com/johnbillion/extended-cpts>
    |
    */

    'taxonomy' => [
        'faq_cat' => [
            'links' => ['faq'],
            'meta_box' => 'radio',
			'hierarchical'      => false,
			'exclusive'         => true,
			'required'         => true,
			'show_admin_column'         => true,
			'sort'         => true,
			'labels' => [
				'singular' => 'Category',
				'plural' => 'Categories',
			],
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Blocks
    |--------------------------------------------------------------------------
    |
    | Here you may specify the block types to be registered by Poet and
    | rendered using Blade.
    |
    | Blocks are registered using the `namespace/label` defined when
    | registering the block with the editor. If no namespace is provided,
    | the current theme text domain will be used instead.
    |
    | Given the block `sage/accordion`, your block view would be located at:
    |   ↪ `views/blocks/accordion.blade.php`
    |
    | Block views have the following variables available:
    |   ↪ $data    – An object containing the block data.
    |   ↪ $content – A string containing the InnerBlocks content.
    |                Returns `null` when empty.
    |
    */

    'block' => [
        // 'whattodo/accordion' => [
        //     'attributes' => [
        //         'title' => [
        //             'default' => 'Lorem ipsum',
        //             'type' => 'string',
        //         ],
        //     ],
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Categories
    |--------------------------------------------------------------------------
    |
    | Here you may specify block categories to be registered by Poet for use
    | in the editor.
    |
    */

    'block_category' => [
        'cta' => [
            'title' => 'Call to Action',
            'icon' => 'star-filled',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Patterns
    |--------------------------------------------------------------------------
    |
    | Here you may specify block patterns to be registered by Poet for use
    | in the editor.
    |
    | Patterns are registered using the `namespace/label` defined when
    | registering the block with the editor. If no namespace is provided,
    | the current theme text domain will be used instead.
    |
    | Given the pattern `sage/hero`, your pattern content would be located at:
    |   ↪ `views/block-patterns/hero.blade.php`
    |
    | See: https://developer.wordpress.org/reference/functions/register_block_pattern/
    */

    'block_pattern' => [
        // 'sage/hero' => [
        //     'title' => 'Page Hero',
        //     'description' => 'Draw attention to the main focus of the page, and highlight key CTAs',
        //     'categories' => ['all'],
        // ],
        'sage/fake-paragraph' => [
        'title' => 'Fake Paragraph',
        'description' => 'Filler content used instead of actual content for testing purposes',
        'categories' => ['all'],
        'content' => '<!-- wp:paragraph --><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione nulla culpa repudiandae nisi nostrum et, labore earum repellendus porro, mollitia voluptas quam? Modi sint tempore deleniti nesciunt ab, perferendis et.</p><!-- /wp:paragraph -->',
    ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Block Pattern Categories
    |--------------------------------------------------------------------------
    |
    | Here you may specify block pattern categories to be registered by Poet for
    | use in the editor.
    |
    */

    'block_pattern_category' => [
        'all' => [
            'label' => 'All Patterns',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Editor Palette
    |--------------------------------------------------------------------------
    |
    | Here you may specify the color palette registered in the Gutenberg
    | editor.
    |
    | A color palette can be passed as an array or by passing the filename of
    | a JSON file containing the palette.
    |
    | If a color is passed a value directly, the slug will automatically be
    | converted to Title Case and used as the color name.
    |
    | If the palette is explicitly set to `true` – Poet will attempt to
    | register the palette using the default `palette.json` filename generated
    | by <https://github.com/roots/palette-webpack-plugin>
    |
    */

    'palette' => [
        // 'red' => '#ff0000',
        // 'blue' => '#0000ff',
    ],

    /*
    |--------------------------------------------------------------------------
    | Admin Menu
    |--------------------------------------------------------------------------
    |
    | Here you may specify admin menu item page slugs you would like moved to
    | the Tools menu in an attempt to clean up unwanted core/plugin bloat.
    |
    | Alternatively, you may also explicitly pass `false` to any menu item to
    | remove it entirely.
    |
    */

    'admin_menu' => [
        // 'gutenberg',
    ],

];
