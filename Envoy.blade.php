@servers(['web' => 'bitnami@15.188.249.130'])


@setup
    $directory = "~/htdocs/wp-content/themes/whattodo/";
    $repository = "git@bitbucket.org:sparkytn/whattodo.git";
@endsetup

@macro('create')
    clone
    configure
@endmacro

@macro('deploy')
    pull
    configure
@endmacro

@macro('refresh')
    delete
    clone
    configure
@endmacro

@task('clone')
    git clone -b master {{ $repository }} {{ $directory }};

    cd {{ $directory }};
    composer self-update;
    composer install --prefer-dist --no-dev --no-interaction;

    cp .env.production .env;

    echo "Project has been created";
@endtask

@task('pull')
    cd {{ $directory }};
	sudo chown -R bitnami:daemon *
    git pull origin;
    composer install --prefer-dist --no-dev --no-interaction;
	sudo chown -R daemon:daemon *
	sudo chown -R bitnami:daemon .git
    #cp .env.production .env;
    echo "Deployment finished successfully!";
@endtask

@task('configure')
	echo "starting configure";
    cd {{ $directory }};

    wp acorn optimize;
    wp acorn view:cache;

	echo "cache cleared";

    sudo chown daemon:daemon -R {{ $directory }};
    echo "Configured and permissions have been set";
@endtask

@task('delete', ['confirm' => true])
    rm -rf {{ $directory }};
    echo "Project directory has been deleted";
@endtask

@task('reset', ['confirm' => true])
    cd {{ $directory }};
    git reset --hard HEAD;
@endtask

@task('migrate')
    cd {{ $directory }};
    php artisan migrate --force;
@endtask

@task('rollback')
    cd {{ $directory }};
    php artisan migrate:rollback --force;
@endtask

@task('seed')
    cd {{ $directory }};
    php artisan db:seed --force;
@endtask
